'use strict';

describe('Service: sendSubscribeFactory', function () {

  // load the service's module
  beforeEach(module('foodieFrontApp'));

  // instantiate service
  var sendSubscribeFactory;
  beforeEach(inject(function (_sendSubscribeFactory_) {
    sendSubscribeFactory = _sendSubscribeFactory_;
  }));

  it('should do something', function () {
    expect(!!sendSubscribeFactory).toBe(true);
  });

});
