'use strict';

describe('Service: personalinformation', function () {

  // load the service's module
  beforeEach(module('foodieFrontApp'));

  // instantiate service
  var personalinformation;
  beforeEach(inject(function (_personalinformation_) {
    personalinformation = _personalinformation_;
  }));

  it('should do something', function () {
    expect(!!personalinformation).toBe(true);
  });

});
