'use strict';

describe('Service: forgotpasswordfactory', function () {

  // load the service's module
  beforeEach(module('foodieFrontApp'));

  // instantiate service
  var forgotpasswordfactory;
  beforeEach(inject(function (_forgotpasswordfactory_) {
    forgotpasswordfactory = _forgotpasswordfactory_;
  }));

  it('should do something', function () {
    expect(!!forgotpasswordfactory).toBe(true);
  });

});
