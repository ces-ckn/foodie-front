'use strict';

describe('Service: homepagefactory', function () {

  // load the service's module
  beforeEach(module('foodieFrontApp'));

  // instantiate service
  var homepagefactory;
  beforeEach(inject(function (_homepagefactory_) {
    homepagefactory = _homepagefactory_;
  }));

  it('should do something', function () {
    expect(!!homepagefactory).toBe(true);
  });

});
