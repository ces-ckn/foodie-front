'use strict';

describe('Service: boxdetail', function () {

  // load the service's module
  beforeEach(module('foodieFrontApp'));

  // instantiate service
  var boxdetail;
  beforeEach(inject(function (_boxdetail_) {
    boxdetail = _boxdetail_;
  }));

  it('should do something', function () {
    expect(!!boxdetail).toBe(true);
  });

});
