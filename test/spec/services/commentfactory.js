'use strict';

describe('Service: commentFactory', function () {

  // load the service's module
  beforeEach(module('foodieFrontApp'));

  // instantiate service
  var commentFactory;
  beforeEach(inject(function (_commentFactory_) {
    commentFactory = _commentFactory_;
  }));

  it('should do something', function () {
    expect(!!commentFactory).toBe(true);
  });

});
