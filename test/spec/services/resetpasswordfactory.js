'use strict';

describe('Service: resetpasswordfactory', function () {

  // load the service's module
  beforeEach(module('foodieFrontApp'));

  // instantiate service
  var resetpasswordfactory;
  beforeEach(inject(function (_resetpasswordfactory_) {
    resetpasswordfactory = _resetpasswordfactory_;
  }));

  it('should do something', function () {
    expect(!!resetpasswordfactory).toBe(true);
  });

});
