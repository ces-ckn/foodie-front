'use strict';

describe('Service: favoritedish', function () {

  // load the service's module
  beforeEach(module('foodieFrontApp'));

  // instantiate service
  var favoritedish;
  beforeEach(inject(function (_favoritedish_) {
    favoritedish = _favoritedish_;
  }));

  it('should do something', function () {
    expect(!!favoritedish).toBe(true);
  });

});
