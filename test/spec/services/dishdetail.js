'use strict';

describe('Service: dishdetail', function () {

  // load the service's module
  beforeEach(module('foodieFrontApp'));

  // instantiate service
  var dishdetail;
  beforeEach(inject(function (_dishdetail_) {
    dishdetail = _dishdetail_;
  }));

  it('should do something', function () {
    expect(!!dishdetail).toBe(true);
  });

});
