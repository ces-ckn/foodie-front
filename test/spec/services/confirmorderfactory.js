'use strict';

describe('Service: confirmOrderFactory', function () {

  // load the service's module
  beforeEach(module('foodieFrontApp'));

  // instantiate service
  var confirmOrderFactory;
  beforeEach(inject(function (_confirmOrderFactory_) {
    confirmOrderFactory = _confirmOrderFactory_;
  }));

  it('should do something', function () {
    expect(!!confirmOrderFactory).toBe(true);
  });

});
