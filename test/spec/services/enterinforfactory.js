'use strict';

describe('Service: enterInforFactory', function () {

  // load the service's module
  beforeEach(module('foodieFrontApp'));

  // instantiate service
  var enterInforFactory;
  beforeEach(inject(function (_enterInforFactory_) {
    enterInforFactory = _enterInforFactory_;
  }));

  it('should do something', function () {
    expect(!!enterInforFactory).toBe(true);
  });

});
