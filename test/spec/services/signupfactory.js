'use strict';

describe('Service: signupfactory', function () {

  // load the service's module
  beforeEach(module('foodieFrontApp'));

  // instantiate service
  var signupfactory;
  beforeEach(inject(function (_signupfactory_) {
    signupfactory = _signupfactory_;
  }));

  it('should do something', function () {
    expect(!!signupfactory).toBe(true);
  });

});
