'use strict';

describe('Controller: FoodpreferenceCtrl', function () {

  // load the controller's module
  beforeEach(module('foodieFrontApp'));

  var FoodpreferenceCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    FoodpreferenceCtrl = $controller('FoodpreferenceCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
