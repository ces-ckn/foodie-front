'use strict';

describe('Controller: OnthemenuCtrl', function () {

  // load the controller's module
  beforeEach(module('foodieFrontApp'));

  var OnthemenuCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OnthemenuCtrl = $controller('OnthemenuCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(OnthemenuCtrl.awesomeThings.length).toBe(3);
  });
});
