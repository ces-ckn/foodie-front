'use strict';

describe('Controller: HistoryorderCtrl', function () {

  // load the controller's module
  beforeEach(module('foodieFrontApp'));

  var HistoryorderCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    HistoryorderCtrl = $controller('HistoryorderCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
