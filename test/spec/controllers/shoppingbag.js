'use strict';

describe('Controller: ShoppingbagCtrl', function () {

  // load the controller's module
  beforeEach(module('foodieFrontApp'));

  var ShoppingbagCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ShoppingbagCtrl = $controller('ShoppingbagCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
