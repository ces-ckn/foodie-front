'use strict';

describe('Controller: InstructionCtrl', function () {

  // load the controller's module
  beforeEach(module('foodieFrontApp'));

  var InstructionCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    InstructionCtrl = $controller('InstructionCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(InstructionCtrl.awesomeThings.length).toBe(3);
  });
});
