'use strict';

describe('Controller: OrderstatusCtrl', function () {

  // load the controller's module
  beforeEach(module('foodieFrontApp'));

  var OrderstatusCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OrderstatusCtrl = $controller('OrderstatusCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
