'use strict';

describe('Controller: EmailconfirmCtrl', function () {

  // load the controller's module
  beforeEach(module('foodieFrontApp'));

  var EmailconfirmCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EmailconfirmCtrl = $controller('EmailconfirmCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
