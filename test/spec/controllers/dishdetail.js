'use strict';

describe('Controller: DishdetailCtrl', function () {

  // load the controller's module
  beforeEach(module('foodieFrontApp'));

  var DishdetailCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DishdetailCtrl = $controller('DishdetailCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(DishdetailCtrl.awesomeThings.length).toBe(3);
  });
});
