'use strict';

describe('Controller: DeliveryaddreesCtrl', function () {

  // load the controller's module
  beforeEach(module('foodieFrontApp'));

  var DeliveryaddreesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DeliveryaddreesCtrl = $controller('DeliveryaddreesCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
