'use strict';

describe('Controller: EnterinforCtrl', function () {

  // load the controller's module
  beforeEach(module('foodieFrontApp'));

  var EnterinforCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EnterinforCtrl = $controller('EnterinforCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
