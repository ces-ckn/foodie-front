'use strict';

describe('Controller: UnsubscribeCtrl', function () {

  // load the controller's module
  beforeEach(module('foodieFrontApp'));

  var UnsubscribeCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    UnsubscribeCtrl = $controller('UnsubscribeCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
