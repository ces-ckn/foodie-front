'use strict';

describe('Controller: BoxdetailctrlCtrl', function () {

  // load the controller's module
  beforeEach(module('foodieFrontApp'));

  var BoxdetailctrlCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    BoxdetailctrlCtrl = $controller('BoxdetailctrlCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(BoxdetailctrlCtrl.awesomeThings.length).toBe(3);
  });
});
