'use strict';

describe('Filter: onthemenu', function () {

  // load the filter's module
  beforeEach(module('foodieFrontApp'));

  // initialize a new instance of the filter before each test
  var onthemenu;
  beforeEach(inject(function ($filter) {
    onthemenu = $filter('filterByType');
  }));

  it('should return the input prefixed with "onthemenu filter:"', function () {
    var text = 'angularjs';
    expect(onthemenu(text)).toBe('onthemenu filter: ' + text);
  });

});
