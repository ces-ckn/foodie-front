'use strict';

describe('Filter: shoppingpage', function () {

  // load the filter's module
  beforeEach(module('foodieFrontApp'));

  // initialize a new instance of the filter before each test
  var shoppingpage;
  beforeEach(inject(function ($filter) {
    shoppingpage = $filter('shoppingpage');
  }));

  it('should return the input prefixed with "shoppingpage filter:"', function () {
    var text = 'angularjs';
    expect(shoppingpage(text)).toBe('shoppingpage filter: ' + text);
  });

});
