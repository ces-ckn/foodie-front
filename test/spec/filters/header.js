'use strict';

describe('Filter: header', function () {

  // load the filter's module
  beforeEach(module('foodieFrontApp'));

  // initialize a new instance of the filter before each test
  var header;
  beforeEach(inject(function ($filter) {
    header = $filter('header');
  }));

  it('should return the input prefixed with "header filter:"', function () {
    var text = 'angularjs';
    expect(header(text)).toBe('header filter: ' + text);
  });

});
