'use strict';

describe('Directive: commentdirective', function () {

  // load the directive's module
  beforeEach(module('foodieFrontApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<commentdirective></commentdirective>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the commentdirective directive');
  }));
});
