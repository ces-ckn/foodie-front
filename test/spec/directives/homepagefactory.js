'use strict';

describe('Directive: homepagefactory', function () {

  // load the directive's module
  beforeEach(module('foodieFrontApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<homepagefactory></homepagefactory>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the homepagefactory directive');
  }));
});
