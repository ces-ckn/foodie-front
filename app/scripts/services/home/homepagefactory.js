'use strict';

/**
 * @ngdoc service
 * @name foodieFrontApp.homepagefactory
 * @description
 * # homepagefactory
 * Service in the foodieFrontApp.
 */
angular.module('foodieFrontApp')
  .service('homepagefactory', function ($http,AppConfig) {
    var homepageService = {};

    homepageService.getSupplier=function(){
      return $http({
          method: "GET", 
          url: AppConfig.api.session.supplier
        });
    };
    return homepageService;
  });
