'use strict';

/**
 * @ngdoc service
 * @name foodieFrontApp.favoritedish
 * @description
 * # favoritedish
 * Service in the foodieFrontApp.
 */
angular.module('foodieFrontApp')
  .service('favoritedishfactory', function ($http,AppConfig) {
    var favoritedishservice={};

    // get list dish in favorite list
     favoritedishservice.getDishlist=function(token){
      return $http({
          method: "GET",
          headers: {
            'X-Spree-Token': token
          },
          // url: "http://192.168.1.126:3000/api/forgot_password"
          url:AppConfig.api.information.favorite
    });
    };

    // delete dish in favorite list
    favoritedishservice.UnlikeDish=function(token,sendingData){
      return $http({
          method: "DELETE",
          data: sendingData,
          headers: {
            'X-Spree-Token': token,
            'Content-Type': 'application/json'
          },
          url: AppConfig.api.products.likes
      });
    };
    favoritedishservice.getDeliveryHour = function(){
      return $http({
          method: "GET",
          url: AppConfig.api.order.hours
      });
    };
    return favoritedishservice;
  });
