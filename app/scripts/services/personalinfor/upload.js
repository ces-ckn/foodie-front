'use strict';

/**
 * @ngdoc service
 * @name foodieFrontApp.upload
 * @description
 * # upload
 * Factory in the foodieFrontApp.
 */
angular.module('foodieFrontApp')
  .factory('UploadFactory',['$q','$http','AppConfig', function ($q,$http,AppConfig) {
    var UploadService = {};
    /*Choose url and method for upload avatar*/
    var chooseMethod = function(user_id,img_id){
      if(img_id == -1){
        var app = {
          url : AppConfig.api.information.avatar + user_id + "/user_images",
          method : "POST"
        }
        return app;
      }else{
        app = {
          url : AppConfig.api.information.avatar + user_id + "/user_images/" + img_id,
          method : "PUT"
        }
        return app;
      }
    } 
    UploadService.uploadAvatar = function(file,id,img_id){
      var fd = new FormData();
      fd.append('user_image[attachment]', file);
      var app = chooseMethod(id,img_id);
      return $http({
        method: app.method,
        url : app.url,
        data: fd,
        transformRequest: angular.identity,
        headers: {
          'Content-Type': undefined,
          'processData' : false
        }
      }).then(
      function(response){
        return response;
      },
      function(response){
        return $q.reject(response.data);
      });
    }
    return UploadService;
  }]);
