'use strict';

/**
 * @ngdoc service
 * @name foodieFrontApp.personalinformation
 * @description
 * # personalinformation
 * Factory in the foodieFrontApp.
 */
angular.module('foodieFrontApp')
  .factory('PersonalInformationFactory',function ($http,AppConfig) {

    var PersonalInformationService = {};
    PersonalInformationService.getInformation = function(token){
      return $http({
          method: "GET",
          headers: {
            'X-Spree-Token': token
          },
          url: AppConfig.api.information.personal
      });
    };
    PersonalInformationService.saveInformation = function(token,data){
      return $http({
          method: "PUT",
          headers: {
            'X-Spree-Token': token,
            'Content-Type': 'application/json'
          },
          data:data,
          url: AppConfig.api.information.updatepersonal
      });
    };
     PersonalInformationService.getUseraddress = function(token){
      return $http({
          method: "GET",
          headers: {
            'X-Spree-Token': token
          },
          url: AppConfig.api.information.useraddress
      });
    };
    PersonalInformationService.SetDelivery=function(token,data,id){
        return $http({
          method: "PUT",
          headers: {
            'X-Spree-Token': token,
            'Content-Type': 'application/json'
          },
          data:data,
          url: AppConfig.api.information.address+"/"+id
      });
    };
    PersonalInformationService.CreateAddress=function(token,data){
        return $http({
          method: "POST",
          headers: {
            'X-Spree-Token': token,
            'Content-Type': 'application/json'
          },
          data:data,
          url: AppConfig.api.information.address
      });
    };
    PersonalInformationService.getFoodPreference=function(token){
      return $http({
          method: "GET",
          headers: {
            'X-Spree-Token': token
          },
          url: AppConfig.api.information.foodpreference
      });
    };
     PersonalInformationService.get=function(token){
      return $http({
          method: "POST",
          headers: {
            'X-Spree-Token': token
          },
          url: AppConfig.api.information.foodpreference+"/check"
      });
    };
    PersonalInformationService.getFoodIngredient=function(data){
       return $http({
          method: "POST",
          data:data,
          url: AppConfig.api.products.ingredients+"/search_by_name"
      });
    };
    PersonalInformationService.addIngredient=function(sendingdata,token,id){
      return $http({
          method: "PUT",
          headers: {
            'X-Spree-Token': token,
            'Content-Type': 'application/json'
          },
          data:sendingdata,
          url: AppConfig.api.information.foodpreference+"/id"
      });
    };
    PersonalInformationService.getOrderStatus=function(token){
      return $http({
          method: "GET",
          headers: {
            'X-Spree-Token': token,
          },
          url: AppConfig.api.order.orderstatus
      });
    };
     PersonalInformationService.getHistoryOrder=function(token){
      return $http({
          method: "GET",
          headers: {
            'X-Spree-Token': token,
          },
          url: AppConfig.api.order.historyorder
      });
    };
    return PersonalInformationService;
  });
