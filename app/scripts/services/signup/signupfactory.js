'use strict';

/**
 * @ngdoc service
 * @name foodieFrontApp.signupfactory
 * @description
 * # signupfactory
 * Service in the foodieFrontApp.
 */
angular.module('foodieFrontApp')
  .service('signupfactory', function ($http,AppConfig) {
   var signupService = {};

    signupService.signup=function(jsonsignup){
      console.log(AppConfig.api.session.signup);
      return $http({
          method: "POST",
          data:jsonsignup,
          contentType: "application/json",
          url: AppConfig.api.session.signup
    });
    };
    return signupService;
  });
