'use strict';

/**
 * @ngdoc service
 * @name foodieFrontApp.factory
 * @description
 * # factory
 * Factory in the foodieFrontApp.
 */
angular.module('foodieFrontApp')
  .factory('OnTheMenuFactory', ['$q','$http','AppConfig',function ($q,$http,AppConfig) {
  	var onTheMenuService = {};
    
    onTheMenuService.getDailyBoxes = function(from,to,token){
      return $http({
        method: "POST",
        headers: {
          'X-Spree-Token': token
        },
        data : {date_from : from,date_to: to},
        url: AppConfig.api.products.boxes.dailyboxes
      }).then(
        function(response){
          return response;
        },
        function(response){
          return $q.reject(response.data);
        });;
    };
    onTheMenuService.getWeeklyBox = function(from,to,token){
      return $http({
        method: "POST",
        headers: {
          'X-Spree-Token': token
        },
        data : {date_from : from,date_to: to},
        url: AppConfig.api.products.boxes.weeklybox
      }).then(
        function(response){
          return response;
        },
        function(response){
          return $q.reject(response.data);
        });;
    };
  	onTheMenuService.getTypeOfDish = function(){
      return $http({
          method: "GET",
          url: AppConfig.api.products.types
        });
    };
    onTheMenuService.getProduct = function(token){
  		return $http({
          method: "GET",
          url: AppConfig.api.products.products,
          headers: {
            'X-Spree-Token': token
          }
        });
  	};

  	return onTheMenuService;
  }]);
