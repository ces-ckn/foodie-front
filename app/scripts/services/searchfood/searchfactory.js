'use strict';

/**
 * @ngdoc service
 * @name foodieFrontApp.searchFactory
 * @description
 * # searchFactory
 * Factory in the foodieFrontApp.
 */
angular.module('foodieFrontApp')
  .factory('searchFactory',['$http','AppConfig', function ($http,AppConfig) {
    var searchService = {};
    searchService.SearchFood = function(sendingData){
      return $http({
        method: "POST",
        data: sendingData,
        url: AppConfig.api.products.products+"/search_by_name"
      }).then(
        function(response){
          return response;
        },
        function(response){
          return $q.reject(response.data);
        });
    };
    return searchService;
  }]);
