'use strict';

/**
 * @ngdoc service
 * @name foodieFrontApp.loginFactory
 * @description
 * # loginFactory
 * Factory in the foodieFrontApp.
 */
angular.module('foodieFrontApp')
  .factory('loginFactory', function ($http,AppConfig) {
    var loginService = {};

    loginService.login=function(jsonlogin){
      return $http({
          method: "POST",
          data:{},  
          params : jsonlogin,
          /*url: "http://192.168.1.126:3000/api/login"*/
          url: AppConfig.api.session.login
        });
    };
    return loginService;
  });
