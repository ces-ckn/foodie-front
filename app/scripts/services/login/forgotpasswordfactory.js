'use strict';

/**
 * @ngdoc service
 * @name foodieFrontApp.forgotpasswordfactory
 * @description
 * # forgotpasswordfactory
 * Service in the foodieFrontApp.
 */
angular.module('foodieFrontApp')
  .service('forgotpasswordfactory', function ($http,AppConfig) {
    // AngularJS will instantiate a singleton by calling "new" on this function
   var forgotpasswordservice = {};

    forgotpasswordservice.ResetPassword=function(emailSend){
      console.log(emailSend);
      return $http({
          method: "POST",
          data:emailSend,
          // url: "http://192.168.1.126:3000/api/forgot_password"
          url:AppConfig.api.email.forgotpassword
    });
    };
    return forgotpasswordservice;
  });
