'use strict';

/**
 * @ngdoc service
 * @name foodieFrontApp.resetpasswordfactory
 * @description
 * # resetpasswordfactory
 * Service in the foodieFrontApp.
 */
angular.module('foodieFrontApp')
  .service('resetpasswordfactory', function ($http,AppConfig) {
   var resetpasswordService = {};

    resetpasswordService.ResetPass=function(jsonpassword){
      // console.log(AppConfig.api.reset_password);
      return $http({
          method: "PUT",
          data:jsonpassword,
          contentType: "application/json",
          // url: "http://192.168.1.126:3000/api/reset_password/"
          url: AppConfig.api.email.resetpasword

    });
    };
    return resetpasswordService;
  });
