'use strict';

/**
 * @ngdoc service
 * @name foodieFrontApp.commentFactory
 * @description
 * # commentFactory
 * Factory in the foodieFrontApp.
 */
angular.module('foodieFrontApp')
  .factory('LikeFactory', ['$http','AppConfig', function ($http,AppConfig) {
    var likeService = {};
    likeService.saveLikes = function(sendingData,token){
      return $http({
          method: "POST",
          data: sendingData,
          headers: {
            'X-Spree-Token': token
          },
          url: AppConfig.api.products.likes
      });
    };
    likeService.saveDislikes = function(sendingData,token){
      return $http({
          method: "DELETE",
          data: sendingData,
          headers: {
            'X-Spree-Token': token,
            'Content-Type': 'application/json'
          },
          url: AppConfig.api.products.likes
      });
    };
    likeService.getDeliveryHour = function(){
      return $http({
          method: "GET",
          url: AppConfig.api.order.hours
      });
    };
    return likeService;
  }]);
