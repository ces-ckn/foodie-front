'use strict';

/**
 * @ngdoc service
 * @name foodieFrontApp.boxdetail
 * @description
 * # boxdetail
 * Factory in the foodieFrontApp.
 */
angular.module('foodieFrontApp')
  .factory('BoxDetailFactory', ['$q','$http','AppConfig',function ($q,$http,AppConfig){
    var BoxDetailService = {};
    BoxDetailService.getBox = function(type,date,token){
      function selectUrl(box){
        if(box == "daily_box"){
          return AppConfig.api.products.boxes.dailyboxes;
        }else if(box == "weekly_box"){
          return AppConfig.api.products.boxes.weeklybox;
        }else{
          console.log("Wrong URL");
        }
      }
      return $http({
          method: "POST",
          headers: {
            'X-Spree-Token': token,
            'Content-Type': 'application/json'
          },
          data: date,
          url: selectUrl(type)
      }).then(
        function(response){
          return response;
        },
        function(response){
          return $q.reject(response.data);
        });
    };
    return BoxDetailService;
  }]);
