'use strict';

/**
 * @ngdoc service
 * @name foodieFrontApp.dishdetail
 * @description
 * # dishdetail
 * Factory in the foodieFrontApp.
 */
angular.module('foodieFrontApp')
  .factory('DishDetailFactory', ['$q','$http','AppConfig',function ($q,$http,AppConfig) {
    
    var DishDetailService = {};
    DishDetailService.getDish = function(param,token){
      return $http({
          method: "GET",
          headers: {
            'X-Spree-Token': token
          },
          url: AppConfig.api.products.products+"/"+param
      });
    };
    DishDetailService.getDishesToday = function(sendingData,token){
      return $http({
          method: "POST",
          headers: {
            'X-Spree-Token': token
          },
          url: AppConfig.api.products.products+"/products_in_days",
          data : sendingData
      }).then(
        function(response){
          return response;
        },
        function(response){
          return $q.reject(response.data);
        });;
    };
    DishDetailService.getIngredients = function(param){
      return $http({
          method: "GET",
          url: AppConfig.api.products.products+"/"+param+"/ingredients"
      }).then(
        function(response){
          return response;
        },
        function(response){
          return $q.reject(response.data);
        });;
    };
    
    DishDetailService.getNutritions = function(param){
      return $http({
          method: "GET",
          url: AppConfig.api.products.products+"/"+param+"/nutritions"
      }).then(
        function(response){
          return response;
        },
        function(response){
          return $q.reject(response.data);
        });;
    };
    return DishDetailService;
  }]);
