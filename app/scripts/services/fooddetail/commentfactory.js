'use strict';

/**
 * @ngdoc service
 * @name foodieFrontApp.commentFactory
 * @description
 * # commentFactory
 * Factory in the foodieFrontApp.
 */
angular.module('foodieFrontApp')
  .factory('commentFactory', ['$http','AppConfig','localStorageService', function ($http,AppConfig,localStorageService) {
    var commentService = {};
    /*Choose url for box or dish*/
    var urlApi = function(type){
      if(type.type == "dish"){
        return AppConfig.api.products.products+"/"+type.id+"/comments"
      }else if(type.type == "box"){ 
        return AppConfig.api.products.boxes.allboxes+"/"+type.id+"/comments"
      }else{
        console.log("Wrong url");
      }
    }
    /* API get all comments*/
    commentService.getComment = function(type,token){
      return $http({
        method: "GET",
        headers: {
          'X-Spree-Token':token
        },
        url: urlApi(type)
      });
    };
    /* API save a comment*/
    commentService.saveComment = function(sendingData,type){
      return $http({
        method: "POST",
        data: sendingData,
        headers: {
          'X-Spree-Token':localStorageService.get('user').spree_api_key,
          'Content-Type': 'application/json'
        },
        url: urlApi(type)
      });
    };

    /* API delete a comment*/
    commentService.deleteComment = function(id){
      return $http({
        method: "DELETE",
        data: id,
        headers: {
          'X-Spree-Token': localStorageService.get('user').spree_api_key,
        },
        url: AppConfig.api.products.comments + "/" + id
      });
    };
    /* API edit a comment*/
    commentService.updateComment = function(sendingData,type,id){
      return $http({
        method: "PUT",
        data: sendingData,
        headers: {
          'X-Spree-Token': localStorageService.get('user').spree_api_key,
          'Content-Type': 'application/json'
        },
        url: AppConfig.api.products.comments + "/" + id
      });
    };
    return commentService;
  }]);
