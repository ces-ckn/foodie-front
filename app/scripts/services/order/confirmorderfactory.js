'use strict';

/**
 * @ngdoc service
 * @name foodieFrontApp.confirmOrderFactory
 * @description
 * # confirmOrderFactory
 * Service in the foodieFrontApp.
 */
angular.module('foodieFrontApp')
  .service('confirmOrderFactory', function (AppConfig,$http) {
     var ConfirmOrderService= {};
      ConfirmOrderService.order = function(token,idOrder){
      return $http({
          method: "PUT",
          headers: {
            'X-Spree-Token': token,
            'Content-Type': 'application/json'
          },
          url: AppConfig.api.order.checkout+"/"+idOrder+"/advance.json"
      });
    };
    ConfirmOrderService.cancelOrder=function(idOrder,token){
      return $http({
          method: "POST",
          headers: {
            'X-Spree-Token': token
          },
          url: AppConfig.api.order.cancelorder+"/"+idOrder+"/cancel"
      });
    };
    return ConfirmOrderService;
  });
