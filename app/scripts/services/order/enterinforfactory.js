'use strict';

/**
 * @ngdoc service
 * @name foodieFrontApp.enterInforFactory
 * @description
 * # enterInforFactory
 * Service in the foodieFrontApp.
 */
angular.module('foodieFrontApp')
  .service('enterInforFactory', function ($http,AppConfig) {
     var EnterInforService= {};
    /*
    * @function
    * @desc 
    * @param
    * @return
    */
      EnterInforService.getUserAddress = function(token){
      return $http({
          method: "GET",
          headers: {
            'X-Spree-Token': token
          },
          url: AppConfig.api.information.useraddress
      });
    };
     /*
    * @function
    * @desc 
    * @param
    * @return
    */
    EnterInforService.createAddress=function(token,data){
        return $http({
          method: "POST",
          headers: {
            'X-Spree-Token': token,
            'Content-Type': 'application/json'
          },
          data:data,
          url: AppConfig.api.information.address
      });
    };
     /*
    * @function
    * @desc 
    * @param
    * @return
    */
    EnterInforService.getIngredients=function(text){
      return $http({
          method: "POST",
          data:text,
          url: AppConfig.api.products.ingredients+"/search_by_name"
      });
    };
    EnterInforService.getDeliveryHour = function(){
      return $http({
          method: "GET",
          url: AppConfig.api.order.hours
      });
    };
     /*
    * @function
    * @desc 
    * @param
    * @return
    */
    EnterInforService.editAddress=function(token,data,id){
        return $http({
          method: "PUT",
          headers: {
            'X-Spree-Token': token,
            'Content-Type': 'application/json'
          },
          data:data,
          url: AppConfig.api.information.address+"/"+id
      });
    };
     /*
    * @function
    * @desc 
    * @param
    * @return
    */
    EnterInforService.createOrder = function(data,token){
      return $http({
          method: "POST",
          headers: {
            'X-Spree-Token': token,
            'Content-Type': 'application/json'
          },
          data:data,
          url: AppConfig.api.order.orders
      });
    };
     /*
    * @function
    * @desc 
    * @param
    * @return
    */
     EnterInforService.nextStep = function(idOrder,token){
      return $http({
          method: "PUT",
          headers: {
            'X-Spree-Token': token
          },
          url: AppConfig.api.order.checkout+"/"+idOrder+"/next.json"
      });
    };
     /*
    * @function
    * @desc 
    * @param
    * @return
    */
    EnterInforService.updateOrder = function(data,token,idOrder){
      return $http({
          method: "PUT",
          headers: {
            'X-Spree-Token': token,
            'Content-Type': 'application/json'
          },
          data:data,
          url: AppConfig.api.order.checkout+"/"+idOrder
      });
    };
     /*
    * @function
    * @desc 
    * @param
    * @return
    */
    EnterInforService.shipment = function(data,token){
      return $http({
          method: "POST",
          headers: {
            'X-Spree-Token': token,
            'Content-Type': 'application/json'
          },
          data:data,
          url: AppConfig.api.order.shipments+"/split"
      });
    };
     /*
    * @function
    * @desc 
    * @param
    * @return
    */
    EnterInforService.setPaymentMethod = function(data,token,idOrder){
      return $http({
          method: "PUT",
          headers: {
            'X-Spree-Token': token,
            'Content-Type': 'application/json'
          },
          data:data,
          url: AppConfig.api.order.checkout+"/"+idOrder
      });
    };
     /*
    * @function
    * @desc 
    * @param
    * @return
    */
    EnterInforService.getFoodPreference=function(token){
      return $http({
          method: "POST",
          headers: {
            'X-Spree-Token': token
          },
          url: AppConfig.api.information.foodpreference+"/check"
      });
    };
     /*
    * @function
    * @desc 
    * @param
    * @return
    */
     EnterInforService.get=function(token){
      return $http({
          method: "GET",
          headers: {
            'X-Spree-Token': token
          },
          url: AppConfig.api.information.foodpreference
      });
    };
     /*
    * @function
    * @desc 
    * @param
    * @return
    */
    EnterInforService.addFoodPreference=function(sendingdata,token){
      return $http({
            method: "POST",
            headers: {
              'X-Spree-Token': token,
              'Content-Type': 'application/json'
            },
            data:sendingdata,
            url: AppConfig.api.information.foodpreference
        });
      };
    return EnterInforService;
  });
