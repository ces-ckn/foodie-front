'use strict';

/**
 * @ngdoc service
 * @name foodieFrontApp.sendSubscribeFactory
 * @description
 * # sendSubscribeFactory
 * Service in the foodieFrontApp.
 */
angular.module('foodieFrontApp')
  .service('sendSubscribeFactory', function ($http,AppConfig) {
   var SubscribeEmailService = {};
    SubscribeEmailService.confirmEmail=function(data){
       return $http({
          method: "PUT",
          data:data,
          contentType: "application/json",
    
          url: AppConfig.api.email.confirm_subscription
    });
    };
    SubscribeEmailService.sendSubscribe=function(emailSend){
      console.log(emailSend);
      return $http({
          method: "POST",
          data:emailSend,
          contentType: "application/json",
           url: AppConfig.api.email.subscribe
    });
    };
    SubscribeEmailService.UnSubscribe=function(emailSend){
      console.log(emailSend);
      return $http({
          method: "PUT",
          data:emailSend,
          contentType: "application/json",
          url: AppConfig.api.email.unsubscribe
    });
    };
    return SubscribeEmailService;
  });
