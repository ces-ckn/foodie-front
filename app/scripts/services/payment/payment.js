'use strict';

/**
 * @ngdoc service
 * @name foodieFrontApp.paypal
 * @description
 * # paypal
 * Factory in the foodieFrontApp.
 */
angular.module('foodieFrontApp')
  .factory('PaymentFactory', ['AppConfig','$http',function (AppConfig,$http) {
    var PaymentService = {};
    PaymentService.paybyPaypal = function(param){
      return $http({
        method: "POST",
        url: AppConfig.api.payment.paypal + param +"?payment_method_id=3"
      });
    };
    PaymentService.addMoneyByPaypal = function(token,amount){
      return $http({
        method: "POST",
        url: AppConfig.api.payment.addfund,
        headers: {
          'X-Spree-Token': token
        },   
        data : amount
      });
    }
    PaymentService.addMoneyByBitCoin = function(token,amount){
      return $http({
        method: "POST",
        url: AppConfig.api.payment.bitcoin + "add_fund",
        headers: {
          'X-Spree-Token': token
        },   
        data : amount
      });
    }
    PaymentService.checkMoney = function(token){
      return $http({
        method: "GET",
        url: AppConfig.api.information.user,
        headers: {
          'X-Spree-Token': token
        }
      });
    }
    PaymentService.payByAccount = function(orderNumber,token,paymentId){
      return $http({
        method: "PUT",
        url: AppConfig.api.payment.account + orderNumber + "/payments/" + paymentId + "/capture",
        headers: {
          'X-Spree-Token': token
        }
      });
    }
    PaymentService.paybyBitCoin = function(orderNumber){
      return $http({
        method: "GET",
        url: AppConfig.api.payment.bitcoin + "invoice/new/" + orderNumber,
      });
    }
    PaymentService.payByCash = function(token,idOrder){
      return $http({
          method: "PUT",
          headers: {
            'X-Spree-Token': token,
            'Content-Type': 'application/json'
          },
          url: AppConfig.api.order.checkout+"/"+idOrder+"/advance.json"
      });
    };
    return PaymentService;
  }]);
