'use strict';

/**
 * @ngdoc overview
 * @name foodieFrontApp
 * @description
 * # foodieFrontApp
 *
 * Main module of the application.
 */
angular
  .module('foodieFrontApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'ui.router',
    'LocalStorageModule',
    'djds4rce.angular-socialshare',
    'smoothScroll',
    'angular-cache',
    'angular-intro'
  ])
  .config(['localStorageServiceProvider', function(localStorageServiceProvider){
    localStorageServiceProvider.setPrefix('ls');
  }])
  .config(function ($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('app', {
      url: "/main",
      views : {
        "header" : {
          templateUrl : "/views/home/header.html",
          // template: "build project",
          controller: "MainCtrl"
          /*controller: "LoginCtrl"*/
        },
        "body" : {
          templateUrl : "/views/home/main.html",
          controller: "UserHomePageCtrl"
        },
        "footer":{
          templateUrl:"views/home/footer.html",
           controller:"FooterCtrl"
        }
      },
      data: {
        requireLogin : false,
        impossibleType:false
      }
    })
    .state('app.onthemenu', {
      url: "/onthemenu",
      views : {
        "body@" : {
          templateUrl : "/views/onthemenu/onthemenu.html",
          controller : "OnthemenuCtrl"
        }
      },
      data: {
        requireLogin : false,
        impossibleType:false
      }
    })
    .state('app.enterinfor', {
      url: "/enter_infor",
      views : {
        "body@" : {
          templateUrl : "/views/order/enterinfor.html",
          controller : "EnterinforCtrl"
        }
      },
      data: {
        requireLogin : true,
        impossibleType:false
      }
    })
    .state('app.prepaid', {
      url: "/prepaidcart",
      views : {
        "body@" : {
          templateUrl : "/views/order/prepaidCart.html",
          controller : "PrepaidCartCtrl"
        }
      },
      data: {
        requireLogin : true,
        impossibleType:false
      }
    })
    .state('app.confirmorder', {
      url: "/confirm_order",
      views : {
        "body@" : {
          templateUrl : "/views/order/confirm_order.html",
          controller : "ConfirmorderCtrl"
        }
      },
      data: {
        requireLogin : true,
        impossibleType:false
      }
    })
    .state('app.confirmorder.revieworder', {
      url: "/reviewOrder",
      views : {
        "body@" : {
          templateUrl : "/views/order/reviewOrder.html",
          controller : "ReviewOrderCtrl"
        }
      },
      data: {
        requireLogin : true,
        impossibleType:false
      }
    })
    .state('app.wishlist', {
      url: "/wishlist",
      views : {
        "body@" : {
          templateUrl : "/views/personalinfor/wishlist.html",
          controller : "WishlistCtrl"
        }
      },
      data: {
        requireLogin : true,
        impossibleType:true
      }
    })
    .state('app.bag', {
      url: "/bag",
      views : {
        "body@" : {
          templateUrl : "/views/shoppingbag/shoppingbag.html",
          controller : "ShoppingbagCtrl"
        }
      },
      data: {
        requireLogin : false,
        impossibleType:false
      }
    })
    .state('app.resetpassword',{
      url:"/reset_password?:id&:token",
      views:{
        "body@":{
          templateUrl : "/views/login/resetpassword.html",
          controller : "ResetpasswordCtrl"
        }
      },
      data: {
        requireLogin : false,
        impossibleType:true
      }
    })
    .state('app.confirmsubscribe',{
      url:"/confirm_subscrition?:id&:token",
      views:{
        "body@":{
          templateUrl : "/views/subscribe/confirm_subscription.html",
          controller : "EmailconfirmCtrl"
        }
      },
      data: {
        requireLogin : false,
        impossibleType:true
      }
    })
    .state('app.unsubscribe',{
      url:"/confirm_unsubscrition?:id&:token",
      views:{
        "body@":{
          templateUrl : "/views/subscribe/unsubscribe.html",
          controller : "UnsubscribeCtrl"
        }
      },
      data: {
        requireLogin : false,
        impossibleType:true
      }
    })
    .state('app.editprofile',{
      url:"/editprofile",
      views:{
        "body@":{
          templateUrl : "/views/personalinfor/editprofile.html",
          controller : "OrderstatusCtrl"
        }
      },
      data: {
        requireLogin : true,
        impossibleType:false
      }
    })
    .state('app.about',{
      url:"/about",
      views:{
        "body@":{
          templateUrl : "/views/introduction/about.html",
          controller : "AboutCtrl"
        }
      },
      data: {
        requireLogin : false,
        impossibleType:false
      }
    })
    .state('app.login',{
      url:"/login",
      views:{
        "body@":{
          templateUrl : "/views/login/login.html",
          controller : "LoginCtrl"
        }
      },
      data: {
        requireLogin : false,
        impossibleType:false
      }
    })
    .state('app.login.forgotpassword',{
      url:"/forgotpassword",
      views:{
        "body@":{
          templateUrl : "/views/login/forgot_password.html",
           controller : "ForgotPasswordCtrl"
        }
      },
      data: {
        requireLogin : false,
        impossibleType:true
      }
    })
    .state('app.login.register',{
      url:"/register",
      views:{
        "body@":{
          templateUrl : "/views/signup/register.html",
          controller : "SignupcontrollerCtrl"
        }
      },
      data: {
        requireLogin : false,
        impossibleType:false
      }
    })
    .state('app.onthemenu.dish', {
      url: "/dish/:id/:date",
      views : {
        "body@" : {
          templateUrl : "/views/fooddetail/dishdetail.html",
          controller: "DishdetailCtrl"
        }
      },
      data: {
        requireLogin : false,
        impossibleType:false
      }
    })
    .state('app.onthemenu.box', {
      url: "/box/:type/:date",
      views : {
        "body@" : {
          templateUrl : "/views/fooddetail/boxdetail.html",
          controller: "BoxDetailCtrl"
        }
      },
      data: {
        requireLogin : false,
        impossibleType:false
      }
    })
    .state('app.redirect', {
      url: "/redirect"
    });
    $urlRouterProvider.otherwise("/main");
  })
.run(['$FB','$rootScope','localStorageService','$state',
  function ($FB,$rootScope,localStorageService,$state){
  $FB.init('900800713369520');
  var currentUser = localStorageService.get('user');
  if (currentUser) {
    $rootScope.isLogined = true;
    $rootScope.user_name_account = currentUser;
  } else {
    $rootScope.user_name_account = "";
  }
  $rootScope.lastStateName = "app";
  $rootScope.lastParams = {}; 
  $rootScope.$on('$stateChangeSuccess',function (event,toState, toParams, fromState, fromParams) {
    //console.log(toState)
    $rootScope.lastStateName = fromState.name;
    $rootScope.lastParams = fromParams;
    if(toState.data.requireLogin){
      if(localStorageService.get('user') == null){
        $state.go('app');
      }
    }
    else{
      if(localStorageService.get('user') != null && toState.name!='app.onthemenu' && toState.name!='app.onthemenu.dish'
        && toState.name!='app.confirmsubscribe' && toState.name!='app.unsubscribe' &&toState.name!='app.resetpassword'
        && toState.name!='app.bag' && toState.name!='app.onthemenu.box' && toState.name!='app.about' && toState.name!='app.instruction'){
        $state.go('app');
      }
    }
    if(toState.name == "app.redirect"){
      localStorageService.remove('dishes');
      localStorageService.remove('data');
      $state.go('app');
    }
  });
}]);
