'use strict';

/**
 * @ngdoc function
 * @name foodieFrontApp.controller:HistoryorderCtrl
 * @description
 * # HistoryorderCtrl
 * Controller of the foodieFrontApp
 */
angular.module('foodieFrontApp')
  .controller('HistoryorderCtrl',['$scope','PersonalInformationFactory','localStorageService',
   function ($scope,PersonalInformationFactory,localStorageService) {

    $scope.historyorders=[];
   	$scope.init=function(){
		var CurrentUser=localStorageService.get('user');
    	
    	if(CurrentUser!==null){	
    		var token=CurrentUser.spree_api_key;
	    	PersonalInformationFactory.getHistoryOrder(token).
	    	success(function(data){
	    		console.log(data);
          if(data.count>0){
            $scope.historyorders=data.orders;	
          }
	    	}).
	    	error(function(data){
	    		console.log(data);

	    	});
    	}
   	};
  }]);
