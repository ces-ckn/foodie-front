'use strict';

/**
 * @ngdoc function
 * @name foodieFrontApp.controller:DeliveryaddreesCtrl
 * @description
 * # DeliveryaddreesCtrl
 * Controller of the foodieFrontApp
 */
angular.module('foodieFrontApp')
  .controller('DeliveryaddreesCtrl',['$scope','$rootScope','$sce','$location','$timeout','localStorageService','PersonalInformationFactory',
   function ($scope,$rootScope,$sce,$location,$timeout,localStorageService,PersonalInformationFactory) {
  	 /*******************************************/
        // define country list
       var countries=[
        {
            "id":0,
            "name":"Liên Chiểu"
        },
        {
            "id":1,
            "name":"Hải Châu"
        },
        {
            "id":2,
            "name":"Sơn Trà"
        }
        ,
        {
            "id":3,
            "name":"Thanh Khê"
        },
        {
            "id":4,
            "name":"Cẩm Lệ"
        }
        ];
        $scope.countries=countries;
        $scope.district=0;
        $scope.newdistrict=0;
          /*define variables for delivery address*/
        $scope.errordeliver="";
        $scope.deliver={};
        $scope.isSuccess=false;
        $scope.isError=false;
        $scope.isDeliverSuccess=false;
        $scope.isDeliverError=false;
        var str='<div style="color:red">'+
                '<div><span style="color:#747474">New address:</span></br>'+
                'Not be blank and must be more than 2 characters.(Ex: Home,...)</br></div>'+
                '<span style="color:#747474">First name:</span></br>'+
                'Not be blank and must be more than 2 characters.(Ex: Le,...)</br>'+
                '<span style="color:#747474">Last name:</span></br>'+
                'Not be blank and must be more than 2 characters.(Ex: Lan,...)</br>'+
                '<span style="color:#747474">Phone number:</span></br>'+
                'Not be blank and must be a phone number.(Ex: 0972802476,...)</br>'+
                '<span style="color:#747474">Address:</span></br>'+
                'Not be blank and must be more than 2 characters.(Ex: 03 Ngo Si Lien,...)</br>'+
                '</div>';
        /*$scope.htmlPopover = $sce.trustAsHtml(str);*/
    /*@function
        * @desc
        * @param
        * return
        */
        // get delivery information from api
        $scope.getDeliveryAddress=function(token){
            PersonalInformationFactory.getUseraddress(token).
                success(function(data){
                    if(data.length>=2){
                        $scope.isLength=true;
                    }
                    else{
                    $scope.isLength=false;
                    }
                    if(data.length!=0)
                    {
                        $scope.listaddress=data;
                        $scope.isNull=false;
                        $scope.SetDeliveryaddress(data,data[data.length-1].id);
                    }
                    else
                    {
                    $scope.isNull=true;
                    }
                }).
                    error(function(error){
                    console.log(error);
                });
        };
     // define first value
        /*@function
        * @desc
        * @param
        * return
        */
    	$scope.init = function(){
            if(localStorageService.get("user") != null){
                var CurrentUser = localStorageService.get('user');
                // $scope.user = CurrentUser;
                // get information of personal
                $scope.getDeliveryAddress(CurrentUser.spree_api_key);
            }else{
                // $scope.user = "";
            }
    		
    	};

/* all functions for delivery information*/

        /*@function
        * @desc
        * @param
        * return
        */
        $scope.SetDistrict=function(string){
             var district;
            for(var i=0;i<countries.length;i++){
                if(countries[i].name===string){
                    district=countries[i].id;
                }
            }
            return district;
        };
        // change information of input when change dropdown
        /*@function
        * @desc
        * @param
        * return
        */
        $scope.changeID=function(){
            var id=$scope.home;
            var listaddress=$scope.listaddress;
            $scope.SetDeliveryaddress(listaddress,id);
        };

        /*@function
        * @desc
        * @param
        * return
        */
        $scope.SetDeliveryaddress=function(data,idAddress){
            for(var i=0;i<data.length;i++){
                if(data[i].id==idAddress){
                    console.log(data[i].city);
                    $scope.home=data[i].id;
                    $scope.firstnamedeliver = data[i].firstname;   
                    $scope.lastnamedeliver = data[i].lastname;
                    $scope.district=$scope.SetDistrict(data[i].city);
                    $scope.phonenumber=data[i].phone;
                    $scope.titlehome=data[i].title;
                    $scope.address=data[i].address1;
                }
            }
        };

            /*@function
            * @desc
            * @param
            * return
            */
           var getDistrict=function(index){
            var district="";
            for(var i=0;i<countries.length;i++){
                if(countries[i].id===index){
                    district=countries[i].name;
                }
            }
            return district;
           };

           // set delivery information
            /*@function
            * @desc
            * @param
            * return
            */
           $scope.SetDelivery=function(){
            console.log("vo dc roi");
             var token = localStorageService.get('user').spree_api_key;
             var sendingdata=
             {
                "address":
                {
                    "first_name":$scope.firstnamedeliver, 
                    "last_name":$scope.lastnamedeliver, 
                    "phone":$scope.phonenumber,
                    "address1":$scope.address, 
                    "title":$scope.titlehome, 
                    "city":getDistrict($scope.district)
                 }
             };
             // send data delivery address to api
             var idaddress=$scope.home;
              PersonalInformationFactory.SetDelivery(token,sendingdata,idaddress).
                success(function(data){   
                    $scope.isDeliverSuccess=true;
                    $scope.errordeliver=data[0].messages;
                    $timeout(function() { $scope.isDeliverSuccess=false;}, 3000);
                    $scope.getDeliveryAddress(token);
                }).
                error(function(error){
                    console.log(error);
                    $scope.isDeliverError=true;
                    $scope.errordeliver=error.messages;
                    $timeout(function() { $scope.isDeliverError=false;}, 3000);
                });
           };

           // send data of create address to api
            /*@function
            * @desc
            * @param
            * return
            */
           $scope.SendDataCreate=function(sendingdata,token){
            PersonalInformationFactory.CreateAddress(token,sendingdata).
                success(function(data){   
                    console.log(data);
                    $scope.isDeliverSuccess=true;
                    $scope.errordeliver="You've added address successfully";
                    $timeout(function() { $scope.isDeliverSuccess=false;}, 2000);
                    $scope.getDeliveryAddress(token);
                }).
                error(function(error){
                    $scope.isDeliverError=true;
                    $scope.errordeliver="Error when add address";
                    $timeout(function() { $scope.isDeliverError=false;}, 2000);
                });      
           };

           // create address when had some address
            /*@function
            * @desc
            * @param
            * return
            */
           $scope.CreateExAddress=function(){
            var token = localStorageService.get('user').spree_api_key;
             var iduser=localStorageService.get('user').id;
             var sendingdata=
             {
                "address":
                {
                    "first_name":$scope.newfirstnamedeliver, 
                    "last_name":$scope.newlastnamedeliver, 
                    "phone":$scope.newphonenumber,
                    "address1":$scope.newaddress,
                    "user_id": iduser,
                    "title":$scope.newhome, 
                    "city":getDistrict($scope.newdistrict)
                 }
             };
             $scope.SendDataCreate(sendingdata,token);
           };
            /*@function
            * @desc
            * @param
            * return
            */
           $scope.CreateAddress=function(){
            var token = localStorageService.get('user').spree_api_key;
             var iduser=localStorageService.get('user').id;
             var sendingdata=
             {
                "address":
                {
                    "first_name":$scope.firstnamedeliver, 
                    "last_name":$scope.lastnamedeliver, 
                    "phone":$scope.phonenumber,
                    "address1":$scope.address,
                    "user_id": iduser,
                    "title":$scope.titlehome, 
                    "city":getDistrict($scope.district)
                 }
             };
             $scope.SendDataCreate(sendingdata,token);
           };
  }]);
