'use strict';

/**
 * @ngdoc function
 * @name foodieFrontApp.controller:PersonalinformationCtrl
 * @description
 * # PersonalinformationCtrl
 * Controller of the foodieFrontApp
 */
angular.module('foodieFrontApp')
  .controller('PersonalinformationCtrl',['$rootScope','$location','$scope','$timeout','PersonalInformationFactory','UploadFactory','PaymentFactory', 'localStorageService',
  	function ($rootScope,$location,$scope,$timeout,PersonalInformationFactory,UploadFactory,PaymentFactory,localStorageService) {
        var loading = 0;
        // define month
        var months = [
  			{ "value": 1, "text": "January" },
	      	{ "value": 2, "text": "February" },
	      	{ "value": 3, "text": "March" },
	      	{ "value": 4, "text": "April" },
	      	{ "value": 5, "text": "May" },
	      	{ "value": 6, "text": "June" },
	      	{ "value": 7, "text": "July" },
	      	{ "value": 8, "text": "August" },
	      	{ "value": 9, "text": "September" },
	      	{ "value": 10, "text": "October" },
	      	{ "value": 11, "text": "November" },
	      	{ "value": 12, "text": "December" }
	    ];

        // define global variable
        /*define variables for personal information*/
	    $scope.months = months;
    	$scope.month = 1;
        $scope.isLoading = false;
    	var year_now = new Date().getFullYear();
    	var years = [];
    	for (var i = year_now; i > year_now-30; i--) {
    		years.push(i);
    	};
    	$scope.years = years;
    	$scope.year = year_now;
    	$scope.date = 1;
    	$scope.change = function(){
    		$scope.date = 1;
    	};
        $scope.errorupdate="";
        $scope.errorchangepass="";
        $scope.isSuccessChangepass=false;
        $scope.isErrorChangepass=false;

        // define first value
        /*@function
        * @desc
        * @param
        * return
        */
    	$scope.init = function(){
            if(localStorageService.get("user") != null){
                var CurrentUser = localStorageService.get('user');
                $scope.user = CurrentUser;
                // get information of personal
                $scope.getPersonalInfor(CurrentUser);
            }else{
                $scope.user = "";
            }
    		
    	};

        /*@function
        * @desc
        * @param
        * return
        */
        $scope.getPersonalInfor=function(CurrentUser){
            var token = CurrentUser.spree_api_key;
            PersonalInformationFactory.getInformation(token).
                success(function(data){
                    console.log(data);
                   // get personal information from api
                    $scope.firstname = data.first_name;   
                    $scope.lastname = data.last_name;
                    $scope.email = data.email;
                    if(data.birth_day!=null){
                    var list=data.birth_day.split('-');
                    $scope.date=parseInt(list[2]);
                    $scope.month=parseInt(list[1]);
                    $scope.year=parseInt(list[0]);
                    }

                }).
                error(function(error){
                    console.log(error);
                });
        };


        /* all functions for personal information*/

        // update personal information
        /*@function
        * @desc
        * @param
        * return
        */
        $scope.Update = function(){
            var token = localStorageService.get('user').spree_api_key;
                // console.log($scope.date);
                var birthday=$scope.year+"/"+$scope.month+"/"+$scope.date;
                // console.log(birthday);
                var sendingdata = 
                    {
                        "user":
                        {
                            "first_name":$scope.firstname,
                            "last_name": $scope.lastname,
                            "birth_day": birthday,
                            "email": $scope.email
                        }
                    };
                    // console.log(sendingdata);
                    $scope.Sendata(token,sendingdata);
           };
        // compare value of password before send
            /*@function
            * @desc
            * @param
            * return
            */
           $scope.ComparePass=function(currentpwd,newpwd,repwd){
            if(currentpwd===undefined||newpwd===undefined||repwd===undefined){
                return false;
            }
            else if(currentpwd.length<6 || newpwd.length<6||repwd.length<6) {
                return false;
            }
            else
            return true;
           };

        // change password
            /*@function
            * @desc
            * @param
            * return
            */
           $scope.ChangePass=function(){
                var token = localStorageService.get('user').spree_api_key;
               
                if($scope.ComparePass($scope.currentpwd,$scope.newpwd,$scope.repwd)===false){
                    $scope.isErrorChangepass=true;
                    $scope.errorchangepass="Password is incorrect";
                    $timeout(function() {$scope.isErrorChangepass=false;}, 3000);
                }
                else{
                    var sendingdata = 
                    {
                        "password":
                        {
                        "old":$scope.currentpwd,
                        "new":$scope.newpwd
                        }
                    };
                    // console.log(sendingdata);
                    $scope.SendataChangepass(token,sendingdata);
                }
                
           };
            /*@function
            * @desc
            * @param
            * return
            */
           $scope.Sendata=function(token,sendingdata){
            PersonalInformationFactory.saveInformation(token,sendingdata).
                success(function(data){   
                    /*console.log(data);*/
                    $scope.showUser(sendingdata); 
                    if(loading >= 2){
                        $scope.isSuccess = true;
                        loading = 0;
                    }else{
                        loading = loading +1;
                    } 
                    $scope.isSuccess=true;              
                    $scope.errorupdate=data[0].messages;
                    $timeout(function() {$scope.isSuccess=false;}, 3000);
                }).
                error(function(error){
                   /* console.log(error);*/
                    $scope.isError=true;
                    $scope.errorupdate=error[0].messages;
                    $timeout(function() {$scope.isError=false;}, 3000);
                });
           };

            // send data change password to api

            /*@function
            * @desc
            * @param
            * return
            */
            $scope.SendataChangepass=function(token,sendingdata){
            PersonalInformationFactory.saveInformation(token,sendingdata).
                success(function(data){
                    console.log(data[0].messages);   
                    $scope.isSuccessChangepass=true;
                    $scope.errorchangepass=data[0].messages;
                    $timeout(function() {$scope.isSuccessChangepass=false;}, 3000);
                }).
                error(function(error){
                    console.log(error);
                    $scope.isErrorChangepass=true;
                    $scope.errorchangepass=error[0].messages;
                    $timeout(function() {$scope.isErrorChangepass=false;}, 3000);
                });
           };
    /*
    * @function : showUser
    * @desc     : a user changed informations(name,image)
                  the header will be changed following that
    * @param    : changed information
    * return    : void - set new localstorage
    */
    $scope.showUser = function(sendingdata){
        var currentUser = localStorageService.get('user');
        localStorageService.remove('user');                 //delete user in localstorage
        localStorageService.remove('sampleuser');           //delete sampleuser in localstorage
        currentUser.first_name = sendingdata.user.first_name;   //change first_name
        currentUser.last_name = sendingdata.user.last_name;     //change last_name
        localStorageService.set('user',currentUser);            //set user again with new value
        localStorageService.set('sampleuser',currentUser);      //set sampleuser again with new value
        $rootScope.$broadcast('listener', true,currentUser);    //send data to announce something is changed   
    }
    /***********************************************************************/
    /*
    * @function : uploadFile
    * @desc     : upload avatar
    * @param    : none
    * return    : void
    */
    $scope.uploadFile = function(element){
        var file = element.files[0];                            //get element file from input tag
        $scope.isLoading = true;
        if(typeof file != "undefined"){
             var currentUser = localStorageService.get('user');
            if(currentUser.images.length < 1){                  // the avatar isn't exist
                $scope.uploadAvatar(file,currentUser.id,-1); 
            }else{                                              // the avatar is exist
                $scope.uploadAvatar(file,currentUser.id,currentUser.images[0].id);
            }
        }
    };
    /***********************************************************************/
    /*
    * @function : uploadAvatar
    * @desc     : call upload service
    * @param    : file,user_id,img_id
    * return    : void
    */
    $scope.uploadAvatar = function(file,user_id,img_id){
      UploadFactory.uploadAvatar(file,user_id,img_id).then(function(res){
        $scope.getAccountInfor();                                           //call to get newest user information
      });
    }
    /***********************************************************************/
    /****************************************************************************************/
    /*
    * @function : getAccountInfor
    * @desc     : send request to get account information(amount of money - important)
    * @param    : token
    * @return   : void - saving infor in localstorage(The token isn't changed)
    */
    $scope.getAccountInfor = function(){
        var CurrentUser = localStorageService.get('user');
        PaymentFactory.checkMoney(CurrentUser.spree_api_key).
        success(function(data){
            $scope.user = data;
            if(loading >= 2){
                $scope.isSuccess = true;
                loading = 0;
            }else{
                loading = loading +1;
            } 
            localStorageService.remove('user');
            localStorageService.set('user',data);
            $rootScope.$broadcast('listener', true,data);
            $timeout(function() {
                $scope.isLoading = false;                                   //hide loading icon
            }, 1200);
        }).
        error(function(error){
          $scope.isError=true;
        });
    }
    /****************************************************************************************/
  }]);
