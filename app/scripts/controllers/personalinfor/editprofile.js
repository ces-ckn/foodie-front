'use strict';

/**
 * @ngdoc function
 * @name foodieFrontApp.controller:EditprofileCtrl
 * @description
 * # EditprofileCtrl
 * Controller of the foodieFrontApp
 */
angular.module('foodieFrontApp')
  .controller('EditprofileCtrl', ['$rootScope','$scope','localStorageService',
   function ($rootScope,$scope,localStorageService){
        var instruction = [
          {
            element   : '#step1',
            intro     : 'Go shopping'
          }
        ];
    /***********************************************/
       //template of menu
        $scope.templates=[
        {  
            "title":"Order Status",
            "url":"views/personalinfor/orderstatus.html",
        },
        {
           "title":"Order History Information",
            "url":"views/personalinfor/historyorder.html"
        },
        {
           "title":"Personal Information",
            "url":"views/personalinfor/personalinfor.html"
        },
        {
            "title":"Deliver Information",
            "url":"views/personalinfor/deliverinfor.html"
        },
        {
            "title":"Food Preferences",
            "url":"views/personalinfor/foodpreference.html"
        },
        {
            "title":"Refer a friend",
            "url":"views/personalinfor/feferafriend.html"
        }
        ];
         // horizontal menu
        $scope.currentIndex=0;
        $scope.isCurrent=function(index){
          return  $scope.currentIndex===index;
        };
        $scope.setCurrent=function(index){
            $scope.currentIndex=index;
        }
        // define first value
        /*@function
        * @desc
        * @param
        * return
        */
    	$scope.init = function(){
            if(localStorageService.get("user") != null){
                var CurrentUser = localStorageService.get('user');
                $scope.user = CurrentUser;
            }else{
                $scope.user = "";
            }
            $rootScope.instruction = instruction;               // set content of instruction "onthemenu" page
            $rootScope.$broadcast('instruction',$rootScope.isActiveIns); // set show/hide instruction
    	};

        /*
        * @desc     : when change avatar, this page receive a event to change avatar
        * @param    : event, isLoggedIn,data
        * @return   : void - set user information
        */
        $scope.$on('listener', function(events, isLoggedIn,data){
            $scope.user = data;
        });
        /****************************************************************************************/
  }]);
