'use strict';

/**
 * @ngdoc function
 * @name foodieFrontApp.controller:OrderstatusCtrl
 * @description
 * # OrderstatusCtrl
 * Controller of the foodieFrontApp
 */
angular.module('foodieFrontApp')
  .controller('OrderstatusCtrl',['$scope','localStorageService','PersonalInformationFactory',
   function ($scope,localStorageService,PersonalInformationFactory) {
   	var districts=[
        {
            "id":0,
            "name":"Liên Chiểu"
        },
        {
            "id":1,
            "name":"Hải Châu"
        },
        {
            "id":2,
            "name":"Sơn Trà"
        }
        ,
        {
            "id":3,
            "name":"Thanh Khê"
        },
        {
          "id":4,
          "name":"Cẩm Lệ"
        }
    ]; 
    $scope.orders=[];
    $scope.address={};
    $scope.districts=districts;
    $scope.listdate=[];
     /*@function
        * @desc
        * @param
        * return
        */
    $scope.init=function(){
    	var CurrentUser=localStorageService.get('user');
    	if(CurrentUser!==null){	
    	var token=CurrentUser.spree_api_key;
	    	PersonalInformationFactory.getOrderStatus(token).
	    	success(function(data){
	    		console.log(data);
          if(data.count>0){  
	    		$scope.orders=data.orders;
               /* console.log(data.orders[0].shipments[0].date_delivery);*/

          }
	    	}).
	    	error(function(data){
	    		console.log(data);

	    	});
    	}
    };
    $scope.getFirstDate=function(index){
      var list=$scope.orders[index].shipments;
      if(list===null){
        list=[];
      }
      else
      {
      for(var i=0;i<list.length-1;i++)
      {
       for(var j=i+1;j<list.length;)
       {
        if($scope.compareDate(new Date(list[i].date_delivery),new Date(list[j].date_delivery))){
          for(var k=j;k<list.length;k++){
            list[k]=list[k+1];
          }
          list.length--;
        }
        else{
          j++;
        }
       }
      }
      }
      console.log(list);
      return list;
    };
     $scope.compareDate=function(date,comparedate){
        if(comparedate.getDate() === date.getDate() && 
            comparedate.getMonth() === date.getMonth() && 
              comparedate.getFullYear() === date.getFullYear()){
        return true;
      }
      else return false;
    };
     /*@function
        * @desc
        * @param
        * return
        */
     $scope.setValue=function(){
     	$scope.address.firstname="Tran";
     	$scope.address.lastname="Thuy";
     	$scope.address.address="03 Ngo Si Lien";
     	$scope.address.district=0;
     };
  }]);
