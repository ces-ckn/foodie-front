'use strict';

/**
 * @ngdoc function
 * @name foodieFrontApp.controller:WishlistCtrl
 * @description
 * # WishlistCtrl
 * Controller of the foodieFrontApp
 */
angular.module('foodieFrontApp')
  .controller('WishlistCtrl',['$scope','$state','$timeout','$rootScope','favoritedishfactory','localStorageService', 
  	function ($scope,$state,$timeout,$rootScope,favoritedishfactory,localStorageService) {
      // define array for amout of serving
    $scope.sizes=
    [
    {
      "value":1,
      "size":"For 1 people"
    },
    {
      "value":2,
      "size":"For 2 people"
    },
    {
      "value":3,
      "size":"For 3 people"
    },
    {
      "value":4,
      "size":"For 4 people"
    },
    {
      "value":5,
      "size":"For 5 people"
    },
    {
      "value":6,
      "size":"For 6 people"
    },
    {
      "value":7,
      "size":"For 7 people"
    },
    {
      "value":8,
      "size":"For 8 people"
    }
    ];
    // define global variables
    $scope.list={};
    $scope.list.sizepeople=[];
    $scope.list.price=[];
     $scope.list.liked=[];
    var listbasesize=[], listbaseprice=[];
    var dishes=[];
    // set list of size and price
    /*@function
    * @desc
    * @param
    * return
    */
    $scope.getSizeList=function(wishdishes){
      for(var i=0;i<wishdishes.length;i++){
        listbaseprice.push(parseInt(wishdishes[i].dish_price));
        listbasesize.push(1);
        $scope.list.sizepeople[i]=1;
        $scope.list.price[i]=parseInt(wishdishes[i].dish_price);
        $scope.list.liked[i]=true;
      }
    };

    // get value of unit price
    /*@function
    * @desc
    * @param
    * return
    */
    $scope.getPrice=function(price){
      var parsePrice=parseInt(price);
      return parsePrice;
    };

    // create base value for list of favorite dishes
    /*@function
    * @desc
    * @param
    * return
    */
  	$scope.init = function(){
            if(localStorageService.get("user") != null){
                var CurrentUser = localStorageService.get('user');
                $scope.user = CurrentUser;
                var token = CurrentUser.spree_api_key;
                if(localStorageService.get('dishes')===null){
                  dishes=[];
                }else{
                  dishes=localStorageService.get('dishes');
                }
                // get lish favorite dishes from api
                favoritedishfactory.getDishlist(token).
                  success(function(data){ 
                    if(data.products.length===1){
                      $scope.isNull=true;
                      $scope.setInit(data.products);
                    }else
                    if(data.products.length<1){
                      $scope.isNull=true;
                    } 
                    else{
                      $scope.isNull=false;
                      $scope.setInit(data.products);
                    }
                    // console.log($scope.wishdishes[0].images[0]);
                  }).
                  error(function(error){
                    console.log(error);
                });
            }else{
                $scope.user = "";
            }
            
        };
        $scope.setInit=function(data){
           $scope.wishdishes=data;
            $scope.getSizeList($scope.wishdishes); 
        };
    /*@function
    * @desc
    * @param
    * return
    */
      $scope.getTime=function(product){
        var listAvailabledate=product.available_on;
        var date=new Date(listAvailabledate[listAvailabledate.length-1].delivery_date);
        return date.getTime();
      };

     /*@function
    * @desc
    * @param
    * return
    */ 
    $scope.change=function(index){
      // console.log($scope.list.sizepeople[index]);
      var totalprice=listbaseprice[index]*$scope.list.sizepeople[index];
      $scope.list.price[index]=totalprice;
    };

    // unlike teh dish
     /*@function
    * @desc
    * @param
    * return
    */
  	$scope.unLike=function(id,index){
  		console.log(index);
       var CurrentUser = localStorageService.get('user');
       var token = CurrentUser.spree_api_key;
       var sendingdata={
        "product_id":id
       };
       // send data to api to unlike the dish
      favoritedishfactory.UnlikeDish(token,sendingdata).
          success(function(data){
          $scope.list.liked[index]=false;
          $scope.messagesuccess="You've unliked dish successfully";
          $scope.init();
          console.log(data); 
          }).
          error(function(error){
          console.log(error);
          });
  	   };
     /*@function
    * @desc
    * @param
    * return
    */
    $scope.compareDate=function(date,dateCompare){
       if(dateCompare.getDate() === date.getDate() && 
              dateCompare.getMonth() === date.getMonth() && 
                dateCompare.getFullYear() === date.getFullYear()){
        return true;
      }
      else return false;
    };

    // get available day of dish
     /*@function
    * @desc
    * @param
    * return
    */
    $scope.availableDate=function(product,price){
      var date=new Date();
      var tomorrow=new Date(date.setDate(date.getDate()+1));
      $scope.availabledate=[];
      var listavailableday=product.available_on;
      for(var i=0;i<listavailableday.length;i++){
        var nextday=new Date(listavailableday[i].delivery_date);
        nextday.setHours(23);
        nextday.setMinutes(59);
        nextday.setSeconds(59);
        if(nextday >= tomorrow){
          var datasend={
            "product":product,
            "date":new Date(listavailableday[i].delivery_date),
            "price":price
          }
          $scope.availabledate.push(datasend);
        }
      }
    };
    // hide button
    /*@function
    * @desc
    * @param
    * return
    */
    $scope.hideButton=function(product,date){
      var dishes=localStorageService.get('dishes');
      if(dishes!==null){
        for(var i=0;i<dishes.length;i++){
          var comparedate=new Date(dishes[i].date);
          if(product.id===dishes[i].product.id &&
            $scope.compareDate(new Date(date),comparedate)){
            return true;
          }
        }
      }
      else return false;
    };
    // add dish to bag
     /*@function
    * @desc
    * @param
    * return
    */
    $scope.chooseDate=function(product,price){
      $scope.isHide=false;
      $scope.availableDate(product,price);
      console.log($scope.isHide);
    };
     /*@function
    * @desc
    * @param
    * return
    */
    $scope.getDeliveryHours=function(product,price,date){
      favoritedishfactory.getDeliveryHour().
      success(function(data){
        console.log(data);
        $scope.times=data.time_frames;
      $scope.isHide=true;
      var data=
      {
        "product":product,
        "size":price/product.dish_price,
        "price":price,
        "date" :date,
        "hour":$scope.times[0]
      };
      dishes.push(data);
      localStorageService.set('dishes',dishes);
      }).
      error(function(data){
        console.log(data);
      });
    };
    $scope.addToBag=function(product,price,date){
      $scope.getDeliveryHours(product,price,date);
     
      // $scope.added=true;
      // $state.go('app.bag');
    };
  }]);
