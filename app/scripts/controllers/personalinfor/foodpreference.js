'use strict';

/**
 * @ngdoc function
 * @name foodieFrontApp.controller:FoodpreferenceCtrl
 * @description
 * # FoodpreferenceCtrl
 * Controller of the foodieFrontApp
 */
angular.module('foodieFrontApp')
  .controller('FoodpreferenceCtrl',['$scope','localStorageService','PersonalInformationFactory',
   function ($scope,localStorageService,PersonalInformationFactory) {
        $scope.foodslike=[];
        $scope.foodsdislike=[];
        $scope.ingredients=[];
        $scope.listlike=[];
        $scope.listdislike=[];
        $scope.foodsearch="";
        $scope.idslike=[];
        $scope.idsdislike=[];
        // define first value
        /*@function
        * @desc
        * @param
        * return
        */
    	$scope.init = function(){
            if(localStorageService.get("user") != null){
                var CurrentUser = localStorageService.get('user');
                // $scope.user = CurrentUser;
                // get information of personal
                $scope.get(CurrentUser);
                /*$scope.getFoodPreference(CurrentUser);*/
                $scope.isEditPeople=true;
                $scope.isEditHabit=true;
            }else{
                // $scope.user = "";
            }
    		
    	};
        $scope.get=function(CurrentUser){
            PersonalInformationFactory.get(CurrentUser.spree_api_key).
            success(function(data){
              if(data[0].messages==="true"){
                $scope.showFoodPreference=true;
                $scope.getFoodPreference(CurrentUser);
              }
              else
              {
                $scope.showFoodPreference=false;
              }
              console.log($scope.showFoodPreference);
            }).
            error(function(data){
              $scope.showFoodPreference=false;
              console.log(data);
            });
        };
        /*@function
        * @desc
        * @param
        * return
        */
        $scope.setInit=function(data){
            $scope.foodslike=[];
            $scope.idslike=[];
            $scope.foodsdislike=[];
            $scope.idsdislike=[];
            var likefood=data.liked_ingredients;
            var dislikefood=data.hated_ingredients;
            if(likefood.length>0){
                $scope.isShowLike=true;
                for(var i=0;i<likefood.length;i++){
                    var food={
                        "id":likefood[i].id,
                        "name":likefood[i].name,
                        "src":likefood[i].images[0].normal_url
                    }
                    $scope.foodslike.push(food);
                    $scope.idslike.push(likefood[i].id);
                }
            }
            if(dislikefood.length>0){
                $scope.isShowDisLike=true;
              for(var j=0;j<dislikefood.length;j++){
                    var food={
                        "id":dislikefood[j].id,
                        "name":dislikefood[j].name,
                        "src":dislikefood[j].images[0].normal_url
                    }
                $scope.foodsdislike.push(food);
                $scope.idsdislike.push(dislikefood[j].id);
                }  
            }
            // for(var i=0;i<data.length;i++){

            // }
        };
        $scope.showEditPeople=function(){
            $scope.isEditPeople=false;
        };
        $scope.showEditHabit=function(){
            $scope.isEditHabit=false;
        };
        /*@function
        * @desc
        * @param
        * return
        */
        $scope.getFoodPreference=function(CurrentUser){
            var token = CurrentUser.spree_api_key;
            $scope.isEditPeople=true;
            $scope.isEditHabit=true;
            PersonalInformationFactory.getFoodPreference(token).
            success(function(data){
                console.log(data);
                if(data!==null){   
                $scope.idvalue=data.id;
                $scope.people=data.family;
                $scope.habit=data.habit;
                $scope.setInit(data);
                }
                // $scope.foodpreferences=foodpreferences;
            }).
            error(function(data){
                console.log(data);
            });
        };
        /*@function
        * @desc
        * @param
        * return
        */
        $scope.delete=function(id,state){
            //delete food like if state=0
            if(state===0){
                for(var i=0;i<$scope.foodslike.length;i++){
                    if($scope.foodslike[i].id===id){
                        $scope.foodslike.splice(i,1);
                        $scope.idslike.splice(i,1);
                    }
                }
            }
            //delete food dislike if state=1
            if(state===1){
                for(var i=0;i<$scope.foodsdislike.length;i++){
                    if($scope.foodsdislike[i].id===id){
                        $scope.foodsdislike.splice(i,1);
                        $scope.idsdislike.splice(i,1);
                    }
                }
            }
            $scope.update();
            console.log($scope.idsdislike);
            console.log($scope.idslike);
        };
         /*@function
        * @desc
        * @param
        * return
        */
        $scope.getFoodIngredient=function(){
           if($scope.foodsearch!==""){
                $scope.isAddedLike=true; 
                var sendingdata={
                name:$scope.foodsearch
               };
               PersonalInformationFactory.getFoodIngredient(sendingdata).
               success(function(data){
                console.log(data);
                $scope.ingredients=data.ingredients;
               }).
               error(function(data){
                console.log(data);
               });
           }
           else{
            $scope.isAddedLike=false; 
           }
           
        };
         /*@function
        * @desc
        * @param
        * return
        */
        $scope.setFood=function(id,state){
            $scope.foodsearch="";
            $scope.isAddedLike=false;
            for(var i=0;i<$scope.ingredients.length;i++){
            if($scope.ingredients[i].id===id){
              var ingredient={
                "id":$scope.ingredients[i].id,
                "image":$scope.ingredients[i].images[0].mini_url,
                "name":$scope.ingredients[i].name
              };
                  $scope.checkToAdd(id,state,ingredient);
                }
            }
        };
        $scope.checkToAdd=function(id,state,ingredient){
            /*list food you like*/
            var count=0;
            if(state===0){
                if($scope.listlike.length>0){
                    for(var i=0;i<$scope.listlike.length;i++){
                        console.log($scope.listlike[i].id);
                        if($scope.listlike[i].id!==id){
                            count++;
                        }
                        else{
                            count=0;
                            return;
                        }
                    }
                    if(count>0){
                        $scope.listlike.push(ingredient); 
                    }
                }
                else{
                 $scope.listlike.push(ingredient);   
                }
            }
            /*list food you don't like*/
            if(state===1){
                if($scope.listdislike.length>0){
                    for(var i=0;i<$scope.listdislike.length;i++){
                        console.log($scope.listdislike[i].id);
                        if($scope.listdislike[i].id!==id){
                            count++;
                        }
                        else{
                            count=0;
                            return;
                        }
                    }
                    if(count>0){
                        $scope.listdislike.push(ingredient); 
                    }
                }
                else{
                 $scope.listdislike.push(ingredient);   
                }
            }
            console.log($scope.listdislike);
        };
        $scope.check=function(state){
            if(state===0){
                if($scope.listlike.length>0 && $scope.idsdislike.length>0){
                    for(var i=0;i<$scope.listlike.length;i++){
                        for(var j=0;j<$scope.idsdislike.length;j++){
                            if($scope.listlike[i].id===$scope.idsdislike[j]){
                                $scope.listlike.splice(i,1);
                            }
                        }
                    }
                }
            }
            if(state===1){
                if($scope.listdislike.length>0 && $scope.idslike.length>0){
                    for(var i=0;i<$scope.listdislike.length;i++){
                        for(var j=0;j<$scope.idslike.length;j++){
                            if($scope.listdislike[i].id===$scope.idslike[j]){
                                $scope.listdislike.splice(i,1);
                            }
                        }
                    }
                }
            }
        };
        /*@function
        * @desc
        * @param
        * return
        */
        $scope.add=function(state){
            if(state===0){
                $scope.check(state);
                if($scope.listlike.length>0){   
                    for(var i=0;i<$scope.listlike.length;i++){
                        $scope.idslike.push($scope.listlike[i].id);
                    }
                }
            }
            if(state===1){
                $scope.check(state);
                console.log($scope.listdislike);
                if($scope.listdislike.length>0){   
                    for(var i=0;i<$scope.listdislike.length;i++){
                        $scope.idsdislike.push($scope.listdislike[i].id);
                    }
                }
            }
          $scope.update();
       };
       $scope.update=function(){
        console.log($scope.idvalue);
        console.log($scope.idsdislike);
        console.log($scope.idslike);
        var CurrentUser = localStorageService.get('user');
           var token=CurrentUser.spree_api_key;
           var sendingdata=
              { "survey" : 
                {
                  "family" : $scope.people,
                  "liked_ingredient_ids" : $scope.idslike,
                  "hated_ingredient_ids" : $scope.idsdislike,
                  "habit" : $scope.habit
                }
              };
            console.log(sendingdata);
            PersonalInformationFactory.addIngredient(sendingdata,token,$scope.idvalue).
            success(function(data){
                console.log("vo day");
                console.log(data);
                $scope.getFoodPreference(CurrentUser);
            }).
            error(function(data){
                console.log(data);
            });
       };
  }]);
