'use strict';

/**
 * @ngdoc function
 * @name foodieFrontApp.controller:ShoppingbagCtrl
 * @description
 * # ShoppingbagCtrl
 * Controller of the foodieFrontApp
 */
angular.module('foodieFrontApp')
  .controller('ShoppingbagCtrl', ['$rootScope','$scope','$timeout','$state','localStorageService',
  	function ($rootScope,$scope,$timeout,$state,localStorageService) {
  	$scope.sizes=
    [
    {
      "value":1,
      "size":"For 1 people"
    },
    {
      "value":2,
      "size":"For 2 people"
    },
    {
      "value":3,
      "size":"For 3 people"
    },
    {
      "value":4,
      "size":"For 4 people"
    },
    {
      "value":5,
      "size":"For 5 people"
    },
    {
      "value":6,
      "size":"For 6 people"
    },
    {
      "value":7,
      "size":"For 7 people"
    },
    {
      "value":8,
      "size":"For 8 people"
    }
    ];
    var instruction_Added = [
      {
        element   : '#step6',
        intro     : "This is food what you added to bag",
        position  : 'right'
      },
      {
        element   : '#step7',
        intro     : "Order. Click it.",
        position  : 'left'
      }
    ];
    var instruction_Back = [
      {
        element   : '#step1',
        intro     : "You haven't added food yet. Let's go shopping",
        position  : 'bottom'
      }
    ];
    // define global varible 
    $scope.list={};
    // define list of dishes in the bag
  	$scope.bagdishes=[];
  	$scope.list.sizeserve=[];
  	$scope.list.prices=[];
  	var listfirstprice=[],listfirstsize=[];
  	$scope.isShow=true;
  	$scope.totalbag=0;
    $scope.dishes=[];
    $scope.prices=[];
    $scope.results=[];
    $scope.availabledate=[];
    $scope.priceOfSize=0;

  	/*@function init()
    * @des define first value to show in html
    *@param void
    *return void
    */
  	$scope.init=function(){
      console.log($rootScope.isActiveIns);
      // get data from localStorage
  		var dishes=localStorageService.get('dishes');
      if(dishes===null){
        dishes=[];
      }
      console.log(dishes);
  		// compare array dishes is null or not null
  		if(dishes.length>=1){
  			$scope.isShow=false;
        var currentDate= new Date();
        var tomorrow=new Date(currentDate.setDate(currentDate.getDate()+1));
        console.log(tomorrow);
        // first value for product,price and available date
  			for(var i=0;i<dishes.length;i++){
            var date=new Date(dishes[i].date);
            date.setHours(23);
            date.setMinutes(59);
            date.setSeconds(59);
            if(date>=tomorrow){
              $scope.dishes.push(dishes[i].product);
              $scope.prices.push(dishes[i].price);
              $scope.availabledate.push(dishes[i].date);
              $scope.bagdishes.push(dishes[i]);
            }
            else{
              localStorageService.get('dishes').splice(i,1);
            }
  			}
        localStorageService.set('senddata',dishes);
        // get list date have diferrent items in list dish
        $scope.getFirstDate();
         // caculate total of dishes in bag
        $scope.getTotalPrice($scope.prices);
        $rootScope.instruction = instruction_Added;       // set content of instruction "bag" page(added food)
        $rootScope.$broadcast('instruction',$rootScope.isActiveIns); // set show/hide instruction
        $rootScope.isAdded = true;
  		}
      else
  		{
        // don't have data
        $rootScope.instruction = instruction_Back;       // set content of instruction "bag" page(not added food yet)
        $rootScope.$broadcast('instruction',$rootScope.isActiveIns); // set show/hide instruction
        $rootScope.isAdded = false;
        console.log("khong co data");
        $scope.isShow=true;
  		}
  	};

    /*@function getFirstDate
    * @des get list available date to sort dish by date
    *@param void
    *return $scope.listdate is list date after out same item
    */
    $scope.getFirstDate=function(){
      var list=$scope.availabledate;
      if(list===null){
        $scope.listdate=[];
      }
      else{
         $scope.listdate=list;
      for(var i=0;i<$scope.listdate.length-1;i++)
      {
       for(var j=i+1;j<$scope.listdate.length;)
       {
        if($scope.compareDate(new Date($scope.listdate[i]),new Date($scope.listdate[j]))){
          for(var k=j;k<$scope.listdate.length;k++){
            $scope.listdate[k]=$scope.listdate[k+1];
          }
          $scope.listdate.length--;
        }
        else{
          j++;
        }
       }
      }
      }
    };

    /*@function getTime()
    *@des convert date to total of time
    *@param date
    *return time is count of date.getTime
    */
    $scope.getTime=function(date){
      var time=(new Date(date)).getTime();
      return time;
    };

    /*@function getTotalPrice()
    * @des get total price of dishes
    *@param prices: list price of dishes
    *return $scope.totalbag
    */
  	// figure total price of shopping bag
  	$scope.getTotalPrice=function(prices){
      $scope.totalbag=0;
  		for(var i=0;i<prices.length;i++){
  			$scope.totalbag+=parseInt(prices[i]);
  		}
  	};

    /*@function changeInLocal()
    * @des change value of dishes in localStorageService
    *@param id, price,size,deliverydate of dish
    *return void
    */
    $scope.changeInLocal=function(id,price,size,deliverydate){
        var dishes=localStorageService.get('dishes');;
        // change data in localStorage
      for(var i=0;i<dishes.length;i++){
        var date=new Date(dishes[i].date);
        if(dishes[i].product.id===id && $scope.compareDate(new Date(deliverydate),date)){
          dishes[i].price=price;
          dishes[i].size=size;
        }
      }
      localStorageService.set('dishes',dishes); 
      localStorageService.set('senddata',dishes);
    }

    /*@function getPrice
    * @des
    *@param id,deliverydate of dish 
    *return $scope.prices is list of price in dishes
    */
    $scope.getPrice=function(id,deliverydate){
      for(var i=0;i<$scope.bagdishes.length;i++){
        var date=new Date($scope.bagdishes[i].date);
        if($scope.bagdishes[i].product.id===id && $scope.compareDate(new Date(deliverydate),date)){
          return $scope.prices[i];
        }
      }
    };
    
    /*@function compareDate()
    * @des to compare 2 date
    *@param date and comparedate
    *return result of compare
    */
    $scope.compareDate=function(date,comparedate){
      if(comparedate.getDate() === date.getDate() && 
              comparedate.getMonth() === date.getMonth() && 
              comparedate.getFullYear() === date.getFullYear()){
        return true;
      }
      else return false;
    };

  	/*@function changeSize()
    * @des change totalprice, price when select amount of serving
    *@param id,deliverydate of dish, selected of select box
    *return void
    */
  	$scope.changeSize=function(id,selected,deliverydate){
      for(var i=0;i<$scope.bagdishes.length;i++){
        var date=new Date($scope.bagdishes[i].date);
        if($scope.bagdishes[i].product.id===id && $scope.compareDate(new Date(deliverydate),date)){
          $scope.prices[i]=$scope.bagdishes[i].product.dish_price*selected;
          $scope.priceOfSize=$scope.bagdishes[i].product.dish_price*selected;
        }
      }
      $scope.getPrice(id,deliverydate);
      $scope.changeInLocal(id,$scope.priceOfSize,selected,deliverydate);
  		$scope.getTotalPrice($scope.prices);
  	};

    /*@function deleteProduct()
    * @desc delete product from the bag
    *@param id && delivery date
    *return 
    */
    $scope.deleteProduct=function(id,deliverydate){
      var dishes=localStorageService.get('dishes');
      for(var i=0;i<$scope.bagdishes.length;i++){
        var date=new Date($scope.bagdishes[i].date);
        if($scope.bagdishes[i].product.id===id && $scope.compareDate(date,new Date(deliverydate))){
          $scope.bagdishes.splice(i,1);
          $scope.prices.splice(i,1);
          dishes.splice(i,1);
        }
      }
      localStorageService.set('dishes',dishes);
      localStorageService.set('senddata',dishes);
      $scope.getTotalPrice($scope.prices);
      if(localStorageService.get('dishes').length<1){
        $scope.isShow=true;
        $rootScope.instruction = instruction_Back;       // set content of instruction "bag" page(not added food yet)
        $rootScope.$broadcast('instruction',$rootScope.isActiveIns); // set show/hide instruction
        $rootScope.isAdded = false;
      }
    };
        /*@function
        * @desc
        * @param
        * return
        */
    $scope.orderNow=function(){
      var user=localStorageService.get('user');
      if(user!==null){
        var step={
          "step":0,
          "idOrder":null,
          "shipment":null
        };
        localStorageService.set('step',step);
        $state.go('app.enterinfor');
      }
      else{
        $state.go('app.login');
      }
    };
    /**************************set Instruction*************************/
      /*
      @function   : setInstruction
      @desc     : Each page has one instruction about how to buy food
      @param    : none
      @return   : void 
      */
      
      $scope.setInstruction = function(){
        $rootScope.instruction = instruction;
      }
      /******************************************************************/
  }]);
