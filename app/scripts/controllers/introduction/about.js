'use strict';

/**
 * @ngdoc function
 * @name foodieFrontApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the foodieFrontApp
 */
angular.module('foodieFrontApp')
  .controller('AboutCtrl',['$scope', function ($scope) {
  	var bosses = [
	  	{
	  		name	 		: "CHRISTOPHE K. NGO",
	  		position		: "Founder & President",
	  		instroduction	: "Christophe brings over 12 years of digital agency development and leadership experience to Code Engine Studio. His previous role as Technology Director at The Hangar Interactive (Critical Mass ‐ Costa Rica) saw him directly oversee and manage 30 technology staff. As part of the leadership team he helped run the company and grow it by over 100% to 100+ staff within 2 years.\n \nChristophe is an avid traveller having gone to 45+ countries and enjoys the outdoors, scuba diving, hiking and snowboarding. \n \nClient Experience: AT&T, HP, Nissan, Infiniti, Mercedes Benz, Citi Bank, Scotiabank, Mattel, USAA, Gucci and Hyatt",
	  		avatar 			: "./images/us/christophe-ngo.jpg"
	  	},
      {
        name      : "SAKURA KOMURO",
        position    : "Founder & Client Partner",
        instroduction : "Sakura's background is in User Experience (UX) having worked at Critical Mass, Corbis/Veer, and Shaw Communications. She has lead numerous teams and worked on several fortune 500 website redesigns. In her previous position as UX Lead, she continuously drove refinement and innovation to meet users changing needs in e-commerce at Veer/Corbis. \n\nSakura loves to cook, but really she cooks because she loves to eat! As a foodie, she enjoys traveling and living abroad to expand her food horizons.\n\nClient Experience: AT&T, Mercedes Benz, Dell, Hyatt Hotel, Corbis/Veer & Shaw Communications",
        avatar      : "./images/us/sakura.jpg"
      },
	  	{
	  		name 			: "ABIGAIL STRIETELMEIRE",
	  		position		: "Teacher & HR Training Specialist",
	  		instroduction	: "Abi ...",
	  		avatar 			: "./images/us/abi.jpg"
	  	}
  	];
  	var developers = [
	  	{
	  		name 			: "HIEU NGUYEN",
	  		position		: "Developer - Back End",
	  		instroduction	: "My name is Bond.  James Bond.\n  Just kidding.  I'm Hieu Bond.\n You can call me 008, for short.\n Takes a sip of martini <shaken, not stirred>",
	  		avatar 			: "./images/us/hieu-nguyen.jpg"
	  	},
	  	{
	  		name 			: "HUY TONG",
	  		position 		: "Developer - Back End",
	  		instroduction 	: "I love purple \n I like pink \n I love myself. \n I'm big Huy =))",
	  		avatar 			: "./images/us/huy-tong.jpg"
	  	},
	  	{
	  		name  			: "THUY TRAN",
	  		position 		: "Developer - Front End",
	  		instroduction 	: "I'm the hot girl in my team. \n\n Yes. Only me is the one in my team. \n Ms.Thuy is here",
	  		avatar 			: "./images/us/thuy-tran.jpg"
	  	},
	  	{
	  		name			: "LINH TRAN",
	  		position 		  : "Developer - Front End",
	  		instroduction 	: "I am sitting here looking at the most amazing person I have ever seen, smart, funny, caring, and absolutely stunning! Yes, I am looking in the mirror! \n\nThey call me Linh, I will give you time to decide. ",
	  		avatar  		: "./images/us/linh-tran.jpg"
	  	}
  	];
  	$scope.isBoss = -1;
  	$scope.isDeveloper = -1;
  	$scope.bosses = bosses;
  	$scope.developers = developers;

    /*
    @func: ShowBoss
    @desc: click image to show detail about boss information
    @param: index of boss
    @return: void - set css
    */
  	$scope.ShowBoss = function(index){
  		$scope.isDeveloper = -1;
  		$scope.styleDeveloper = {'argin-left' : '0px', 'transition'  : 'all .4s ease-in'};
  		if(index == $scope.isBoss){
  			$scope.isBoss = -1;
  			$scope.style = {'argin-left' : '0px', 'transition'  : 'all .4s ease-in'};
  		}else{
  			$scope.isBoss = index;
  			$scope.style = {'margin-left' : -294 * index +'px', 'transition'  : 'all .4s ease-in'};
  		}
  	}
    /*************************************************************************************/
    /*
    @func: moveBSection
    @desc: adjust css
    @param: index of boss
    @return: void - set css
    */
  	$scope.moveBSection = function(param){
  		$scope.isDeveloper = -1;
  		$scope.isBoss = -1;
  		$scope.style = {'margin-left' : -294 * param +'px', 'transition'  : 'all .4s ease-in'};
  		$scope.styleDeveloper = {'argin-left' : '0px', 'transition'  : 'all .4s ease-in'};
  	}
    /*************************************************************************************/
    /*
    @func: ShowDeveloper
    @desc: click image to show detail about developer information
    @param: index of developer
    @return: void - set css
    */
  	$scope.ShowDeveloper = function(index){
  		$scope.style = {'argin-left' : '0px', 'transition'  : 'all .4s ease-in'};
  		$scope.isBoss = -1;
  		if(index == $scope.isDeveloper){
  			$scope.isDeveloper = -1;
  			$scope.styleDeveloper = {'argin-left' : '0px', 'transition'  : 'all .4s ease-in'};
  		}else{
  			$scope.isDeveloper = index;
  			$scope.styleDeveloper = {'margin-left' : -294 * index +'px', 'transition'  : 'all .4s ease-in'};
  		}
  	}
    /*************************************************************************************/
    /*
    @func: moveDSection
    @desc: adjust css
    @param: index of developer
    @return: void - set css
    */
  	$scope.moveDSection = function(param){
  		$scope.isBoss = -1;
  		$scope.isDeveloper = -1;
  		$scope.style = {'argin-left' : '0px', 'transition'  : 'all .4s ease-in'};
  		$scope.styleDeveloper = {'margin-left' : -294 * param +'px', 'transition'  : 'all .4s ease-in'};
  	}
    /*************************************************************************************/
  }]);
