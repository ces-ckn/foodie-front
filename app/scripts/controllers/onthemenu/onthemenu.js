'use strict';

/**
 * @ngdoc function
 * @name foodieFrontApp.controller:OnthemenuCtrl
 * @description
 * # OnthemenuCtrl
 * Controller of the foodieFrontApp
 */
angular.module('foodieFrontApp')
	.controller('OnthemenuCtrl', ['$rootScope','$scope', 'localStorageService', 'OnTheMenuFactory',
		function ($rootScope,$scope, localStorageService, OnTheMenuFactory) {
		var date_param = 0;
		var month_param = 0;
		var box;
		var loading = 0;
		var days = [
			{"id":0,"name":"Monday"},
			{"id":1,"name":"Tuesday"},
			{"id":2,"name":"Wednesday"},
			{"id":3,"name":"Thursday"},
			{"id":4,"name":"Friday"},
			{"id":5,"name":"Saturday"},
		];
		var weeks = [
			{name : "This Week",index : 0},
			{name : "Next Week",index : 7}
			/*{name : "Last Week",index : -7}*/
		];
		$scope.selectedIndex = 0; 						//index - type of product
		$scope.Popover = {
		    title: "Dish's Name" 						//tool tip if name of dish is too long
	  	};
	  	$scope.days = days;
	  	$scope.weeks = weeks;
	  	var instruction = [								//introduction: on the menu page
	      {
	        element   : '#step2',
	        intro     : "View box detail",
	        position  : 'right'
	      },
	      {
	        element   : '#step3',
	        intro     : "Or view dish detail",
	        position  : 'right'
	      },
	      {
	        element   : '#step4',
	        intro     : "Add to bag",
	        position  : 'right'
	      },
	      {
	        element   : '#step5',
	        intro     : "Check your bag",
	        position  : 'left'
	      }
	    ];
		/**************************set index of types*************************/
	    /*
	    @function: setIndex
	    @desc: set index of type when choosing the type.
	    @param: id of type
	    @return: void
	    */
		$scope.setIndex = function(param){
			$scope.selectedIndex = param; //index of type
		};
		/******************************************************************/

		/**************************set defaut today*************************/
	    /*
	    @function: setToday
	    @desc: the default day is today, click to choose another day.
	    @param: id of day base on object date (Monday - 0, Tuesday - 1)
	    @return: void
	    */
		$scope.setToday = function(param){
			$scope.specificDay = param;
			date_param = param;
			$scope.selectedDate = $scope.getSpecificDay(date_param,month_param); // date of product - date
			$scope.showingdate = new Date($scope.selectedDate).getTime();	//time of product - milisecond
			$scope.setDailyBox(box,$scope.selectedDate);  					//set daily box
		};
		/******************************************************************/

		/**************************set week*************************/
	    /*
	    @function: setWeek
	    @desc: the default week is this week, click to choose another week.
	    @param: id of day base on object date,name of week (last week,this week, next week)
	    @return: void
	    */
		$scope.setWeek = function(param){
			$scope.week = weeks[param].name;								//show name of week
			$scope.index = weeks[param].index;								//hide selected week
			var monday = $scope.getMonday(weeks[param].index);
			var saturday = $scope.getSaturday(weeks[param].index);
			$scope.Monday = monday											//set monday of choosed week
			$scope.Saturday = saturday;										//set saturday of choosed week
			$scope.getDailyBoxes(monday,saturday);
			$scope.getWeeklyBox(monday,saturday);
			month_param = weeks[param].index;
			$scope.showingdate = new Date($scope.selectedDate).getTime();   //time of product - milisecond
		};
		/******************************************************************/

		/**************************get Data*************************/
	    /*
	    @function: getData
	    @desc: when the on the menu page is load
	    @param: none
	    @return: void
	    */
		$scope.getData = function(){
			$scope.isLoading = true;
			$scope.usingDirective();
			$scope.getAllDishes();
			$scope.getTypeOfDishes();
			var date = new Date();
			date_param = date.getDay(); 								//index of day
			if(date.getDay() == 6 || date.getDay() == 0){				//saturday or sunday
				$scope.specificDay = $scope.getMonday(7).getDay() -1;	//next monday
				$scope.selectedDate = $scope.getMonday(7); 				//set specific date of choosed week
				$scope.setWeek(1);										//set next week
			}else{														
				$scope.specificDay = new Date().getDay(); 				//tomorrow - day
				var tomorrow = new Date();
				tomorrow.setDate(tomorrow.getDate() + 1)				//tomorrow - milisecond
				$scope.selectedDate = tomorrow;							//set specific date of choosed week - date
				$scope.setWeek(0);										//set this week
			}
		};
		/******************************************************************/
		/**************************get Type of dishes*************************/
	    /*
	    @function   : getTypeOfDishes
	    @desc       : get type of dishes through API
	    @param      : none
	    @return     : void 
	    */
		$scope.getTypeOfDishes = function(){
			OnTheMenuFactory.getTypeOfDish().
				success(function(data){
					console.log(data);
					$scope.types = data; //set types
					$scope.isLoaded();
				}).
				error(function(error){
					console.log(error);
				});
		};
		/******************************************************************/
		/**************************get all dishes *************************/
	    /*
	    @function   : getAllDishes
	    @desc       : get all dishes in 3 weeks(last,this,next)
	    @param      : none
	    @return     : void 
	    */
		$scope.getAllDishes = function(){
			var token = $scope.getToken();
			OnTheMenuFactory.getProduct(token).
				success(function(data){
					console.log(data);
					$scope.products = data.products; //set dishes
					$scope.isLoaded();
				}).
				error(function(error){
					console.log(error);
				});
		};
		/******************************************************************/
		/**************************get daily boxes*************************/
	    /*
	    @function   : getDailyBoxes
	    @desc       : get daily box through API in one week
	    @param      : none
	    @return     : void 
	    */
		
		$scope.getDailyBoxes = function(start_date,end_date){
			var token = $scope.getToken();
			OnTheMenuFactory.getDailyBoxes(start_date,end_date,token).then(function(res){
				console.log(res.data);
				box = res.data;
	          	$scope.setDailyBox(box,$scope.selectedDate); 	// set/show daily box
	        	$scope.isLoaded();
	        });
		};
		/******************************************************************/
		/**************************get weekly boxes*************************/
	    /*
	    @function   : getWeeklyBoxes
	    @desc       : get weekly box through API in 1 week
	    @param      : none
	    @return     : void
	    */
		$scope.getWeeklyBox = function(start_date,end_date){
			var token = $scope.getToken();
			OnTheMenuFactory.getWeeklyBox(start_date,end_date,token).then(function(res){
				console.log(res.data);
				if(res.data.delivery.length != 0){
					$scope.weeklybox = res.data;
		    		$scope.weeklymoney = $scope.setMoney(res.data.delivery[0].products); //set money of weekly box
		    		$scope.weeklydate  = new Date(res.data.delivery[0].date).getTime();  //set date of weekly box
				}
				$scope.isLoaded();
	        });
		};
		/******************************************************************/
		/**************************set property to use directive*************************/
	    /*
	    @function   : usingDirective
	    @desc       : inject some condition to use comment/like directive
	    @param      : none
	    @return     : void - set type by json
	    */
	    $scope.usingDirective = function(){
	      	var dish = {
                type : "dish",
                page : "onthemenu"
            };
            var box = {
                type : "box",
                page : "onthemenu"
            };
            $scope.boxtype 	= box;
	      	$scope.dishtype = dish;
	  	}
	    /******************************************************************/

	    /**************************get Day*************************/
	    /*
	    @function   : getMonday,getSaturday, getSpecificDay 
	    @desc       : get some day, get Moday and Saturday (the website works from Monday to Saturday)
	    			  get Specific day when a user choose another day in day bar.
	    @param      : id of day ( monday : 0, tuesday:1) 
	    @return     : return day
	    */
		$scope.getMonday = function(param) {
			var d = new Date();
		  	var day = d.getDay(),
		    first = d.getDate() - day + (day == 0 ? -6:1) + param ;
		    return new Date(d.setDate(first));
		};

		$scope.getSaturday = function(param) {
			var d = new Date();
		  	var day = d.getDay(),
		    last = d.getDate() - day + (day == 0 ? -6:1) + 5 +param;
		    return new Date(d.setDate(last));	
		};

		$scope.getSpecificDay = function(month_param,date_param){
			var d = new Date();
		  	var day = d.getDay(),
		    first = d.getDate() - day + (day == 0 ? -6:1) + date_param + month_param;
		    return new Date(d.setDate(first));
		};
		/******************************************************************/

	  	/**************************set Money*************************/
	    /*
	    @function: setMoney
	    @desc: show money of box, total price of dishes
	    @param: dishes
	    @return: money
	    */
	    $scope.setMoney = function(products){
	      	var money = 0;
	      	for(var i = 0; i < products.length; i++){
	       	 	money = money + parseFloat(products[i].dish_price);
	      	}
	      	return money;
	    }

	    /******************************************************************/
	    /**************************set Daily Box*************************/
	    /*
	    @function 	: setDailyBox
	    @desc 		: show money of box, total price of dishes
	    @param 		: dishes,date
	    @return 	: void - set money of box
	    */
	    $scope.setDailyBox = function(box,date){
	    	var date = new Date(date);					//selected date
	    	var dishes;
	      	for(var i = 0; i < box.delivery.length; i++){
	       	 	var deliverydate = new Date(box.delivery[i].date);
	       	 	if(date.toDateString() == deliverydate.toDateString()){ 	//find box belongs to selected date
	       	 		dishes = box.delivery[i].products;
	       	 		break;
	       	 	}
	      	}
      		$scope.dailybox = box;											//set daily box
      		if(typeof dishes != "undefined"){
      			$scope.dailymoney = $scope.setMoney(dishes);				//set money of daily box
	    		$scope.dishes = dishes;
	    	}else{
	    		$scope.dailymoney = 0;										//if daily box isn't exist
	    	}
	    }
	    /******************************************************************/
	    /**************************get Token*************************/
	    /*
	    @function 	: getToken
	    @desc 		: get token from local storage
	    @param 		: none
	    @return 	: token
	    */
	    $scope.getToken = function(){
	    	var token;
			var currentUser = localStorageService.get('user');
			if(currentUser != null){
				token = currentUser.spree_api_key;
			}
			return token;
	    }
	    /******************************************************************/
	    /**************************isLoaded*************************/
	    /*
	    @function 	: isLoaded
	    @desc 		: show/hide spinner
	    @param 		: none
	    @return 	: void
	    */
	    $scope.isLoaded = function(){
	    	loading = loading + 1;
	    	if(loading >= 4){
	    		$scope.isLoading = false;
	    		loading = 0;
	    		$rootScope.instruction = instruction;				// set content of instruction "onthemenu" page
	       		$rootScope.$broadcast('instruction',$rootScope.isActiveIns); // set show/hide instruction
	    	}
	    }
	    /******************************************************************/
	}]);
