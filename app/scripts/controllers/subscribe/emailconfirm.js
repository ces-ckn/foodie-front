'use strict';

/**
 * @ngdoc function
 * @name foodieFrontApp.controller:EmailconfirmCtrl
 * @description
 * # EmailconfirmCtrl
 * Controller of the foodieFrontApp
 */
angular.module('foodieFrontApp')
  .controller('EmailconfirmCtrl',['$scope','$timeout','$stateParams','sendSubscribeFactory',
  	function ($scope,$timeout,$stateParams,sendSubscribeFactory) {
  	$scope.errorConfirm="";
  	 /*@function
    * @desc
    * @param
    * return
    */    
  	$scope.ConfirmEmail=function(){
	     var Data = 
	  	{
	  		"email_confirm_subscription":
	  		{
	  		"id":$stateParams.id, 
	  		"confirm_subscription_token":$stateParams.token
	  		}
	  	};
	  	console.log(Data);
	      sendSubscribeFactory.confirmEmail(Data).
	      success(function(data){
	      	console.log(data);
	      	$scope.isSuccess=true;
	      	$scope.errorConfirm="You've subscribed successfully";
	      	$timeout(function() { $scope.isSuccess=false;}, 3000);
	      }).
	      error(function(error){
	      	$scope.isError=true;
	      	$scope.errorConfirm="Error subscribe email";
	      	$timeout(function() { $scope.isError=false;}, 3000);
	      	console.log(error);
	     });
  	};
  }]);
