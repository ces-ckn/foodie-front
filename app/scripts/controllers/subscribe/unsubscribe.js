'use strict';

/**
 * @ngdoc function
 * @name foodieFrontApp.controller:UnsubscribeCtrl
 * @description
 * # UnsubscribeCtrl
 * Controller of the foodieFrontApp
 */
angular.module('foodieFrontApp')
  .controller('UnsubscribeCtrl',['$scope','$timeout','$stateParams','$state','sendSubscribeFactory'
  	,function ($scope,$timeout,$stateParams,$state,sendSubscribeFactory) {
   var id=$stateParams.id;
   var token=$stateParams.token;
    /*@function
    * @desc
    * @param
    * return
    */    
   	$scope.UnSubscribe=function(){
   		var emailSend=
      	{
         "email_confirm_unsubscription":
         {
          "id":id, 
          "confirm_unsubscription_token":token
        }
      };
      console.log(emailSend);
   		sendSubscribeFactory.UnSubscribe(emailSend).
   		success(function(data){
        $scope.isSuccess=true;
	      $scope.errorConfirm="You've unsubscribed successfully";
        $timeout(function() { $scope.isSuccess=false;}, 3000);
      console.log(data);
	  	}).
	  	error(function(error){
        $scope.isError=true;
        $scope.errorConfirm="Error when unsubscribed email";
        $timeout(function() { $scope.isError=false;}, 3000);
	  	  console.log(error);
		});
   	};   
  }]);
