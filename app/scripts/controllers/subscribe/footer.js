'use strict';

/**
 * @ngdoc function
 * @name foodieFrontApp.controller:FooterCtrl
 * @description
 * # FooterCtrl
 * Controller of the foodieFrontApp
 */
angular.module('foodieFrontApp')
  .controller('FooterCtrl',['AppURL','$scope','$timeout','sendSubscribeFactory',
  	function (AppURL,$scope,$timeout,sendSubscribeFactory) {
    $scope.url = AppURL.app.domain;
    $scope.description = AppURL.app.description;
    $scope.title=AppURL.app.name;
    $scope.image=AppURL.app.image;
    $scope.isCollapsed=true;
    $scope.errorsubemail="";
    $scope.textSubscribe="Submit";
    $scope.loading=false;
    $scope.showModal = false;
    /*@function
    * @desc
    * @param
    * return
    */    
    $scope.sendSubscribe=function(){
     // console.log(button);
        $scope.email=$scope.emailSend;
    	 $scope.showModal = !$scope.showModal;
    };
   /*@function
    * @desc
    * @param
    * return
    */    
    $scope.Send=function(){
      var emailSend=
        {
          "email":$scope.emailSend,
        };
        $scope.textSubscribe="Sending...";
        $scope.loading=true;
      console.log($scope.emailSend);
      sendSubscribeFactory.sendSubscribe(emailSend).
      success(function(data){
        $scope.isCollapsed=false;
        $scope.isSuccess=true;
        $scope.textSubscribe="Submit";
        $scope.errorsubemail="You've subscribed successfully";
        $scope.emailSend="";
        $timeout(function() {$scope.isCollapsed=true;}, 3000);
      }).
      error(function(error){
      $scope.isCollapsed=false;
      $scope.textSubscribe="Submit";
      $scope.emailSend="";
      $scope.isError=true;
      $scope.errorsubemail="You can't subscribe email successfully ";
      $timeout(function() {$scope.isCollapsed=true;}, 3000);
     });
    };
  }]);
