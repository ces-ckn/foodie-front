'use strict';

/**
 * @ngdoc function
 * @name foodieFrontApp.controller:EnterinforCtrl
 * @description
 * # EnterinforCtrl
 * Controller of the foodieFrontApp
 */
angular.module('foodieFrontApp')
  .controller('EnterinforCtrl',['$rootScope','$scope','$filter','localStorageService','$timeout','$window','$state','enterInforFactory','PaymentFactory',
    function ($rootScope,$scope,$filter,localStorageService,$timeout,$window,$state,enterInforFactory,PaymentFactory) {
    var instruction_address = [
      {
        element   : '#step8',
        intro     : 'Click <img src="../images/icons/add.png"> to begin'
      }
    ];
    var instruction_defaulttime = [
      {
        element   : '#step9',
        intro     : 'Set delivery time',
        position  : 'right'
      },{
        element   : '#step9a',
        intro     : 'Set default delivery time for all foods',
        position  : 'right'
      }
    ];
    var instruction_othertime = [
      {
        element   : '#step9',
        intro     : 'Set delivery time',
        position  : 'right'
      },{
        element   : '#step9b',
        intro     : 'Information and delivery time for this food',
        position  : 'right'
      },{
        element   : '#step9c',
        intro     : 'Set specific time',
        position  : 'right'
      },{
        element   : '#step9d',
        intro     : 'Arrangement',
        position  : 'right'
      }

    ];
    var instruction_more = [
      {
        element   : '#step11',
        intro     : 'Some information (Optional)',
        position  : 'right'
      }
    ];
    var instruction_payment = [
      {
        element   : '#step10',
        intro     : 'Choose one payment method',
        position  : 'right'
      }
    ];
    // define country list
    var districts=[
      {
        "id":0,
        "name":"Liên Chiểu"  
      },
      {
        "id":1,
        "name":"Hải Châu"
      },
      {
        "id":2,
        "name":"Sơn Trà"
      },
      {
        "id":3,
        "name":"Thanh Khê"
      },
      {
        "id":4,
        "name":"Cẩm Lệ"
      }  
    ]; 

      // define list of people in family
    var peoples=[
    {
      "value":2,
      "amount":"2 people"
    },
    {
      "value":3,
      "amount":"3 people"
    },
    {
      "value":4,
      "amount":"4 people"
    },
    {
      "value":5,
      "amount":"5 people"
    },
    {
      "value":6,
      "amount":"6 people"
    },
    {
      "value":7,
      "amount":"7 people"
    },
    {
      "value":8,
      "amount":"8 people"
    }
    ];
    /*define global variables */
  	$scope.hours=[];
    $scope.dishes=[];
    $scope.orderdishes=[];
    $scope.ingredients=[];
  	$scope.deliverinfor={};
    $scope.deliverinfor.district=0;
    $scope.deliverinfor.newdistrict=0;
    $scope.deliverinfor.people=2;
    $scope.deliverinfor.foodlike="";
    $scope.deliverinfor.fooddislike="";
    $scope.deliverinfor.feeling="";
    $scope.deliverinfor.check="0";
    // list first value
    $scope.peoples=peoples;
    $scope.districts=districts;
    $scope.currentPayMethod=0;
    $scope.availabledate=[];
    $scope.total=0;
    $scope.error_newaddress="";
    $scope.isEdit=true;
    $scope.paymethod="Cash";
    $scope.paymentId = 2;
    $scope.loading = false;
    $scope.isDefault=true;
    $scope.showInforShipping=true;
    $scope.showTimeDeliver=false;
    $scope.showPaymentMethod=false;
    $scope.showFoodPreference=false;
    $scope.times=[];
    /*
    * @function
    * @desc
    * @param
    * @return
    */
    $scope.setCurrentPayMethod=function(index){
      $scope.currentPayMethod=index;
      if(index===0){
        $scope.paymethod = "Cash";
        $scope.paymentId = 2;
      }
      if(index===1){
        $scope.paymethod = "Prepaid";
        $scope.paymentId = 4;
      }
      if(index===2){
        $scope.paymethod = "Paypal";
        $scope.paymentId = 3;
      }
      if(index == 3){
        $scope.paymethod = "BitCoin";
        $scope.paymentId = 5;
      }
    };
    $scope.isCurrentPayMethod=function(index){
      return $scope.currentPayMethod===index;
    };

  	/*
  	* @function
  	* @desc
  	* @param
  	* @return
  	*/
  $scope.init = function() {
    var userAccount = localStorageService.get('user');
    var token = userAccount.spree_api_key;
    $scope.balance = userAccount.balance;
    $scope.getAccountInfor(token); //checking amount of money in customer account
    // get user_address from api
    $scope.getUserAddress(token);
    /*********************************/
    /*get time delivery from APIs*/
    enterInforFactory.getDeliveryHour().
    success(function(data) {
        $scope.hours = data.time_frames;
        $scope.deliverinfor.time = data.time_frames[0].id;
    }).
    error(function(data) {
        console.log(data);
    });
    /*get total price for product*/
    if (localStorageService.get('dishes').length > 0) {
        $scope.orderdishes = localStorageService.get('dishes');
        for (var i = 0; i < $scope.orderdishes.length; i++) {
            $scope.total += parseInt($scope.orderdishes[i].price);
        }
    }
    /*call ngInit for set all value of page*/
    $scope.ngInit();
  };
    /****************************************************************/ 
    /*get list of user address*/
  $scope.getUserAddress = function(token) {
     enterInforFactory.getUserAddress(token).
     success(function(data) {
         if (data.length > 0) {
             $scope.isNull = false;
             $scope.user_address = data;
             $scope.getDeliver(data[data.length - 1]);
         } else {
             $scope.isNull = true;
         }
         $rootScope.$broadcast('instruction', $rootScope.isActiveIns); // set show/hide instruction
     }).
     error(function(data) {
         console.log(data);
     });
  };

    /*set all value of user address for form*/
    $scope.getDeliver=function(data){
      $scope.deliverinfor.firstname=data.firstname;
      $scope.deliverinfor.lastname=data.lastname;
      $scope.deliverinfor.selectaddress=data.id;
      $scope.deliverinfor.phone=data.phone;
      $scope.deliverinfor.address=data.address1;
      $scope.deliverinfor.district=$scope.getDistrict(data.city);  
    };

    /*set all value for page*/
    $scope.ngInit=function(){
        // get dishes from bag
        var dishes=localStorageService.get('senddata');
        if(dishes!==null){
        // first value for product,price and available date
        for(var i=0;i<dishes.length;i++){
          var date=new Date(dishes[i].date);
          $scope.availabledate.push(dishes[i].date);
          $scope.times.push(dishes[i].hour);
        }
          $scope.isShow=true;
          $scope.dishes=dishes;
          $scope.getFirstDate();
          $scope.getFirstTime();
        }
        else{
          $scope.isShow=false;
        }

    };

    /*get all time delivery of dishes without same value*/
  $scope.getFirstTime = function() {
     if ($scope.times === null) {
         $scope.times = [];
     } else {
         for (var i = 0; i < $scope.times.length - 1; i++) {
             for (var j = i + 1; j < $scope.times.length;) {
                 if ($scope.times[i].id === $scope.times[j].id) {
                     for (var k = j; k < $scope.times.length; k++) {
                         $scope.times[k] = $scope.times[k + 1];
                     }
                     $scope.times.length--;
                 } else {
                     j++;
                 }
             }
         }
     }
 };
    /*
    * @function
    * @desc get all delivery date without same value
    * @param
    * @return
    */
    $scope.getFirstDate = function() {
        var list = $scope.availabledate;
        if (list === null) {
            $scope.listdate = [];
        } else {
            $scope.listdate = list;
            for (var i = 0; i < $scope.listdate.length - 1; i++) {
                for (var j = i + 1; j < $scope.listdate.length;) {
                    if ($scope.compareDate(new Date(list[i]), new Date(list[j]))) {
                        for (var k = j; k < $scope.listdate.length; k++) {
                            $scope.listdate[k] = $scope.listdate[k + 1];
                        }
                        $scope.listdate.length--;
                    } else {
                        j++;
                    }
                }
            }
        }
    };

    /*@function compareDate()
    * @des to compare 2 date
    *@param date and comparedate
    *return result of compare
    */
    $scope.compareDate = function(date, comparedate) {
    if (comparedate.getDate() === date.getDate() &&
        comparedate.getMonth() === date.getMonth() &&
        comparedate.getFullYear() === date.getFullYear()) {
        return true;
    } else
        return false;
    };
    /***************************************************/
    /*all funtion of deliver address information*/
    /*
    * @function getDistrict(district)
    * @desc return id of district name
    * @param district is a name of district
    * @return id
    */
    $scope.getDistrict=function(district){
      for(var i=0;i<$scope.districts.length;i++){
        if($scope.districts[i].name===district){
          return $scope.districts[i].id;
        }
      }
    }; 
    /*
    * @function
    * @desc change value of address when select
    * @param
    * @return
    */  
    $scope.changeAddress=function(){
      $scope.isEdit=true;
      var idAddress=$scope.deliverinfor.selectaddress;
      for(var i=0;i<$scope.user_address.length;i++){
        if($scope.user_address[i].id===idAddress){
          $scope.deliverinfor.district=$scope.getDistrict($scope.user_address[i].city);
          $scope.deliverinfor.phone=$scope.user_address[i].phone;
          $scope.deliverinfor.address=$scope.user_address[i].address1;
        }
      }
    };
    /*
    * @function
    * @desc create new address
    * @param
    * @return
    */  
    $scope.overwriteAddress=function(){
      var user=localStorageService.get('user');
      var token=user.spree_api_key;
      var sendingdata=
        {
          "address":
          {
            "first_name":$scope.deliverinfor.firstname, 
            "last_name":$scope.deliverinfor.lastname, 
            "phone":$scope.deliverinfor.newphone,
            "address1":$scope.deliverinfor.newaddress,
            "user_id": user.id,
            "title":$scope.deliverinfor.newaddress, 
            "city":$scope.setDistrict($scope.deliverinfor.newdistrict)
          }
        };
        $scope.createAddress(token,sendingdata);
    };
    /*
    * @function
    * @desc send data create new address to api
    * @param
    * @return
    */
    $scope.createAddress=function(token,sendingdata){
      enterInforFactory.createAddress(token,sendingdata).
      success(function(data){
        $scope.getUserAddress(token);
        $scope.isSuccess=true;
        $scope.error_newaddress="You created address successfully";
        $timeout(function() { $scope.isSuccess=false;}, 3000);
      }).
      error(function(data){
        $scope.isError=true;
        $scope.error_newaddress="You can't create new address";
        $timeout(function() { $scope.isError=false;}, 3000);
      });

    };
    /*
    * @function
    * @desc return name of district id
    * @param
    * @return
    */
    $scope.setDistrict=function(district){
      for(var i=0;i<districts.length;i++){
        if(districts[i].id===district){
          return districts[i].name;
        }
      }
    };
    /*
    * @function
    * @desc change some value of address if you want
    * @param
    * @return
    */
    $scope.editAddress=function(){
      var token = localStorageService.get('user').spree_api_key;
      var sendingdata=
        {
          "address":
          {
            "first_name":$scope.deliverinfor.firstname, 
            "last_name":$scope.deliverinfor.lastname, 
            "phone":$scope.deliverinfor.phone,
            "address1":$scope.deliverinfor.address, 
            "title":$scope.deliverinfor.address, 
            "city":$scope.setDistrict($scope.deliverinfor.district)
          }
        };
        // send data delivery address to api
      var idaddress=$scope.deliverinfor.selectaddress;
        enterInforFactory.editAddress(token,sendingdata,idaddress).
        success(function(data){   
          $scope.getUserAddress(token);
          $scope.isDeliverSuccess=true;          
          $scope.error_editaddress=data[0].messages;          
          $timeout(function() { $scope.isDeliverSuccess=false;}, 3000                    );            
        }).
        error(function(error){
          $scope.isDeliverError=true;          
          $scope.error_editaddress=error.messages;          
          $timeout(function() { $scope.isDeliverError=false;}, 3000);          
        });
      };
    /*********************************************************************/
    /*all function of tim delivery*/
    /*
    * @function
    * @desc 
    * @param
    * @return
    */
    $scope.isChecked=function(){
      var checked=$scope.deliverinfor.check;
      if(checked==="0"){
        $scope.isDefault=true;
        $scope.isOtherOption=false;
        $rootScope.instruction = instruction_defaulttime;  
        $rootScope.$broadcast('instruction',$rootScope.isActiveIns);  // set show/hide instruction
      }
      if(checked==="1"){
        $scope.isDefault=false;
        $scope.isOtherOption=true;
        $rootScope.instruction = instruction_othertime;  
        $rootScope.$broadcast('instruction',$rootScope.isActiveIns);  // set show/hide instruction
      }
    };
    /*instruction for enter infor page*/
    $scope.checkStep=function(value){
      var step1=localStorageService.get('step');
      if(step1!==null){
      var step=step1.step;
        if(step===value){
           if(step===0){
            $scope.showInforShipping=true;
            $rootScope.instruction = instruction_address;                         // set content of instruction "enter information" page
            return true;
           }
           if(step===1){
            $scope.showTimeDeliver=true;
            $rootScope.instruction = instruction_defaulttime;                         // set content of instruction "enter information" page
            return true;
           }
           if(step===2){
            $scope.showFoodPreference=true;
            return true;
           }
           if(step==3){
            $scope.showPaymentMethod=true;
            $rootScope.instruction = instruction_payment;  
            return true;
           }
        }
      }
      else{
        console.log("vo day");
        $scope.showInforShipping=true;
        var step2={
          "step":0,
          "idOrder":$scope.orderNumber
        };  
        localStorageService.set('step',step2);
        return true;
      }
    };
    /*change time delivery for each dish*/
    $scope.changeSplit = function(id, date, IDhour, number, selectedHour) {
    var dishes = localStorageService.get('senddata');
    var countFirstValue = 0;
    for (var i = 0; i < $scope.dishes.length; i++) {
        if ($scope.dishes[i].product.id === id &&
            $scope.compareDate(new Date($scope.dishes[i].date), new Date(date)) &&
            $scope.dishes[i].hour.id === IDhour) {
            var count = 0;
            countFirstValue++;
            if (countFirstValue === 1) {
                /*if time delivery is changed*/
                if (IDhour !== selectedHour) {
                    /*compare with same value as hour,date,id when select hour*/
                    var same = null;
                    for (var j = 0; j < $scope.dishes.length; j++) {
                        if ($scope.dishes[j].product.id === id &&
                            $scope.compareDate(new Date($scope.dishes[j].date), new Date(date)) &&
                            $scope.dishes[j].hour.id === selectedHour) {
                            same = j;
                        }
                    }
                    /*if number is changed*/

                    if (number <= $scope.dishes[i].size) {
                        var count = $scope.dishes[i].size - number;
                        if (same !== null) {
                            console.log("vo day");
                            $scope.dishes[same].size = $scope.dishes[same].size + number;
                            $scope.dishes[same].price = $scope.dishes[same].product.dish_price * $scope.dishes[same].size;
                            if (count > 0) {
                                $scope.dishes[i].size = count;
                                $scope.dishes[i].price = $scope.dishes[i].product.dish_price * count;
                            } else {
                                $scope.dishes.splice(i, 1);
                            }
                            localStorageService.set('senddata', $scope.dishes);
                        } else {
                            if (count > 0) {
                                var data = {
                                    "product": $scope.dishes[i].product,
                                    "size": count,
                                    "price": parseInt($scope.dishes[i].product.dish_price) * parseInt(count),
                                    "date": date,
                                    "hour": $scope.getHour(IDhour)
                                };
                                $scope.dishes.push(data);
                            }
                            var data1 = {
                                "product": $scope.dishes[i].product,
                                "size": number,
                                "price": parseInt($scope.dishes[i].product.dish_price) * parseInt(number),
                                "date": date,
                                "hour": $scope.getHour(selectedHour)
                            };
                            $scope.dishes.splice(i, 1);
                            $scope.dishes.push(data1);
                            localStorageService.set('senddata', $scope.dishes);
                        }
                    }
                }
            }
        }
    }
    $scope.ngInit();
};
  /*change hour from idHour*/
    $scope.getHour=function(idHour){
      for(var i=0;i<$scope.hours.length;i++){
        if($scope.hours[i].id===idHour){
          return $scope.hours[i];
        }
      }
    };
    /*change time delivery when choose default time*/
    $scope.changeDefaultTime=function(){
      var time=$scope.getHour($scope.deliverinfor.time);
      for(var i=0;i<$scope.orderdishes.length;i++){
        $scope.orderdishes[i].hour=time;
      }
    };
    /***********************************************************************/
    /*all funtion of food preferences*/
    /*
    * @function
    * @desc get all ingredients
    * @param
    * @return
    */
    $scope.list={};
    $scope.list.like=[];
    $scope.list.dislike=[];
    $scope.list.likeids=[];
    $scope.list.dislikeids=[];
    $scope.list.dishes=[];
    /*
    * @function
    * @desc get ingredient to add
    * @param
    * @return
    */
    $scope.getIngredient=function(id,value){
      for(var i=0;i<$scope.ingredients.length;i++){
        if($scope.ingredients[i].id===id){
          var ingredient={
            "id":id,
            "image":$scope.ingredients[i].images[0].mini_url,
            "name":$scope.ingredients[i].name
          };
            $scope.checkToAdd(id,value,ingredient);
            $scope.check(value);
        }
      }
    };
    /*
    * @function
    * @desc check id of ingredient with something choosed
    * @param
    * @return
    */
    $scope.checkToAdd = function(id, state, ingredient) {
    /*list food you like*/
    var count = 0;
    if (state === 0) {
        if ($scope.list.like.length > 0) {
            for (var i = 0; i < $scope.list.like.length; i++) {
                if ($scope.list.like[i].id !== id) {
                    count++;
                } else {
                    count = 0;
                    return;
                }
            }
            if (count > 0) {
                $scope.list.like.push(ingredient);
                $scope.list.likeids.push(id);
            }
        } else {
            $scope.list.like.push(ingredient);
            $scope.list.likeids.push(id);
        }
    }
    /*list food you don't like*/
    if (state === 1) {
        if ($scope.list.dislike.length > 0) {
            for (var i = 0; i < $scope.list.dislike.length; i++) {
                if ($scope.list.dislike[i].id !== id) {
                    count++;
                } else {
                    count = 0;
                    return;
                }
            }
            if (count > 0) {
                $scope.list.dislike.push(ingredient);
                $scope.list.dislikeids.push(id);
            }
        } else {
            $scope.list.dislike.push(ingredient);
            $scope.list.dislikeids.push(id);

        }
    }
};
        /*
    * @function
    * @desc check same id in 1 array choose
    * @param
    * @return
    */
   $scope.check = function(state) {
    if (state === 0) {
        if ($scope.list.like.length > 0 && $scope.list.dislikeids.length > 0) {
            for (var i = 0; i < $scope.list.like.length; i++) {
                for (var j = 0; j < $scope.list.dislikeids.length; j++) {
                    console.log($scope.list.like[i].id);
                    if ($scope.list.like[i].id === $scope.list.dislikeids[j]) {
                        $scope.list.like.splice(i, 1);
                        $scope.list.likeids.splice(i, 1);
                    }
                }
            }
        }
    }
    if (state === 1) {
        console.log($scope.list.dislike);
        if ($scope.list.dislike.length > 0 && $scope.list.likeids.length > 0) {
            for (var i = 0; i < $scope.list.dislike.length; i++) {
                for (var j = 0; j < $scope.list.likeids.length; j++) {
                    if ($scope.list.dislike[i].id === $scope.list.likeids[j]) {
                        $scope.list.dislike.splice(i, 1);
                        $scope.list.dislikeids.splice(i, 1);
                    }
                }
            }
        }
    }
};
        /*
    * @function
    * @desc get food preference
    * @param
    * @return
    */
  $scope.getfoodpreference = function() {
    var userAccount = localStorageService.get('user');
    var token = userAccount.spree_api_key;
    // get food preference from api
    enterInforFactory.getFoodPreference(token).
    success(function(data) {
        if (data[0].messages === "true") {
            $scope.showFoodPreference = false;
            $scope.getAllInforFoodPreference(token);
        } else {
            $scope.showFoodPreference = true;
        }
    }).
    error(function(data) {
        $scope.showFoodPreference = true;
        console.log(data);
    });
  };
    /*
    * @function
    * @desc get all information about food preference
    * @param
    * @return
    */
$scope.getAllInforFoodPreference = function(token) {
     enterInforFactory.get(token).
     success(function(data) {
         // $scope.list.like
         // $scope.list.dislike
         $scope.deliverinfor.people = data.family;
         $scope.deliverinfor.feeling = data.habit;
         var disheslike = data.liked_ingredients;
         var dishesdislike = data.hated_ingredients;
         if (disheslike.length > 0) {
             for (var i = 0; i < disheslike.length; i++) {
                 $scope.list.like.push(disheslike[i]);
             }
         }
         if (dishesdislike.length > 0) {
             for (var i = 0; i < dishesdislike.length; i++) {
                 $scope.list.dislike.push(dishesdislike[i]);
             }
         }
     }).
     error(function(data) {
         console.log(data);
     });
 };
    /*
    * @function
    * @desc get food like
    * @param
    * @return
    */
$scope.getFoodLike = function() {
    $scope.isAddedDisLike = false;
    $scope.isAddedLike = true;
    var searchtext = $scope.deliverinfor.foodlike;
    console.log(searchtext);
    if (searchtext !== "") {
        var sendingdata = {
            "name": searchtext
        }
        enterInforFactory.getIngredients(sendingdata).
        success(function(data) {
            $scope.ingredients = data.ingredients;
        }).
        error(function(data) {
            console.log(data);
        });
    } else {
        $scope.isAddedLike = false;
    }
};
  $scope.setFoodLike=function(food){
      $scope.deliverinfor.foodlike="";
      $scope.isAddedLike=false;
      $scope.isChooseLike=true;
      $scope.getIngredient(food,0);
    };
    /*
    * @function
    * @desc get food you dislike
    * @param
    * @return
    */
    $scope.getFoodDisLike=function(){
      $scope.isAddedLike=false;
      $scope.isAddedDisLike=true;
      var searchtext=$scope.deliverinfor.fooddislike;
      console.log(searchtext);
       if(searchtext!==""){
        var sendingdata={
          "name":searchtext
        }
        enterInforFactory.getIngredients(sendingdata).
        success(function(data){
           $scope.ingredients=data.ingredients;
        }).
        error(function(data){
          console.log(data);
        });
      }
      else{
        $scope.isAddedDisLike=false;
      }
    };
    $scope.setFoodDisLike=function(food){
      $scope.deliverinfor.fooddislike="";
      $scope.isAddedDisLike=false;
      $scope.isChooseDisLike=true;
      $scope.getIngredient(food,1);
    };
    /*
    * @function
    * @desc delete ingredient you liked
    * @param
    * @return
    */
    $scope.deleteLIkeIngredient=function(id){
      for(var i=0;i<$scope.list.like.length;i++){
        if($scope.list.like[i].id===id){
          $scope.list.likeids.splice(i,1);
          $scope.list.like.splice(i,1);
        }
      }
    };
    /*
    * @function
    * @desc delete ingredient you disliked
    * @param
    * @return
    */
    $scope.deleteDisLikeIngredient=function(id){
      for(var i=0;i<$scope.list.dislike.length;i++){
        if($scope.list.dislike[i].id===id){
          $scope.list.dislikeids.splice(i,1);
          $scope.list.dislike.splice(i,1);
        }
      }
    };
    /*
    * @function
    * @desc add food preference
    * @param
    * @return
    */
    $scope.addFoodPreference=function(){
      var user=localStorageService.get('user');
      var token=user.spree_api_key;
      var people=$scope.deliverinfor.people;
      var foodlike=$scope.list.likeids;
      var fooddislike=$scope.list.dislikeids;
      var feeling=$scope.deliverinfor.feeling;
      var sendingdata=
      { "survey" : 
        {
          "family" : people,
          "liked_ingredient_ids" : foodlike,
          "hated_ingredient_ids" : fooddislike,
          "habit" : feeling
        }
      };
      console.log(sendingdata);
      enterInforFactory.addFoodPreference(sendingdata,token).
      success(function(data){
         var step1={
              "step":3,
              "idOrder":localStorageService.get('step').idOrder,
              "shipment":localStorageService.get('step').shipment
              };
        localStorageService.set('step',step1);
        $scope.showFoodPreference=false;
        $scope.showPaymentMethod=true;
        $rootScope.instruction = instruction_payment;                    // instruction payment
        $rootScope.$broadcast('instruction',$rootScope.isActiveIns);  // set show/hide instruction
      }).
      error(function(data){
        console.log(data);
      });
    };
   
    /*
    * @function
    * @desc get time the user want to deliver
    * @param
    * @return
    */
    $scope.getTimeDeliver=function(value){
      for(var i=0;i<deliveryhours.length;i++){
        if(deliveryhours[i].value===value){
          return deliveryhours[i].time;
        }
      }
    };
    
    /*
    * @function
    * @desc get time the user want to deliver
    * @param
    * @return
    */
    $scope.getTimeDeliver=function(value){
      for(var i=0;i<$scope.hours.length;i++){
        if($scope.hours[i].value===value){
          return $scope.hours[i].time;
        }
      }
    };
    /****************************************************************************/
    /*all function for order function*/
    /*
    * @function
    * @desc create order 
    * @param
    * @return
    */
      $scope.createOrder=function(){
        $scope.loading = true;
          var user=localStorageService.get('user');
          var token=user.spree_api_key;
          var sendproduct=[];
          var dishes=localStorageService.get('dishes');
          var products=dishes;
          for(var i=0;i<products.length;i++){
            var product=
            {
            "variant_id":products[i].product.id,
            "quantity":products[i].size
            };
            sendproduct.push(product);
          }
          console.log(sendproduct);
          var sendingdata=
          {
            "order":
            {
              "line_items":sendproduct
            },
            "address_id": $scope.deliverinfor.selectaddress
          };
          console.log(sendingdata);
          enterInforFactory.createOrder(sendingdata,token).
          success(function(data){
            console.log(data);
            $scope.nextStep(data.number,token);
          }).
          error(function(data){
            console.log(data);
            $window.alert("Can't create the order");
          });
        };
        /*next step function*/
        $scope.nextStep=function(number,token){
          enterInforFactory.nextStep(number,token).
          success(function(data){
            console.log(data);
            $scope.setDeliverAddress(number,token);
          }).
          error(function(data){
            console.log(data);
          });
        };
        /*set delivery address for order*/
        $scope.setDeliverAddress=function(number,token){
          var sendingdata=
              {
                "order": {
                  "bill_address_attributes": {
                    "firstname": $scope.deliverinfor.firstname,
                    "lastname": $scope.deliverinfor.lastname,
                    "address1": $scope.deliverinfor.address,
                    "city": $scope.setDistrict($scope.deliverinfor.district),
                    "phone": $scope.deliverinfor.phone,
                    "state_id": 3708,
                    "country_id": 240
                  },
                  "ship_address_attributes": {
                    "firstname": $scope.deliverinfor.firstname,
                    "lastname": $scope.deliverinfor.lastname,
                    "address1": $scope.deliverinfor.address,
                    "city": $scope.setDistrict($scope.deliverinfor.district),
                    "phone": $scope.deliverinfor.phone,
                    "state_id": 3708,
                    "country_id": 240
                  }
                }
              };
          console.log(sendingdata);
          enterInforFactory.updateOrder(sendingdata,token,number).
            success(function(data){
              console.log(data);
              $scope.loading=false;
              $scope.shipmentId=data.shipments[0].id;
              $scope.orderNumber=number;
              $scope.showInforShipping=false;
              $scope.showTimeDeliver=true;
              $scope.getfoodpreference();
               var step1={
                  "step":1,
                  "idOrder":$scope.orderNumber,
                  "shipment":data.shipments[0].number
                };
              localStorageService.set('step',step1); 
              $rootScope.instruction = instruction_defaulttime;                    // instruction delivery time
              $rootScope.$broadcast('instruction',$rootScope.isActiveIns);  // set show/hide instruction
            }).
            error(function(data){
              console.log(data);
            });

        };
    /*
    * @function
    * @desc set time deliver
    * @param
    * @return
    */
      $scope.createTimeOrder = function() {
          var user = localStorageService.get('user');
          var token = user.spree_api_key;
          $scope.nextloading = true;
          var number = localStorageService.get('step').idOrder;
          var shipment = localStorageService.get('step').shipment;
          if ($scope.deliverinfor.check === "0") {
              var dishes = $scope.orderdishes;
              localStorageService.set("senddata", dishes);
          } else {
              var dishes = localStorageService.get('senddata');
          }
          var senddata = [];
          $scope.results = dishes;
          for (var i = 0; i < $scope.listdate.length; i++) {
              for (var j = 0; j < $scope.hours.length; j++) {
                  dishes = $filter('filterByDate')($scope.results, $scope.listdate[i]);
                  dishes = $filter('filterByHour')(dishes, $scope.hours[j]);
                  if (dishes.length > 0) {
                      var data = [];
                      for (var k = 0; k < dishes.length; k++) {
                          var text = {
                              "variant_id": dishes[k].product.id,
                              "quantity": dishes[k].size
                          }
                          data.push(text);
                      }
                      var date = $scope.listdate[i];
                      date = $filter('date')(new Date($scope.listdate[i]), 'dd/MM/yyyy');
                      var datawithdate = {
                          "line_items": data,
                          "date_delivery": date,
                          "time_frame_id": $scope.hours[j].id
                      }
                      senddata.push(datawithdate);
                  }
              }
          }
          var sendingdata = {
              "order_number": number,
              "original_shipment_number": shipment,
              "shipments": senddata
          };
          enterInforFactory.shipment(sendingdata, token).
          success(function(data) {
              $scope.showTimeDeliver = false;
              $scope.nextStep1(number, token, shipment);
          }).
          error(function(data) {
              console.log(data);
          });

      };

        $scope.nextStep1 = function(number, token, shipment) {
          enterInforFactory.nextStep(number, token).
          success(function(data) {
              $scope.nextloading = false;
              if ($scope.showFoodPreference === true) {
                  $scope.showPaymentMethod = false;
                  var step1 = {
                      "step": 2,
                      "idOrder": number,
                      "shipment": shipment
                  };
                  localStorageService.set('step', step1);
                  $rootScope.instruction = instruction_more; // instruction more information
                  $rootScope.$broadcast('instruction', $rootScope.isActiveIns); // set show/hide instruction
              } else {
                  $scope.showPaymentMethod = true;
                  var step1 = {
                      "step": 3,
                      "idOrder": number,
                      "shipment": shipment
                  };
                  localStorageService.set('step', step1);
                  $rootScope.instruction = instruction_payment; // instruction payment
                  $rootScope.$broadcast('instruction', $rootScope.isActiveIns); // set show/hide instruction
              }
          }).
          error(function(data) {
              console.log(data);
          });
      };
        /*
        * @function
        * @desc update payment method
        * @param
        * @return
        */
        $scope.checkOut=function(){
          $scope.loading=true;
          var user=localStorageService.get('user');
          var token=user.spree_api_key; 
          var shipment=localStorageService.get('step').shipment;
          var idOrder=localStorageService.get('step').idOrder;
          var sendingdata=
          {
            "order": {
              "payments_attributes": [
                {
                  "payment_method_id": $scope.paymentId
                }
              ]
            },
            "payment_source" : {
              "5":{"bogus":"bogus"},
            }
          };
          if($scope.paymentId == 3){
            $scope.checkout(idOrder,0);
            $state.go('app.confirmorder');
          }else{
            enterInforFactory.updateOrder(sendingdata,token,idOrder).
            success(function(data){
              $scope.checkout(idOrder,data.payments[0].id);
              $state.go('app.confirmorder');
              var step1={
              "step":0,
              "idOrder":idOrder,
              "shipment":shipment
              };
              localStorageService.set('step',step1); 
            }).
            error(function(data){
              console.log(data);
              $window.alert("Can't update the order");
            });
          }
        };
        /*
    * @function
    * @desc check out, save all information to localStorage
    * @param
    * @return
    */
    $scope.checkout=function(idOrder,idProduct){
        var user=localStorageService.get('user');
        var token=user.spree_api_key;
        var products=localStorageService.get('senddata');
        var deliveryinfor={
          "first_name":$scope.deliverinfor.firstname,
          "last_name":$scope.deliverinfor.lastname,
          "email":user.email,
          "idOrder":idOrder,
          "idAddress":$scope.deliverinfor.selectaddress,
          "phonenumber":$scope.deliverinfor.phone,
          "address":$scope.deliverinfor.address,
          "district":$scope.setDistrict($scope.deliverinfor.district),
          "time_delivery":$scope.getTimeDeliver($scope.deliverinfor.time),
          "payBy":$scope.paymethod,
          "methodId": $scope.paymentId,
          "products":products,
          "paymentId":idProduct
        }
        var food_like=$scope.list.like;
        var food_dislike=$scope.list.dislike;
        var foodpreferencedata={
          "people":$scope.deliverinfor.people,
          "food_like":food_like,
          "food_dislike":food_dislike,
          "anything_else":$scope.deliverinfor.feeling
        }
        var sendingdata=
        {
          "deliver":deliveryinfor,
          "comment":foodpreferencedata
        };
        localStorageService.set('data',sendingdata);
    };
        
        /*
        * @function : getAccountInfor
        * @desc     : send request to get account information(amount of money - important)
        * @param    : token
        * @return   : void - saving infor in localstorage(The token isn't changed)
        */
        $scope.getAccountInfor = function(token){
          PaymentFactory.checkMoney(token).
          success(function(data){
            localStorageService.remove('user');
            localStorageService.set('user',data);
            $rootScope.$broadcast('listener', true,data); 
          }).
          error(function(error){
            console.log(error);
          });
        }
        /****************************************************************************************/
        

  }]);
