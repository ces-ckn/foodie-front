'use strict';

/**
 * @ngdoc function
 * @name foodieFrontApp.controller:EnterinforCtrl
 * @description
 * # EnterinforCtrl
 * Controller of the foodieFrontApp
 */
angular.module('foodieFrontApp')
  .controller('PrepaidCartCtrl',['$rootScope','$scope','PaymentFactory','localStorageService','$window',
    function ($rootScope,$scope,PaymentFactory,localStorageService,$window) {
    $scope.number = 0;
    $scope.currentPayMethod = 0;
  	var amount = [
      {
        name      : "Fresh",
        price     : 300000,
        bonus     : 30000
      },
      {
        name      : "Green",
        price     : 500000,
        bonus     : 50000
      },
      {
        name      : "Healthy",
        price     : 700000,
        bonus     : 70000
      },
      {
        name      : "Super Green",
        price     : 900000,
        bonus     : 90000
      }
    ];
    /*var atms = [
      {
        name  : "Dong A",
        image : "./images/atm/donga.jpg"
      },
      {
        name  : "Dong A",
        image : "./images/atm/eximbank.jpg"
      },
      {
        name  : "Dong A",
        image : "./images/atm/techcombank.jpg"
      },
      {
        name  : "Dong A",
        image : "./images/atm/vietcombank.jpg"
      },
      {
        name  : "Dong A",
        image : "./images/atm/vietinbank.jpg"
      }
    ];*/
    var instruction = [
      {
        element   : '#plans',
        intro     : "Choose amount of money",
        position  : "top"
      },
      {
        element   : '#payment',
        intro     : "Choose payment method",
        position  : "top"
      }
    ]
    $scope.amounts = amount;
    //$scope.atms = atms;
    /*
    * @function : init
    * @desc     : set data when this page is loaded
    * @param    : none
    * @return   : void - set instruction
    */
    $scope.init = function(){
      $rootScope.instruction = instruction;                        // set content of instruction "prepaid cart" page
      $rootScope.$broadcast('instruction',$rootScope.isActiveIns); // set show/hide instruction
    }
    /**********************************************************************/
    /*
    * @function : selectAmount
    * @desc     : a user click to choose a amount and it will be highlight
    * @param    : index of amount
    * @return   : void - set selectedAmount
    */
    $scope.selectAmount = function(index){
      $scope.number = index; 
    }
    
    /**********************************************************************/
    /*
    * @function : selectAtm
    * @desc     : a user click to choose a atm payment
    * @param    : index of the atm bank
    * @return   : void - set selectedATM
    */
    $scope.selectAtm = function(index){
      $scope.selectedATM = index; 
    }
    /**********************************************************************/
    /*
    * @function : isCurrentATM
    * @desc     : add effect for the atm item.
    * @param    : index of amount
    * @return   : true/false
    */
    $scope.isCurrentATM = function(index) {
      return $scope.selectedATM === index;
    };
    /**********************************************************************/
    /*
    * @function : setCurrentPayMethod
    * @desc     : a user click to choose pay method
    * @param    : index of the pay method
    * @return   : void - set currentPayMethod
    */
    $scope.setCurrentPayMethod = function(index){
      $scope.currentPayMethod = index;
    };
    /**********************************************************************/
    /**********************************************************************/
    /*
    * @function : addMoney
    * @desc     : go to paypal page and add more money into account by paypal
    * @param    : none
    * @return   : void 
    */
    $scope.addMoney = function(param){
      var token = localStorageService.get('user').spree_api_key;
      var sendingData = {
        "amount" : amount[$scope.number].price
      }
      switch (param) {
        case 0:                                       //method is paypal
          $scope.addMoneyByPaypal(token,sendingData);
          break;
        case 2:
          $scope.addMoneyByBitCoin(token,sendingData); //method is bitcoin
          break;
        default:
          console.log("ERROR");
          break;
      }
    }
    /**********************************************************************/
    /**********************************************************************/
    /*
    * @function : addMoneyByPaypal
    * @desc     : go to paypal page and add more money into account by paypal
    * @param    : none
    * @return   : void 
    */
    $scope.addMoneyByPaypal = function(token,sendingData){
      PaymentFactory.addMoneyByPaypal(token,sendingData).
        success(function(data){
          console.log(data);
          $window.open(data,"_self");         // go to paypal page
        }).
        error(function(error){
          console.log(error);
        });
    }
    /**********************************************************************/
    /**********************************************************************/
    /*
    * @function : addMoneyByBitCoin
    * @desc     : go to bitcoin page and add more money into account by bitcoin
    * @param    : none
    * @return   : void 
    */
    $scope.addMoneyByBitCoin = function(token,sendingData){
      PaymentFactory.addMoneyByBitCoin(token,sendingData).
        success(function(data){
          console.log(data);
          $window.open(data,"_self");         // go to bitcoin page
        }).
        error(function(error){
          console.log(error);
        });
    }
    /**********************************************************************/
  }]);
