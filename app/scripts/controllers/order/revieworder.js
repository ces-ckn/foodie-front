'use strict';

/**
 * @ngdoc function
 * @name foodieFrontApp.controller:EnterinforCtrl
 * @description
 * # EnterinforCtrl
 * Controller of the foodieFrontApp
 */
angular.module('foodieFrontApp')
  .controller('ReviewOrderCtrl',['$rootScope','$scope','$state','localStorageService','PaymentFactory',
    function ($rootScope,$scope,$state,localStorageService,PaymentFactory) {
     var instruction = [
      {
        element   : '#step20',
        intro     : "The receipt",
        position  : 'left'
      },
      {
        element   : '#step21',
        intro     : "Download the receipt",
        position  : 'top'
      },
      {
        element   : '#step22',
        intro     : "Return the shopping",
        position  : 'left'
      }
    ];
    /*
    * @function : init
    * @desc     : show informations (user,order) like bill
    * @param    : none
    * @return   : void - set values
    */
    $scope.init = function(){
      var data = localStorageService.get('data');
      if(data != null){
        $scope.getAccountInfor();
        $scope.user = data.deliver;
        $scope.dishes = data.deliver.products;
        $scope.total = $scope.calculateTotalMoney(data.deliver.products);
        $scope.amount = $scope.calculateTotalPeople(data.deliver.products);
        localStorageService.remove('data');
        localStorageService.remove('dishes');
      }else{
        $state.go("app.onthemenu");
      }

      $scope.getAccountInfor()
      $rootScope.instruction = instruction;                         // set content of instruction "review order" page
      $rootScope.$broadcast('instruction',$rootScope.isActiveIns);  // set show/hide instruction
    };
    /**********************************************************************/

    /*
    * @function : calculateTotalMoney
    * @desc     : Calculating all money of dishes.
    * @param    : all dishes
    * @return   : money
    */
    $scope.calculateTotalMoney = function(dishes){
      var money = 0; 
      for (var i = 0; i < dishes.length ; i++) {
        money += parseInt(dishes[i].price); 				//plus other price of dish
      };
      return money;
    };
    /**********************************************************************/
    /*
    * @function : calculateTotalPeople
    * @desc     : Calculating amount of serving for all dishes..
    * @param    : all dishes
    * @return   : amount of serving
    */
    $scope.calculateTotalPeople = function(dishes){
      var amount = 0; 
      for (var i = 0; i < dishes.length ; i++) {
        amount += dishes[i].size; 						//plus other people of dish
      };
      return amount;
    };
    /**********************************************************************/
    /*
    * @function : getAccountInfor
    * @desc     : send request to get account information(amount of money - important)
    * @param    : token
    * @return   : void - saving infor in localstorage(The token isn't changed)
    */
    $scope.getAccountInfor = function(){
      var token = localStorageService.get('user').spree_api_key;
      PaymentFactory.checkMoney(token).
      success(function(data){
        localStorageService.remove('user');
        localStorageService.set('user',data);
        $rootScope.$broadcast('listener', true,data); 
      }).
      error(function(error){
        console.log(error);
      });
    }
    /****************************************************************************************/
    /*
    * @function : returnShopping
    * @desc     : continue shopping
    * @param    : none
    * @return   : void
    */
    $scope.returnShopping = function(){
      $rootScope.$broadcast('instruction',false);  // set show/hide instruction
      $state.go("app.onthemenu");
    }
    /****************************************************************************************/
  }]);
