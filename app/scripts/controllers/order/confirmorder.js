'use strict';

/**
 * @ngdoc function
 * @name foodieFrontApp.controller:ConfirmorderCtrl
 * @description
 * # ConfirmorderCtrl
 * Controller of the foodieFrontApp
 */
angular.module('foodieFrontApp')
  .controller('ConfirmorderCtrl',['$rootScope','$scope','$state','$window','localStorageService','confirmOrderFactory','PaymentFactory', 
  	function ($rootScope,$scope,$state,$window,localStorageService,confirmOrderFactory,PaymentFactory) {
    var instruction = [
      {
        element   : '#step15',
        intro     : "Review your information",
        position  : 'top'
      },
      {
        element   : '#step16',
        intro     : "View food",
        position  : 'top'
      },
      {
        element   : '#step17',
        intro     : "Price",
        position  : 'left'
      },
      {
        element   : '#step18',
        intro     : "Confirm",
        position  : 'left'
      },
      {
        element   : '#step19',
        intro     : "Cancel to return shopping",
        position  : 'left'
      }
    ];
  	$scope.availabledate=[];
    $scope.confirmorderinfor={};
    $scope.confirmorderinfor.username="";
    $scope.confirmorderinfor.phone="";
    $scope.confirmorderinfor.email="";
    $scope.confirmorderinfor.address="";
    $scope.confirmorderinfor.district="";
    $scope.confirmorderinfor.timeDeliver="";
    $scope.confirmorderinfor.people="";
    $scope.confirmorderinfor.like="None";
    $scope.confirmorderinfor.dislike="None";
    $scope.confirmorderinfor.feeling="None";
    $scope.confirmorderinfor.payBy="";
    $scope.total=0;
  	/*init function*/
  	$scope.init=function(){
      var deliveryinfor=localStorageService.get('data');
  		var products=deliveryinfor.deliver.products;
  		/*
		 get information of order from api
  		*/
  		if(deliveryinfor!==null){
          $scope.getInfor(deliveryinfor);
          $scope.deliveryinfor=deliveryinfor;
          var currentDate=new Date();
        // first value for product,price and available date
        for(var i=0;i<products.length;i++){
          $scope.total+=parseInt(products[i].price);
          var date=new Date(products[i].date);
          date.setHours(23);
          date.setMinutes(59);
          date.setSeconds(59);
          if(date>=currentDate){
            $scope.availabledate.push(products[i].date);
          }
          else{
            localStorageService.get('dishes').splice(i,1);
          }
        }
          $scope.isShow=true;
          $scope.getFirstDate();
          $scope.products=products;
        }
        else{
          $scope.isShow=false;
          $state.go('app.onthemu');
        }
        $rootScope.instruction = instruction;                         // set content of instruction "confirm order" page
        $rootScope.$broadcast('instruction',$rootScope.isActiveIns);  // set show/hide instruction
  	};
    $scope.getInfor=function(deliveryinfor){
      var infor=deliveryinfor.deliver;
      var foodpreference=deliveryinfor.comment;
      $scope.confirmorderinfor.username=infor.first_name+" "+infor.last_name;
      $scope.confirmorderinfor.phone=infor.phonenumber;
      $scope.confirmorderinfor.email=infor.email;
      $scope.confirmorderinfor.address=infor.address;
      $scope.confirmorderinfor.district=infor.district;
      $scope.confirmorderinfor.timeDeliver=infor.time_delivery;
      $scope.confirmorderinfor.people=foodpreference.people;
      if(foodpreference.food_like===""||foodpreference.food_like.length<1){
       $scope.confirmorderinfor.like="None"; 
      }
      else{
        $scope.confirmorderinfor.like=$scope.getFood(foodpreference.food_like);
      }
      if(foodpreference.food_dislike===""||foodpreference.food_dislike.length<1){
        $scope.confirmorderinfor.dislike="None"; 
      }
      else{
        $scope.confirmorderinfor.dislike=$scope.getFood(foodpreference.food_dislike);
      }
      $scope.confirmorderinfor.feeling=foodpreference.anything_else;
      $scope.confirmorderinfor.payBy=infor.payBy;
    };
    $scope.getFood=function(data){
      var strfood="";
      for(var i=0;i<data.length;i++){
        var send=data[i].name+", ";
        strfood+=send;
      }
      return strfood;
    };
  	/*
    * @function
    * @desc
    * @param
    * @return
    */
    $scope.getFirstDate=function(){
      var list=$scope.availabledate;
      if(list===null){
        $scope.listdate=[];
      }
      else{
         $scope.listdate=list;
      for(var i=0;i<$scope.listdate.length-1;i++)
      {
       for(var j=i+1;j<$scope.listdate.length;)
       {
        if($scope.compareDate(new Date($scope.listdate[i]),new Date($scope.listdate[j]))){
          for(var k=j;k<$scope.listdate.length;k++){
            $scope.listdate[k]=$scope.listdate[k+1];
          }
          $scope.listdate.length--;
        }
        else{
          j++;
        }
       }
      }
      }
    };
    /*@function compareDate()
    * @des to compare 2 date
    *@param date and comparedate
    *return result of compare
    */
    $scope.compareDate=function(date,comparedate){
      if(comparedate.getDate() === date.getDate() && 
              comparedate.getMonth() === date.getMonth() && 
              comparedate.getFullYear() === date.getFullYear()){
        return true;
      }
      else return false;
    };
    $scope.backBeforePage=function(){
      var user=localStorageService.get('user');
      var token=user.spree_api_key; 
      var deliveryinfor=localStorageService.get('data').deliver;
      var idOrder=deliveryinfor.idOrder;
      $scope.cancelOrder(idOrder,token);
      $state.go('app.enterinfor');
    };
    /*
    * @function : payByCash
    * @desc     : the customer will send money when they receive foods
    * @param    : none
    * @return   : void 
    */
    $scope.payByCash = function(idOrder,token){
      PaymentFactory.payByCash(token,idOrder).
      success(function(data){
        console.log(data);
        $state.go('app.confirmorder.revieworder');
      }).
      error(function(data){
        console.log(data);
        $window.alert("Can't order");
      });
    }
    /****************************************************************************************/
    /*
    * @function : payByAccount
    * @desc     : if the customer has enough money in account - the system will calculate and pay by this method
    * @param    : none
    * @return   : void 
    */
    $scope.payByAccount = function(idOrder,token,paymentId){
      console.log(paymentId);
      PaymentFactory.payByAccount(idOrder,token,paymentId).
        success(function(data){
          console.log(data);
          $state.go('app.confirmorder.revieworder');
        }).
        error(function(error){
          console.log(error);
        });
    }
    /****************************************************************************************/
    /*
    * @function : payByPaypal
    * @desc     : go to paypal page to pay directly by paypal
    * @param    : none
    * @return   : void - set url(data)
    */
    $scope.payByPaypal = function(idOrder){
      PaymentFactory.paybyPaypal(idOrder).
        success(function(data){
          $window.open(data,'_self');
        }).
        error(function(error){
          console.log(error);
        });
    }
    /****************************************************************************************/
    /*
    * @function : payByBitCoin
    * @desc     : go to paypal page to pay directly by paypal
    * @param    : none
    * @return   : void - set url(data)
    */
    $scope.payByBitCoin = function(idOrder){
      PaymentFactory.paybyBitCoin(idOrder).
        success(function(data){
          console.log(data);
          $window.open(data,'_self');
        }).
        error(function(error){
          console.log(error);
        });
    }
    /****************************************************************************************/
    /**************************get Token*************************/
    /*
    @function   : getToken
    @desc       : get token from local storage
    @param      : none
    @return     : token
    */
    $scope.getToken = function(){
      var token;
      var currentUser = localStorageService.get('user');
      if(currentUser != null){
        token = currentUser.spree_api_key;
      }else{
        $state.go("app");
      }
      return token;
    }
    /******************************************************************/
    /*
    * @function : confirm
    * @desc     : The customer makes sure what they want to order
    * @param    : none
    * @return   : void 
    */
    $scope.confirm = function(){
      var token = $scope.getToken();
      var deliveryinfor = localStorageService.get('data').deliver;
      var idOrder = deliveryinfor.idOrder;
      var methodId = deliveryinfor.methodId;
      localStorageService.remove('dishes');
      localStorageService.remove('senddata');
      switch(methodId) {
        case 2:                               //Cash
          $scope.payByCash(idOrder,token);
          break;
        case 3:                               //Paypal
          $scope.payByPaypal(idOrder);
          break;
        case 4:                               //Account
          var PaymentId = deliveryinfor.paymentId;
          $scope.payByAccount(idOrder,token,PaymentId);
          break;
        case 5:                               //BitCoin
          $scope.payByBitCoin(idOrder);
          break;
        default:
          console.log("ERROR");
      }
    };
    /****************************************************************************************/
    $scope.cancel=function(){
      var user=localStorageService.get('user');
      var token=user.spree_api_key; 
      var deliveryinfor=localStorageService.get('data').deliver;
      var idOrder=deliveryinfor.idOrder;
      $scope.cancelOrder(idOrder,token);
        localStorageService.remove('dishes');
        localStorageService.remove('data');
        $state.go('app.onthemenu');
    };
    $scope.cancelOrder=function(idOrder,token){
       confirmOrderFactory.cancelOrder(idOrder,token).
      success(function(data){
        console.log(data);
      }).
      error(function(data){
        console.log(data);
      });
    };
        
  }]);
