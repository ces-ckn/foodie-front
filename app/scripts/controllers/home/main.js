'use strict';

/**
 * @ngdoc function
 * @name foodieFrontApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the foodieFrontApp
 */
angular.module('foodieFrontApp')
  .controller('MainCtrl',['$rootScope','searchFactory','$state','$scope','localStorageService','$timeout',
    function ($rootScope,searchFactory,$state,$scope,localStorageService,$timeout) {
    var Options = {
      steps             : [],
      showStepNumbers   : false,
      showBullets       : false,
      exitOnOverlayClick: false,
      exitOnEsc         : true,
      nextLabel         : '<span>Next</span>',
      prevLabel         : '<span>Previous</span>',
      skipLabel         : 'Exit',
      doneLabel         : 'Got it'
    };

    $scope.wantingToShowItem = false;
    $rootScope.isActiveIns = false;
    /*@function 
    * @desc     : create listener to check user login
    */      
    $scope.$on('listener', function(events, isLoggedIn,data){
      if(isLoggedIn===true){
        $scope.isLogined = true;
        $scope.user_name_account=data;
      }
      else
      {
        if(localStorageService.get('user') == null){
          $scope.user_name_account="";
          $scope.isLogined=false;
          localStorageService.remove('user');
        }
      }
    });
    /************************************************************************/

     /*@function 
    * @desc     
    * @param    
    * return     
    */    
  	$scope.Logout=function(){
  		$scope.user_name_account="";
        $scope.isLogined=false;
      if(localStorageService.get('checked')===false){
        localStorageService.remove('sampleuser');
        localStorageService.remove('user');
        localStorageService.remove('checked');
      }
      else{
        localStorageService.remove('user');
      }
      localStorageService.remove('dishes');
      localStorageService.remove('data');
      localStorageService.remove('prices');
      $state.go('app');
  	}
    /************************************************************************/
    /*
    * @function   : search
    * @desc       : when a user want to search some food by name
    * @params     : searchText
    * @return void() 
    */
    $scope.search = function(){
      if($scope.searchText != ""){
        var sendingData = { name : $scope.searchText };
        searchFactory.SearchFood(sendingData).then(function(res){
          console.log(res.data);
          $scope.products = res.data.products;
        });
      }
    };
    /************************************************************************/
    
     /*
      @function : show
    * @desc     : the user reacts/chooses a food, the page go to dish detail page
    * @param    : id of food, day array of food
    * return    : void
    */    
    $scope.show = function(id, available_ons) {
      $scope.wantingToShowItem = false; //close results
      $scope.hide();                    //hide results
      $state.go("app.onthemenu.dish",{  //go to dish detail page
        id : id,
        date: $scope.chooseAvailbleDate(available_ons)
      });
    };
    /************************************************************************/
     /*
      @function : startToShowItem
    * @desc     : show the results what the api send back.
    * @param    : none
    * return    : void
    */    
    $scope.startToShowItem = function() {
      $scope.wantingToShowItem = true;
    };
    /************************************************************************/
    /*@function : hide
    * @desc     : the user don't react searching function.
    * @param    : none
    * return    : void
    */    
    $scope.hide = function() {
      if(!$scope.wantingToShowItem){
        $scope.searchText = '';  //nothing to search
      }
    };
    /************************************************************************/

    /*@function : chooseAvailbleDate
    * @desc     : when the user choose a food in results, the system will find the available day of this food.
    * @param    : available_ons (day array)
    * @return   : available date
    */    
    $scope.chooseAvailbleDate = function(input){
      var days = 1000 * 60 * 60 * 24;               //tranfer day into miliseconds
      var availableDate = new Date(input[0].delivery_date).getTime();
      var now = new Date();
      now.getTime();
      now = Math.round(now / days);
      for(var i = 0; i < input.length; i++){
          var date = new Date(input[i].delivery_date).getTime();
          var days_date = Math.round(date / days);  // days of this time
          availableDate = date;
          if(days_date > now){                     // date of food is nearest date in future.
            break;
          }
      }
      return availableDate;
    };
    /************************************************************************/

    /*@function
    * @desc
    * @param
    * return
    */ 
    $scope.getAmountOfBag=function(){
      var dishes=localStorageService.get('dishes');
      if(dishes===null|| dishes.length<1){
        return 0;
      }
      else
      {
        var currentDate=new Date();
        var tomorrow=new Date(currentDate.setDate(currentDate.getDate()+1));
        for(var i=0;i<dishes.length;i++){
          var date=new Date(dishes[i].date);
          date.setHours(23);
          date.setMinutes(59);
          date.setSeconds(59);
          if(date<tomorrow){
            dishes.splice(i,1);
          }
        }
        localStorageService.set('dishes',dishes);
      }
      return dishes.length;
    };
    /***************************Some events of instruction******************************/
    /*@function : CompletedEvent,ExitEvent
    * @desc     : when click "Next" button, "Exit" button, "Continue" button
    * @param    : none
    * return    : void - set $rootScope.isActiveIns
    */ 
    $scope.CompletedEvent = function () {
      $rootScope.isActiveIns = true;
    };

    $scope.ExitEvent = function () {
      $rootScope.isActiveIns = false;
      $scope.isActive = false;
    };
    /************************************************************************/
    /***************************Instruction******************************/
    /*@function : Instruction
    * @desc     : when click "Instruction" button on header
    * @param    : none
    * return    : void - set $rootScope.isActiveIns
    */ 
    $scope.Instruction = function(){
      if($rootScope.isActiveIns){
        $rootScope.isActiveIns = false; //turn off instruction mode
        $scope.isActive = false;
      }else{
        $rootScope.isActiveIns = true; //turn on instruction mode
        $scope.isActive = true;
        $scope.ShowInstruction();
      }
    };
    /************************************************************************/
     /***************************listener of instruction******************************/
    /*@function : instruction
    * @desc     : the page is changed state, this functionality will listen and handle event
    * @param    : none
    * return    : void
    */ 
    $scope.$on('instruction', function(events, isActived){
      Options.steps = [];
      Options.steps = Options.steps.concat($rootScope.instruction);
      $rootScope.isActiveIns = isActived;
      if(isActived){
        $scope.ShowInstruction();
      }else{
        $scope.isActive = isActived;
      }
    });
    /************************************************************************/
    /***************************Show Instruction******************************/
    /*@function : ShowInstruction
    * @desc     : to show instruction
    * @param    : none
    * return    : void
    */ 
    $scope.ShowInstruction = function(){
      $scope.IntroOptions = Options;
      $timeout(function() {
        $scope.CallMe();
      }, 500);
    }
    /************************************************************************/
  }]);

