'use strict';

/**
 * @ngdoc function
 * @name foodieFrontApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the foodieFrontApp
 */
angular.module('foodieFrontApp')
  .controller('UserHomePageCtrl',['$rootScope','$scope','localStorageService','$sce','homepagefactory','PaymentFactory', 
    function ($rootScope,$scope,localStorageService,$sce,homepagefactory,PaymentFactory) {
    var instruction = [
      {
        element   : '#step1',
        intro     : 'Go shopping'
      }
    ];
  $scope.currentSlideIndex=0;
  // define array of main header
    $scope.slides=[
    {
        src:'../images/MainHeader/MainHeader-1.png',
        title1:'Cooking make easier for Vietnamese women',
        title2:'Fresh ingredients and chef-designed recipes, delivered each week',
        icon: './images/cook-1.png'
    },
    {
        src:'../images/MainHeader/MainHeader-2.png',
        title1:'Happy cooking, happier family',
        title2:'Fresh, clean and nutitious prepared food',
        icon: './images/cook-2.png'
    },
    {
        src:'../images/MainHeader/MainHeader-3.png',
        title1:'Warm dinner just 20 minutes',
        title2:'Fresh ingredients and chef-designed recipes, delivered each week',
        icon: './images/cook-3.png'
    }];

    // define array dishes
    $scope.dishes=[
    {
        id:0,
        icon:'./images/icons/icon-maindish.png',
        image:'./images/maindish/dish-1.jpg',
        title1:'Cơm chiên kim chi',
        title2:'with Garlic Crostini',
        category:'Main Dish'
    },
    {
        id:1,
        icon:'./images/icons/icon-maindish.png',
        image:'./images/maindish/dish-2.jpg',
        title1:'Sapagetti nấu đậu',
        title2:'with Garlic Crostini',
        category:'Main Dish'
    },
    {
        id:2,
        icon:'./images/icons/icon-maindish.png',
        image:'./images/maindish/dish-3.jpg',
        title1:'Cơm lúa mạch',
        title2:'with Garlic Crostini',
        category:'Main Dish'
    },
    {
        id:3,
        icon:'./images/icons/icon-maindish.png',
        image:'./images/maindish/dish-5.jpg',
        title1:'Tôm cay rang muối',
        title2:'with Garlic Crostini',
        category:'Main Dish'
    },
    {
        id:4,
        icon:'./images/icons/icon-vet.png',
        image:'./images/vegetable/dish-4.jpg',
        title1:'Cơm chiên kim chi',
        title2:'with Garlic Crostini',
        category:'Vegetable'
    },
    {
        id:5,
        icon:'./images/icons/icon-vet.png',
        image:'./images/vegetable/dish-6.jpg',
        title1:'Sapagetti nấu đậu',
        title2:'with Garlic Crostini',
        category:'Vegetable'
    },
    {
        id:6,
        icon:'./images/icons/icon-vet.png',
        image:'./images/vegetable/dish-7.jpg',
        title1:'Cơm lúa mạch',
        title2:'with Garlic Crostini',
        category:'Vegetable'
    },
    {
        id:7,
        icon:'./images/icons/icon-vet.png',
        image:'./images/soup/dish-9.jpg',
        title1:'Tôm cay rang muối',
        title2:'with Garlic Crostini',
        category:'Soup'
    },
    {
        id:8,
        icon:'./images/icons/icon-vet.png',
        image:'./images/soup/dish-10.jpg',
        title1:'Sapagetti nấu đậu',
        title2:'with Garlic Crostini',
        category:'Soup'
    },
    {
        id:9,
        icon:'./images/icons/icon-vet.png',
        image:'./images/soup/dish-11.jpg',
        title1:'Cơm lúa mạch',
        title2:'with Garlic Crostini',
        category:'Soup'
    },
    {
        id:10,
        icon:'./images/icons/icon-vet.png',
        image:'./images/soup/dish-12.jpg',
        title1:'Tôm cay rang muối',
        title2:'with Garlic Crostini',
        category:'Soup'
    }];
    // define value of list customer
    $scope.quotes = [
        {
            name: "Markl T",
            address: "Hai Chau, Da Nang",
            content: "I harvest my own free-range water, so the idea of putting it in a plastic tray and a commercially made electricity-wasting freezer disgusts me. I prefer nature's method, waiting until the temperature outside drops below freezing",
            avatar: "./images/quote_avatar/quote-avtar-1.png",
            bigImage: "./images/Quote/quote_1.png"
        },
        {
            name: "Truc Nhan",
            address: "Hai Phong, Da Nang",
            content: "This recipe is horrible! Maybe I should have left them in longer than two minutes (the recipe doesn't say how long to leave them in the freezer so I just kind of guessed) but mine came out all watery. I won't be making these again",
            avatar: "./images/quote_avatar/quote-avtar-5.png",
            bigImage: "./images/Quote/quote_5.png"
        },
        {
            name: "Ngoc Diem",
            address: "458 Nguyen Thi Minh Khai, Hai Chau",
            content: "I made a few adjustments...... used a pot instead of trays. boiled instead of freezing. Added salt, potatoes, carrots and beef to the water. It turned out more like soup instead of ice cubes. Next time I will make a few more adjustments to try and get this recipe to work for me.",
             avatar: "./images/quote_avatar/quote-avtar-2.png",
            bigImage: "./images/Quote/quote_2.png"
        },
        {
            name: "Duong My Yen",
            address: "59 Nguyen Tan Thanh",
            content: "It looked a bit thin so I added some kidney beans (half of a 15 oz. can) and some chicken stock. Try it, you'll like it!",
             avatar: "./images/quote_avatar/quote-avtar-3.png",
            bigImage: "./images/Quote/quote_3.png"
        },
        {
            name: "Ho Anh",
            address: "Hoa Khanh, Da Nang",
            content: "Thank you for your recipe. We make something very similar in Thailand. Our version is called hielo, and instead of water we use agua",
            avatar: "./images/quote_avatar/quote-avtar-4.png",
            bigImage: "./images/Quote/quote_4.png"
        }
    ];
    // define main slide show
    /*@function
    * @desc
    * @param
    * return
    */    
    $scope.setCurrentSlideIndex=function(index){
        $scope.currentSlideIndex=index;
    };
     /*@function
    * @desc
    * @param
    * return
    */  

    $scope.isCurrentSlideIndex=function(index){
        return $scope.currentSlideIndex===index;
    };

     /*@function
    * @desc
    * @param
    * return
    */    
    $scope.setIcon = function(category) {

                    var iconClass = "";
                    switch(category){
                        case "Main Dish":
                            iconClass = "icon-main-dish";
                            break; 
                        case "Soup":
                            iconClass = "icon-soup-dish";
                            break; 
                        case "Vegetable":
                            iconClass = "icon-veget-dish";
                            break; 
                    }
                    return iconClass;
                };
      // define cate function
    /*@function
    * @desc
    * @param
    * return
    */    
    $scope.currentCate = null;
    $scope.setCurrentCateIndex = function ($index) {
        $scope.currentCate = $index;
    };
    /*@function
    * @desc
    * @param
    * return
    */    
    $scope.isCurrentCateIndex = function (index) {
        return $scope.currentCate === index;
        console.log(currentCate);
    };
    //define filter depend on cate
    /*@function
    * @desc
    * @param
    * return
    */    
     var selectedCategory = "Main Dish";
     var idDish=0;
    $scope.selectCategory = function (newCategory) {
        selectedCategory = newCategory;
    };
    /*@function
    * @desc
    * @param
    * return
    */    
     $scope.ismainCategory = function (newCategory) {
        return selectedCategory === "Main Dish";
    };
    /*@function
    * @desc
    * @param
    * return
    */    
     $scope.issoupCategory = function (newCategory) {
        return selectedCategory === "Soup";
    };
    /*@function
    * @desc
    * @param
    * return
    */    
     $scope.isvetCategory = function (newCategory) {
        return selectedCategory === "Vegetable";
    };
    /*@function
    * @desc
    * @param
    * return
    */    
    $scope.categoryFilterFn = function (product) {
        return selectedCategory === null || product.category === selectedCategory;
    };
    /*@function
    * @desc
    * @param
    * return
    */    
    $scope.getCategoryClass = function (category) {
        return selectedCategory === category;
    };
    /*@function
    * @desc
    * @param
    * return
    */    
    $scope.setdishID = function(id) {
        // $rootScope.dishdetailID = id;
        idDish=id;
        console.log(idDish);
    };
    /*@function
    * @desc
    * @param
    * return
    */    
    $scope.showImagefirst=function(){
       console.log(idDish);
       return true;
    };
     /*@function
    * @desc
    * @param
    * return
    */    
     $scope.selectMainItems = function (item) {
        return item.category === "Main Dish";
    };
     /*@function
    * @desc
    * @param
    * return
    */    
    $scope.selectVegItems = function (item) {
        return item.category === "Vegetable";
    };
     /*@function
    * @desc
    * @param
    * return
    */    
    $scope.selectSoupItems = function (item) {
        return item.category === "Soup";
    };
    // select avatar quote
     /*@function
    * @desc
    * @param
    * return
    */    
     $scope.selectedQuote = 0;
    $scope.setCurrentQuoteIndex = function (index) {
            $scope.selectedQuote = index;
            console.log($scope.selectedQuote);
        };
    /*@function
    * @desc
    * @param
    * return
    */    
    $scope.isCurrentQuoteIndex = function (index) {
            return $scope.selectedQuote === index;
            // return selectedQuote == index ? activeQuotePerson : "";
        };

    /*@function
    * @desc
    * @param
    * return
    */    
    $scope.prevQuote = function () {
            $scope.selectedQuote = ($scope.selectedQuote < $scope.slides.length - 1) ? ++$scope.selectedQuote : 0;
        };
    /*@function
    * @desc
    * @param
    * return
    */       
    $scope.nextQuote = function () {
        $scope.selectedQuote = ($scope.selectedQuote > 0) ? --$scope.selectedQuote : $scope.slides.length - 1;
    };
    /*@function
    * @desc
    * @param
    * return
    */
    var supplierimages=[
    {
        "image":"./images/article/aticle-1.png",   
    },
    {
        "image":" ./images/article/box-2.jpg",
    },
    {
        "image":" ./images/article/article-2.jpg",
    }
    ]; 
    $scope.supplier=[];
    // console.log(supplier[0].content.length);
    $scope.getSupplier=function(){
        homepagefactory.getSupplier().
        success(function(data){
            for(var i=0;i<supplierimages.length;i++){
                var supplier=
                {
                    "head":data.suppliers[i].name,
                    "image":supplierimages[i].image,
                    "content":data.suppliers[i].description
                };
                $scope.supplier.push(supplier);
            }
        // console.log($scope.supplier);
        // $scope.supplier=data;
      }).
      error(function(data){
        console.log(data);
      });
    };
    $scope.getSupplier();
    $scope.setLink=function(){
    homepagefactory.setLink().
      success(function(data){
        console.log(data);
      
      }).
      error(function(data){
        console.log(data);
      });
    };
    /*@function : init
    * @desc     : set data when this page is loaded
    * @param    : none
    * @return   : void - set instruction
    */ 
    $scope.init = function(){
        $rootScope.instruction = instruction;                        // set content of instruction "home" page
        $rootScope.$broadcast('instruction',$rootScope.isActiveIns); // set show/hide instruction
        if(localStorageService.get("user") != null){
            var CurrentUser = localStorageService.get('user');
            $scope.getAccountInfor(CurrentUser.spree_api_key);
        }
    }
    /****************************************************************************************/
    /*
    * @function : getAccountInfor
    * @desc     : send request to get account information(amount of money - important)
    * @param    : token
    * @return   : void - saving infor in localstorage(The token isn't changed)
    */
    $scope.getAccountInfor = function(token){
        PaymentFactory.checkMoney(token).
        success(function(data){
            /*console.log(data);*/
            var old_balance = localStorageService.get('user').balance;
            if(parseInt(old_balance) < parseInt(data.balance)){
                $scope.money = data.balance - old_balance;
                $scope.isAddedMoney = true;
            }else{
                $scope.isAddedMoney = false;
            }
            localStorageService.remove('user');
            localStorageService.set('user',data);
            $rootScope.$broadcast('listener', true,data);
        }).
        error(function(error){
          console.log(error);
        });
    }
    /****************************************************************************************/
  }]);