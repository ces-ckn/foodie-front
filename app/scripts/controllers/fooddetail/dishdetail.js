'use strict';

/**
 * @ngdoc function
 * @name foodieFrontApp.controller:DishdetailCtrl
 * @description
 * # DishdetailCtrl
 * Controller of the foodieFrontApp
 */
angular.module('foodieFrontApp')
  .controller('DishdetailCtrl', ['$sce','$rootScope','$scope', 'DishDetailFactory','OnTheMenuFactory','$stateParams','localStorageService','$state',
    function ($sce,$rootScope,$scope,DishDetailFactory,OnTheMenuFactory,$stateParams,localStorageService,$state) {
        var instruction = [
          {
            element   : '#step23',
            intro     : "Introduction about food and available date",
            position  : 'left'
          },
          {
            element   : '#step24',
            intro     : "Amount of serving and price",
            position  : 'left'
          },
          {
            element   : '#step25',
            intro     : "Add food to bag",
            position  : 'left'
          }
        ]; 
        
        $scope.isAppend = false;
        $scope.isIngredient = false;
        $scope.isNutrition = true;
        $scope.radioModel = 'Ingredient';           //show default ingredient tab
        var token;
        var currentUser = localStorageService.get('user');
        if(currentUser != null){
            token = currentUser.spree_api_key;
        }
        /**************************Init functionality*************************/
        /*
        @function   : getData
        @desc       : get information through api - initiating page
        @param      : none
        @return     : void - data from api
        */
        $scope.getData = function(){
            $scope.isLoading = true;        // show spinner
            $scope.usingDirective();
            $scope.getDishDetail();         // set dish detail              
    	}
        /******************************************************************/

        /**************************Delivery Day*************************/
        /*
        @function   : DeliveryDay
        @desc       : the api send a product what includes available_on array
                      front end need find the exact delivery day
        @param      : availble_on from product
        @return     : delivery day
        */
        $scope.DeliveryDay = function(arrayDay){
            var d = new Date();
            d.setTime($stateParams.date);
            for(var j = 0; j< arrayDay.length; j++){                //loop in available_on array
                var date = new Date(arrayDay[j].delivery_date);
                if(d.toDateString() === date.toDateString()){       //the date is exist in available_on
                    return d;
                }
            }
        }
        /******************************************************************/

        /**************************set values*************************/
        /*
        @function   : setValues
        @desc       : when front end receive data, it helps show data.
        @param      : data from api, selected date.
        @return     : void - set type by json
        */
        $scope.setValues = function(data,selectedDate){
            //$scope.DeliveryDay(data.available_on);
            $scope.id = $stateParams.id;
            $scope.name = data.name;                            //set name of dish
            $scope.date = selectedDate;                         //set date of dish
            $scope.description = data.description;              //set description of dish
            $scope.money = data.dish_price;
            $rootScope.money = data.dish_price;                     //set price
            $scope.dish_currency = data.dish_currency;          //set currency
            $scope.product = data;                  
            $scope.current_user_like = data.current_user_like;  //using it to check someone like/dislike a dish
            $scope.image = data.images[0].large_url;            //set image of dish
            $scope.cooking = $sce.trustAsHtml(data.dish_cooking_instructions);
            //$scope.cooking = data.dish_cooking_instructions;    //set cooking instructions
            $scope.ingredients = data.ingredients;              //set ingridients
            $scope.comments = data.comments;                    //set comments
            $scope.isAppend = true;                             //appending comment directive
            $scope.nutritions = data.nutritions;                //set nutritions
        }
        /******************************************************************/


        /**************************set property to use directive*************************/
        /*
        @function   : usingDirective
        @desc       : inject some condition to use comment/like directive
        @param      : none
        @return     : void - set type by json
        */

        $scope.usingDirective = function(){
            var sendingData = {
                id   : $stateParams.id,
                type : "dish",
                page : "dish"
            };
            var filter = {
                id_dish : $stateParams.id,
                date_box : $stateParams.date,
                id       : 0
            } 
            $scope.filter = filter;
            $scope.type = sendingData;
        }

        /******************************************************************/

        /**************************Collapse*************************/
        /*
        @function   : Collapse
        @desc       : using it to show ingredients/nutritions tab
        @param      : param of tab (1 : nutritions)
        @return     : void (show/hide effect)
        */
        $scope.Collapse = function(param){
            if(param==1){
                $scope.isIngredient = false;
                $scope.isNutrition = true;
                
            }else{
                $scope.isIngredient = true;
                $scope.isNutrition = false;
            }
        }
        /******************************************************************/
        /**************************The dish detail*************************/
        /*
        @function   : getDishDetail
        @desc       : show dish detail
        @param      : none
        @return     : void - set dish detail
        */
        $scope.getDishDetail = function(){
            DishDetailFactory.getDish($stateParams.id,token).
                success(function(data){
                    console.log(data);
                    var selectedDate = $scope.DeliveryDay(data.available_on);
                    if(typeof selectedDate != "undefined"){         //the selected day is exist in available_on
                        $scope.setValues(data,selectedDate);
                    }else{                                          //something is wrong with selected date
                        $state.go("app.onthemenu");
                    }
                    $rootScope.instruction = instruction;                        // set content of instruction "box" page
                    $rootScope.$broadcast('instruction',$rootScope.isActiveIns); // set show/hide instruction
                    $scope.isLoading = false;                       //hide spinner
                }).
                error(function(error){
                    $state.go("app.onthemenu");
                });
        }
        /******************************************************************/
  }]);
