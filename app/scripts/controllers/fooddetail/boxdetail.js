'use strict';

/**
 * @ngdoc function
 * @name foodieFrontApp.controller:BoxdetailctrlCtrl
 * @description
 * # BoxdetailctrlCtrl
 * Controller of the foodieFrontApp
 */
angular.module('foodieFrontApp')
  .controller('BoxDetailCtrl',['$rootScope','$scope','$state','localStorageService','BoxDetailFactory','$stateParams', 
    function ($rootScope,$scope,$state,localStorageService, BoxDetailFactory,$stateParams) {
    var instruction = [
      {
        element   : '#step23',
        intro     : "Introduction about food and available date",
        position  : 'left'
      },
      {
        element   : '#step24',
        intro     : "Amount of serving and price",
        position  : 'left'
      },
      {
        element   : '#step25',
        intro     : "Add food to bag",
        position  : 'left'
      }
    ]; 
    var images = [];
   	$scope.dish_currency = "VND";
   	$scope.isIngredient = false;
    $scope.isNutrition = true;
    $scope.radioModel = 'Ingredient';
    $scope.isAppend = false;
    $scope.index = 0;

    /**************************ShowHide Ingredients/Nutritions Action*************************/
    /*
    @func: Collapse
    @desc: when a user click nutritions/ingredients bar, it will show information about user's choice
    @param: id of bar
    @return: void
    */
   	$scope.Collapse = function(param){
        if(param==1){
            $scope.isIngredient = false;
            $scope.isNutrition = true;
            
        }else{
            $scope.isIngredient = true;
            $scope.isNutrition = false;
        }
    }
    /******************************************************************/

    /**************************Show Image Action*************************/
    /*
    @func: changeImage
    @desc: when a user click small image, the big picture and its name will be changed
    @param: id of dish
    @return: void
    */
    $scope.changeImage = function(param){
      for (var i = 0; i < images.length; i++) {
        if(images[i].id == param){
          $scope.showingImage = images[i].src;
          $scope.name = images[i].name;
          $scope.index = param;
        }
      };
    }
    /******************************************************************/

    /**************************Initial Action*************************/
    /*
    @func: getData
    @desc: load data from sever when a user go to box detail page
    @param: id of box
    @return: void
    */
    $scope.getData = function(){
      $scope.isLoading = true;                                   // show spinner
      var token = $scope.getToken();
      var d = new Date();
      d.setTime($stateParams.date);
      var sendingData = {
        date : d
      };
      BoxDetailFactory.getBox($stateParams.type,sendingData,token).then(function(res){
        console.log(res.data);
        if(res.data.delivery.length != 0){
          $scope.setValues(res.data);
        }else{
          $state.go("app.onthemenu");
        }
        $rootScope.instruction = instruction;                       // set content of instruction "box" page
        $rootScope.$broadcast('instruction',$rootScope.isActiveIns); // set show/hide instruction
        $scope.isLoading = false;                                   // hide spinner
      }),function(response){
        $state.go("app.onthemenu");
      };
    }
    /******************************************************************/
    /**************************set Values*************************/
    /*
    @func: setValues
    @desc: It's convenient for arranging images, use it for showing Images
    @param: id of dish
    @return: array images
    */
    $scope.setValues = function(box){
      $scope.box = box;
      $scope.products = box.delivery[0].products;      // set nutritions
      $scope.product = box;                            // using it for like directive
      $scope.setMoney(box.delivery[0].products);       // set price of box
      $scope.usingDirective(box.id);                   // using it for appending comment directive
      $scope.comments = box.comments;
      $scope.isAppend = true;
      $scope.setIngredients(box.delivery[0].products); // set ingredients of box
      $scope.description = box.description;
      $scope.createImagesArray(box);
      $scope.showingImage = images[0].src; 
      $scope.name = images[0].name;
      $scope.images = images;
      $scope.setBoxDate(box);
      
    };
    /******************************************************************/

    /**************************Create Images Array*************************/
    /*
    @func: createImagesArray
    @desc: It's convenient for arranging images, use it for showing Images
    @param: id of dish
    @return: array images
    */
    $scope.createImagesArray = function(box){
      var t = 
        {
          id    : 0,                       
          src   : box.images[0].large_url,  // image of box
          name  : box.name                  // name of box
        };

        images.push(t);                     
        for(var i = 1; i <= box.delivery[0].products.length;i++){
          t = 
            {
              id    : i,
              src   : box.delivery[0].products[i-1].images[0].large_url, // image of dish
              name  : box.delivery[0].products[i-1].name                 // name of dish
            };
          images.push(t); 
        }
    };
    /******************************************************************/

    /**************************get ingredients of dishes*************************/
    /*
    @function: getIngredients
    @desc: Get all ingredients of dishes
    @param: products(all products in box) to get id of dishes
    @return: void - array ingredients
    */
    $scope.setIngredients = function(products){
      var ingredients = [];
      for(var i = 0; i < products.length; i++){
        for(var j = 0; j < products[i].ingredients.length ; j++){
          var id = products[i].ingredients[j].id;
          if(!$scope.isTheSameIngredient(ingredients,id)){ // no ingredient have the same id
            ingredients.push({
              id        : products[i].ingredients[j].id,
              name      : products[i].ingredients[j].name,
              image     : products[i].ingredients[j].images[0].normal_url
            });
          };
        }
      }
      $scope.ingredients = ingredients;
    }
    /******************************************************************/

    /**************************filter ingredient*************************/
    /*
    @function: isTheSameIngredient
    @desc: show ingredient if they have the same id.
    @param: array ingredients and id of ingredient
    @return: true/false
    */
    $scope.isTheSameIngredient = function(ingredients,id){
      for(var i = 0; i < ingredients.length; i++){
        if(ingredients[i].id == id){ // if the ingredient existed
          return true;
        }
      }
      return false;
    }

    /******************************************************************/

    /**************************set Money*************************/
    /*
    @function: setMoney
    @desc: show money of box, total price of dishes
    @param: dishes
    @return: money
    */
    $scope.setMoney = function(products){
      var money = 0;
      for(var i = 0; i < products.length; i++){
        money = money + parseFloat(products[i].dish_price);
      }
      $scope.money = money;
    }

    /******************************************************************/

    /**************************set property to use directive*************************/
    /*
    @function   : usingDirective
    @desc       : inject some condition to use comment/like directive
    @param      : none
    @return     : void - set type by json
    */
    $scope.usingDirective = function(id){
      var sendingData = {
          id   : id,
          type : "box",           //type of product
          page : "box"            //page 
      };
      var filter = {
        id_dish  : 1,
        date_box : $stateParams.date,
        id_box   : id
      }
      $scope.filter = filter;
      $scope.type = sendingData;
   }
    /******************************************************************/
    /**************************get Token*************************/
      /*
      @function   : getToken
      @desc     : get token from local storage
      @param    : none
      @return   : token
      */
      $scope.getToken = function(){
        var token;
        var currentUser = localStorageService.get('user');
        if(currentUser != null){
          token = currentUser.spree_api_key;
        }
        return token;
      }
    /******************************************************************/
    /**************************set date of box*************************/
      /*
      @function   : setBoxDate
      @desc       : the date of box dependens on type of box
      @param      : box
      @return     : void
      */
      $scope.setBoxDate = function(box){
        var start = new Date(box.delivery[0].date);
        var date = new Date(box.delivery[0].date);
        $scope.start = start;                         // set 'start' date of box  
        if($stateParams.type == "weekly_box"){        // if this box is weekly box, it'll have end date.
          var end = date.setDate(date.getDate() + 5); 
          $scope.separation = "-"
          $scope.end = new Date(end);                 // set end date of box
        }
        $scope.date = date;                           // set date to use directive 'add to bag'
                                                      // if daily box - set start date
                                                      // else weeklybox -set end date
      }
    /******************************************************************/
  }]);
