'use strict';

/**
 * @ngdoc function
 * @name foodieFrontApp.controller:SignupcontrollerCtrl
 * @description
 * # SignupcontrollerCtrl
 * Controller of the foodieFrontApp
 */
angular.module('foodieFrontApp')
  .controller('SignupcontrollerCtrl',['$state','$scope','$rootScope','signupfactory','localStorageService',
   function ($state,$scope,$rootScope,signupfactory,localStorageService) {
  var instruction = [               //introduction: on the menu page
    {
      element   : '#register-form',
      intro     : "Sign up",
      position  : 'left'
    }
  ]; 
  $scope.init = function(){
    $rootScope.instruction = instruction;                        // set content of instruction "sign up" page
    $rootScope.$broadcast('instruction',$rootScope.isActiveIns); // set show/hide instruction
  }
  $scope.signup=function(){
    $scope.errorsignup="";
    var jsonsignup={
      user : { 
        email : $scope.email, 
        password :$scope.password
      }
    };
    signupfactory.signup(jsonsignup).
    success(function(data){
      console.log(data);
      localStorageService.set('user',data);
      $rootScope.$broadcast('listener', true,data);
      $state.go('app.editprofile');
    }).
    error(function(error){
    $rootScope.$broadcast('listener', false,null);
    $scope.errorsignup="Error when sign up";    
   });
  };
  }]);
