'use strict';

/**
 * @ngdoc function
 * @name foodieFrontApp.controller:ResetpasswordCtrl
 * @description
 * # ResetpasswordCtrl
 * Controller of the foodieFrontApp
 */
angular.module('foodieFrontApp')
  .controller('ResetpasswordCtrl',['$scope','$state','$rootScope','resetpasswordfactory','$stateParams','localStorageService',
   function ($scope,$state,$rootScope,resetpasswordfactory,$stateParams,localStorageService) {
   	var id=$stateParams.id;
   	var token=$stateParams.token;
   	$scope.Reset=function(){
   		var password=
   			{
          "password" : 
          {
          "id" : id, 
          "token" : token,
          "new_password": $scope.passwordreset
        }
      };
      console.log(password);
   		resetpasswordfactory.ResetPass(password).
   		success(function(data){
	       console.log(data);
        $state.go('app.login');
	  	}).
	  	error(function(error){
        $scope.reseterror="Error when reset password";
  	  	console.log(error);
		});
   	};   
  }]);
