'use strict';

/**
 * @ngdoc function
 * @name foodieFrontApp.controller:LogincontrollerCtrl
 * @description
 * # LogincontrollerCtrl
 * Controller of the foodieFrontApp
 */
angular.module('foodieFrontApp')
  .controller('LoginCtrl', ['$scope','$state','$rootScope','loginFactory','localStorageService', 
    function ($scope,$state,$rootScope,loginFactory,localStorageService) {
    $scope.email="";
    $scope.rememberMe=true;
    var instruction = [
      {
        element   : '#login-form',
        intro     : "Let's login to dicover everything",
        position  : "left"
      }
    ];
  
  $scope.init = function(){
    var checked=localStorageService.get('checked');
    var currentUser=localStorageService.get('sampleuser');
    if(checked===true){
      $scope.email=currentUser.email;
    }
    $rootScope.instruction = instruction;                        // set content of instruction "login" page
    $rootScope.$broadcast('instruction',$rootScope.isActiveIns); // set show/hide instruction
  }
  $scope.login=function(){
    $scope.errorlogin="";
  	loginFactory.login({email:$scope.email,password:$scope.password}).
  	success(function(data){
      /*console.log(data);*/
      localStorageService.set('user',data);
      localStorageService.set('sampleuser',data);
      localStorageService.set('checked',$scope.rememberMe);
      $rootScope.$broadcast('listener', true,data);
      if($rootScope.lastStateName==="" || $rootScope.lastStateName==="app.login.register" 
        || $rootScope.lastStateName==="app.resetpassword"){
        $rootScope.lastStateName = "app";
      }
      $state.go($rootScope.lastStateName, $rootScope.lastParams); 
  	}).
  	error(function(error, status){
      console.log(status);
      $rootScope.$broadcast('listener', false,null);
      $scope.errorlogin=error[0].messages;	  
	 });
  };
  }]);
