'use strict';

/**
 * @ngdoc function
 * @name foodieFrontApp.controller:ForgotCtrl
 * @description
 * # ForgotCtrl
 * Controller of the foodieFrontApp
 */
angular.module('foodieFrontApp')
  .controller('ForgotPasswordCtrl',['$scope','forgotpasswordfactory',
    function ($scope,forgotpasswordfactory) {
      $scope.error_forgot="";
      $scope.success_forgot="";
      $scope.textreset="Reset";
      $scope.loading=false;
    $scope.Reset=function(){
    	var email={
        email:$scope.emailforgot
      };
      $scope.loading=true;
      $scope.textreset="Sending...";
    	console.log(email);
    	forgotpasswordfactory.ResetPassword(email).
  		success(function(data){
     	console.log(data);
      $scope.textreset="Reset";
      $scope.emailforgot="";
      $scope.success_forgot="You've sent email successfully.Check email for reset password";
  		}).
  		error(function(error){
      $scope.textreset="Reset";
      $scope.emailforgot="";
    	$scope.error_forgot="Error sending email";
		  console.log(error);
	   });
    };
  }]);
