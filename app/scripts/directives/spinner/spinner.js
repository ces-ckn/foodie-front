'use strict';

/**
 * @ngdoc directive
 * @name foodieFrontApp.directive:spinner
 * @description
 * # spinner
 */
angular.module('foodieFrontApp')
  .directive('spinner', function () {
    return {
      templateUrl: './views/spinner/spinner.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        
      }
    };
  });
