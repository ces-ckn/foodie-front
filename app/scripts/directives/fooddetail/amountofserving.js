'use strict';

/**
 * @ngdoc directive
 * @name foodieFrontApp.directive:likeAction
 * @description
 * # likeAction
 */
angular.module('foodieFrontApp')
  .directive('amountofServing', function () {
    return {
      templateUrl: '../views/fooddetail/amountofserving.html',
      restrict: 'E',
      scope: {
      	type   : '=',
        money  : '='
      },
      controller : ['$rootScope','$scope',function ($rootScope,$scope){
        $scope.select = "- select number -";
        $scope.person = "person";
        var previous = 1;
        $rootScope.amountofserving=1;             //beginning the quantity of product = 1
        /*
        @func     : setMoney
        @desc     : when a user choose amount of serving, price will be increased/decreased by persons
        @param    : amount of serving
        @return   : void - set price, select persons.
        */
        $scope.setMoney = function(param){
          if(param > 1){
            $scope.person = param+" persons";
            $scope.select = "- "+param+" persons -";
          }else{
            $scope.person = "person";
            $scope.select = "- "+param+" person -";
          }
          var money = $scope.money / previous * param;
          $scope.money = money;
          $rootScope.money = $scope.money;            //money of product
          previous = param;
          $rootScope.amountofserving = previous;      //quantity of product
        }
        /**********************************************************************/
      }],
      link: function postLink(scope, element, attrs) {
        
      }
    };
  });
