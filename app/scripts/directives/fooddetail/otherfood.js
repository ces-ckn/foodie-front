'use strict';

/**
 * @ngdoc directive
 * @name foodieFrontApp.directive:otherfood
 * @description
 * # otherfood
 */
angular.module('foodieFrontApp')
  .directive('otherFood',['DishDetailFactory','OnTheMenuFactory', 
  	function (DishDetailFactory,OnTheMenuFactory) {
    return {
      	templateUrl: '../views/fooddetail/otherfood.html',
      	restrict: 'E',
      	scope: {
	      	filter: "="
	    },
      	controller : ['$scope','localStorageService','$state', 
      	function ($scope,localStorageService,$state){
      	$scope.init = function(){
      		$scope.Popover = {
	            title: "Dish's Name"                        //tool tip if name of dish is too long
	        };
      		$scope.usingDirective();
      		var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            $scope.getOtherDishes(tomorrow);        // set other dishes
            $scope.getOtherBoxes(tomorrow);

      	}
  		/**************************Other Dishes*************************/
        /*
        @function   : getOtherDishes
        @desc       : get other dishes to show in the dish or box detail
        @param      : none
        @return     : void - set other dishes
        */
        $scope.getOtherDishes = function(tomorrow){
            var sendingData = {
                date_from: tomorrow,
                date_to : tomorrow
            };
            DishDetailFactory.getDishesToday(sendingData,$scope.getToken()).then(function(res){
                console.log(res.data[0]);
                $scope.dishes = res.data[0];
                $scope.showingdate = new Date(res.data[0].date).getTime();
            });
        }
        /******************************************************************/
        /**************************Other Boxes*************************/
        /*
        @function   : getOtherBoxes
        @desc       : get other boxes to show in the dish or box detail
        @param      : none
        @return     : void - set other dishes
        */
            
        $scope.getOtherBoxes = function(tomorrow){
            var today = new Date();
            var start;
            var end;
            if(today.getDay() == 0 || today.getDay() >= 5){
                start = $scope.getMonday(7);
                end = $scope.getSaturday(7);
            }else{
                start = tomorrow;
                end = $scope.getSaturday(0);
            }
            OnTheMenuFactory.getDailyBoxes(start,end,$scope.getToken()).then(function(res){
                $scope.otherbox = res.data;
                $scope.boxes = res.data.delivery;
            });
        }
        /******************************************************************/
        /**************************get Day*************************/
        /*
        @function   : getMonday,getSaturday, getSpecificDay 
        @desc       : get some day, get Moday and Saturday (the website works from Monday to Saturday)
                      get Specific day when a user choose another day in day bar.
        @param      : id of day ( monday : 0, tuesday:1) 
        @return     : return day
        */
        $scope.getMonday = function(param) {
            var d = new Date();
            var day = d.getDay(),
            first = d.getDate() - day + (day == 0 ? -6:1) + param ;
            return new Date(d.setDate(first));
        };
        $scope.getSaturday = function(param) {
            var d = new Date();
            var day = d.getDay(),
            last = d.getDate() - day + (day == 0 ? -6:1) + 5 +param;
            return new Date(d.setDate(last));   
        };
        /******************************************************************/
        /**************************go to box detail page*************************/
        /*
        @function   : ChangeState
        @desc       : the customer click to go the box detail page
        @param      : type of box, date of box
        @return     : void - change state
        */
        $scope.ChangeState = function(type,date){
            var d = new Date(date);
            $state.go("app.onthemenu.box",{type: type, date : d.getTime()});
        }
        /******************************************************************/
        /**************************get Token*************************/
	    /*
	    @function 	: getToken
	    @desc 		: get token from local storage
	    @param 		: none
	    @return 	: token
	    */
	    $scope.getToken = function(){
	    	var token;
			var currentUser = localStorageService.get('user');
			if(currentUser != null){
				token = currentUser.spree_api_key;
			}
			return token;
	    }
	    /******************************************************************/
	    /**************************using other Directive*************************/
	    /*
	    @function 	: usingDirective
	    @desc 		: add more attr to use like directive
	    @param 		: none
	    @return 	: token
	    */
	    $scope.usingDirective = function(){
            var onlylike_dish = {
                type : "dish",
                page : "detail"
            }
            var onlylike_box = {
                type : "box",
                page : "detail"
            }
            $scope.only_like_dish = onlylike_dish;
            $scope.only_like_box = onlylike_box;
        }
        /******************************************************************/
	    /**************************show/hide box *************************/
	    /*
	    @function 	: isShow
	    @desc 		: hide/show box following selectedDate
	    @param 		: none
	    @return 	: token
	    */
        $scope.isShow = function(idbox,date,filter){
        	if(idbox == filter.id_box){
	        	var days = 1000 * 60 * 60 * 24;
	          	var boxDate = new Date(date).getTime();
	          	boxDate = Math.round(boxDate / days);
	          	var filtertime = Math.round(filter.date_box / days);
		        if(boxDate != filtertime){
		            return true;
		        }else{
		            return false;
		        }
        	}else{
        		return true;
        	}
        }
        /******************************************************************/
  		}],
      	link: function postLink(scope, element, attrs) {
        
      }
    };
  }]);
