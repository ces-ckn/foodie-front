'use strict';

/**
 * @ngdoc directive
 * @name foodieFrontApp.directive:commentdirective
 * @description
 * # commentdirective
 */
angular.module('foodieFrontApp')
  .directive('comment',['commentFactory',function (commentFactory) {
    return {
      templateUrl: '../views/fooddetail/comment.html',
      restrict: 'E',
      scope: {
        type: "=",
        data: "="
      },
      controller : ['$scope','$state','localStorageService',
      function ($scope, $state,localStorageService){
        var CurrentUser = localStorageService.get('user');
        var errorMessage = { 
          "blank":"You're missing your comment?",
          "nochange": "Please change your comment"
        };
       	$scope.rate = 3; //default stars
  	  	$scope.max = 5; //max stars
  	  	$scope.isReadonly = false; //can't rating
  	  	$scope.isValid = false; // validate
        $scope.comments = []; //array to show comments
        $scope.isDisplay = true; // can display
        $scope.isSuccess = false; // Display success
        $scope.templateUrl = 'option.html'; //using template
        var number = 3; //number of comments showed
        var start = 0;
        var end = number + start;
        var listComments;
        var paramEdit;

        /*****************************show % following stars***************************************/
        /*
        @func   : hoveringOver
        @desc   : estimate % following stars and show it next to stars in comment box
        @param  : number stars
        @return : void - % following stars
        */
  	  	$scope.hoveringOver = function(value) {
  	    	$scope.overStar = value;
  		    $scope.percent = 100 * (value / $scope.max);
  	  	};
        /**********************************************************************************/

        /*****************************getting more Comments *******************************/
        /*
        @func   : getmoreComments
        @desc   : when a user clicks view more button, push more comments into array and show them
        @param  : number of comments( number) and logical
        @return : void 
        */
        $scope.getmoreComments = function(){
          if(listComments.length >= end){
            for (var i = start; i < end; i++) {
              $scope.comments.push(listComments[i]);
            };
            start = end;
            end = start + number;
          }else{
            for (var i = start; i < listComments.length; i++) {
              $scope.comments.push(listComments[i]);
            };
          }
          $scope.visibleComments = $scope.comments.length;
        }
        /**********************************************************************************/

        $scope.allComments = $scope.data.length;
        listComments = $scope.data;
        $scope.getmoreComments();

        /*****************************Comment Action*******************************/
        /*
        @func  : comment
        @desc  : when a user clicks comment button, check validate and save comment
        @param : content and number of stars in comment box
        @return: void - if success, return successful comment message
        */
        $scope.comment = function(){
          if(CurrentUser){
            if(typeof($scope.textarea) === "undefined" || $scope.textarea == ""){
              $scope.isValid = true;
              $scope.errorMessage = errorMessage.blank;
            }else{                  //the user commented
              $scope.isSuccess = true;
              $scope.isValid = false;
              var sendingData = {
                "comment":
                  {
                    title: "Nothing", //it's not necessary
                    body: $scope.textarea,
                    rating: $scope.rate
                  }
              };
              $scope.textarea = "";
              commentFactory.saveComment(sendingData,$scope.type).
              success(function(data){
              }).
              error(function(error){
                  console.log(error);
              });
            }
          }else{
            $state.go('app.login');
          }
        };
        /**********************************************************************************/

        /**************************************Delete Action*****************************/
        /*
        @func   : deleteComment
        @desc   : when a user deletes comment,delete comment, update to database
        @param  : index of element in array to get attribute[id] of the comment
        @return : delete message
        */
        $scope.deleteComment = function(index) {
          var id = listComments[index].id;
          $scope.deleleClientSide(index);
          commentFactory.deleteComment(id).
              success(function(data){
                console.log(data);
              }).
              error(function(error){
                console.log(error);
              });
        };
        /*****************************support deleted Action*******************************/
        /*
        @func   : deleteClientSide
        @desc   : handle in client side
        @param  : index of element to delete it in array
        @return : void
        */
        $scope.deleleClientSide = function(index){
          $scope.comments.splice(index, 1);
          listComments.splice(index,1);
          $scope.allComments = listComments.length;
          start = start - 1;
          end = start + number;
          $scope.visibleComments = $scope.comments.length;
          if($scope.comments.length == 0 && listComments.length != 0){
            $scope.getmoreComments();
          }
          $scope.cancel();
        }
        /**********************************************************************************/

        /*****************************support Edited Action*******************************/
        /*
        @func   : editComment
        @desc   : handle edit action in client side
        @param  : index of element in array
        @return : void - show message
        */
        $scope.editComment = function(param){
          $scope.isValid = false;
          $scope.textarea = listComments[param].body;
          $scope.rate = listComments[param].rating;
          $scope.isDisplay = false;
          paramEdit = param;
        };
        /**********************************************************************************/

        /***************************Cancel edited Action*****************************/
        /*
        @func   : cancel
        @desc   : just show if a user clicks edit ,return comment interface
        @param  : no
        @return : no
        */
        $scope.cancel = function(){
          $scope.textarea = "";
          $scope.rate = 3;
          $scope.isDisplay = true;
          $scope.isValid = false;
        };
        /**********************************************************************************/

        /*****************************edit Action*******************************/
        /*
        @func     : updateComment
        @desc     : when a user clicks edit button, check validate
        @param    : content of comment (textarea)
        @return   : void - show message
        */
        $scope.updateComment = function(){
          if(typeof($scope.textarea) === "undefined" || $scope.textarea == ""){
            $scope.isValid = true;
            $scope.errorMessage = errorMessage.blank;
          }else if($scope.textarea == listComments[paramEdit].body 
            && $scope.rate == listComments[paramEdit].rating){
            $scope.isValid = true;
            $scope.errorMessage = errorMessage.nochange;
          }else{
            $scope.update();
          }
        };
        /**********************************************************************************/

        /*****************************update comment*******************************/
        /*
        @func   : update
        @desc   : when validate is true, update into database and handle edit event in client side
        @param  : index of element in array, content of comment, stars
        @return : update message
        */
        $scope.update = function(){
          var id = listComments[paramEdit].id;
          var sendingData = {
            "comment":
              {
                title: "Nothing",//it's not necessary
                body: $scope.textarea,
                rating: $scope.rate
              }
          };
          $scope.deleleClientSide(paramEdit);
          $scope.isDisplay = true;
          $scope.isSuccess = true;
          $scope.isValid = false;
          $scope.textarea = "";
          commentFactory.updateComment(sendingData,$scope.type,id).
            success(function(data){
              console.log(data);
            }).
            error(function(error){
              console.log(error);
            });
        };
        /**********************************************************************************/

        
      }],
      link: function postLink(scope, element, attrs) {
       
      }
    };
  }]);
