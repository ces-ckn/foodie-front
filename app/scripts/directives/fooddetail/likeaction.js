'use strict';

/**
 * @ngdoc directive
 * @name foodieFrontApp.directive:likeAction
 * @description
 * # likeAction
 */
angular.module('foodieFrontApp')
  .directive('likeAction', ['LikeFactory', function (LikeFactory) {
    return {
      templateUrl: '../views/fooddetail/likeadddirective.html',
      restrict: 'E',
      scope: {
      	type   : '=',
        product: '=',
        date   : "="
      },
      controller : ['$rootScope','$scope','localStorageService','$state', '$timeout',
      function ($rootScope,$scope,localStorageService,$state,$timeout){
        $scope.init=function(){
          $scope.showMessage = false;
          LikeFactory.getDeliveryHour().
          success(function(data){
             $scope.times=data.time_frames;
          }).
          error(function(data){
            console.log(data);
          });
        };
        /*
        @function : isOutOfDate
        @desc     : checking food is out dated
        @param    : product( dish)
        @return   : true/false (false is out dated)
        */
        $scope.isOutOfDate = function(){
          var days = 1000 * 60 * 60 * 24;
          //console.log($scope.date);
          var selectedDate = new Date($scope.date);
          selectedDate.setHours(0,0,0,0);
          selectedDate.getTime();
          selectedDate = Math.round(selectedDate / days);
          var tomorrow = new Date();
          tomorrow.setHours(0,0,0,0);
          tomorrow.setDate(tomorrow.getDate() + 1);
          tomorrow.getTime();
          tomorrow = Math.round(tomorrow / days);
          if(selectedDate >= tomorrow){
            return true;
          }else{
            return false;
          }
        };
      /***********************************************************************************/
        /*@function : likeFood
        * @desc     : when the customer like/dislike a food and they want to vote/devote it
        * @param    : product(dish/box)
        * return    : void
        */        
        $scope.likeFood = function(product){
          var user = localStorageService.get('user');
          if(user != null){
              var sendingData = $scope.createSendingData(product);
              var token = user.spree_api_key;
              if(!product.current_user_like){
                $scope.likeAction(sendingData,token);
              }else{
                $scope.dislikeAction(sendingData,token);
              }
          }else{
              $state.go('app.login');
          }
        };
        /***********************************************************************************/
        /*@function : likeAction
        * @desc     : when the customer like a food and they want to vote it
        * @param    : sendingData,token
        * return    : void
        */
        $scope.likeAction = function(sendingData,token){
          $scope.product.likes = $scope.product.likes +1;
          $scope.product.current_user_like = true;
          LikeFactory.saveLikes(sendingData,token).
              success(function(data){
                console.log(data);
              }).
              error(function(error){
                  console.log(error);
              });
        };
        /***********************************************************************************/
        /*@function : dislikeAction
        * @desc     : when the customer dislike a food and they want to devote it
        * @param    : sendingData,token
        * return    : void
        */        
        $scope.dislikeAction = function(sendingData,token){
          $scope.product.likes = $scope.product.likes -1;
          $scope.product.current_user_like = false;
          LikeFactory.saveDislikes(sendingData,token).
              success(function(data){
                  console.log(data);
              }).
              error(function(error){
                  console.log(error);
              });
        };
        /***********************************************************************************/
        /*@function : createSendingData
        * @desc     : create data to send
        * @param    : product (just need id of product)
        * @return   : json (id of product)
        */  
        $scope.createSendingData = function(product){
          var sendingData;
          if($scope.type.type == "dish"){
            sendingData =
              {
                "product_id": product.id
              };
          }else if($scope.type.type == "box"){
            sendingData =
              {
                "box_id": product.id
              };
          }else{
            console.log("createSendingData error");
          }
          return sendingData;
        }
        /*****************************************************************************/
          /*@function
          * @desc
          *@param
          *return
          */
          $scope.compareInLocal=function(){
          var dishes=[];
          if(localStorageService.get('dishes')===null){
            dishes=[];
          }
          else if(localStorageService.get('dishes').length<1){
            dishes=[];
          }
          else
          {
            dishes=localStorageService.get('dishes');
          }
          return dishes;
          };
          /*@function
          * @desc
          *@param
          *return
          */

        $scope.addFood = function(product,value){
          $scope.dateSelected= $scope.date;
          if(!$scope.compareBagList(product,$scope.date))
          {
            if(value===0){
              $scope.add(product,$scope.dateSelected,product.dish_price);
              $scope.showMessage = true;
              $timeout(function() {
                $scope.showMessage = false;
              }, 3000);
            }
            if(value===1){
              $scope.add(product,$scope.dateSelected,$rootScope.money);
              $state.go('app.bag');
            }
          }
          // $state.go('app.bag');
        };
       $scope.add=function(product,date,money){
            var dishes=$scope.compareInLocal();
            var data=
            {
              "product":product,
              "size":money/product.dish_price,
              "price":money,
              "date" :date,
              "hour":$scope.times[0]
            };
          dishes.push(data);
          localStorageService.set('dishes',dishes);
          $scope.isAdded(product);
       };
        /*@function
        * @desc
        * @param
        * return
        */
        $scope.isAdded=function(product){
          var days = 1000 * 60 * 60 * 24;
          var dishes=localStorageService.get('dishes');
          var selecteddate=new Date($scope.date).getTime();
          if(dishes!==null){
           for(var i=0;i<dishes.length;i++){
            var date=new Date(dishes[i].date).getTime();
            if(product.id===dishes[i].product.id && Math.round(date / days)===Math.round(selecteddate / days)){
              return true;
            } 
          }
        }
            else{
              return false;
            }
        };
        /*@function
        * @desc
        * @param
        * return
        */
        $scope.compareBagList=function(product,date){
          var date=new Date(date);
          date.setHours(23);
          date.setMinutes(59);
          date.setSeconds(59);
          var dishes=localStorageService.get('dishes');
          if(dishes!==null){
            for(var i=0;i<dishes.length;i++){
              var deliverydate=new Date(dishes[i].date);
              if(dishes[i].product.id===product.id && $scope.compareDate(date,deliverydate)){
                return true;
              }
            }
          }
          else return false;
        };
        
        /*@function
        * @desc
        *@param
        *return
        */
        $scope.compareDate=function(date,dateCompare){
          if(dateCompare.getDate() === date.getDate() && 
              dateCompare.getMonth() === date.getMonth() && 
              dateCompare.getFullYear() === date.getFullYear()){
            return true;
          }
          else return false;
        };

        /*
        @function
        @desc
        @param
        @return 
        */
        $scope.addBox = function(box){
          var dishesinlocal=$scope.compareInLocal();
          var dishes=box.delivery[0].products;
          if(dishesinlocal.length>=1){
            for(var i=0;i<dishes.length;i++){
              if($scope.compareBagList(dishes[i],$scope.date)){
                $scope.deleteSampleObject(dishes[i],$scope.date);
              }
              $scope.add(dishes[i],$scope.date,dishes[i].dish_price * $rootScope.amountofserving);
            }
          }
          else{
            for(var i=0;i<dishes.length;i++){
              $scope.add(dishes[i],$scope.date,dishes[i].dish_price * $rootScope.amountofserving);
            }
          }
          $state.go('app.bag');
        };
        /*
        @function
        @desc
        @param
        @return 
        */
        $scope.deleteSampleObject=function(product,date){
          var dishes=localStorageService.get('dishes');
          for(var i=0;i<dishes.length;i++){
            if($scope.compareBagList(product,date)){
              dishes.splice(i,1);
              localStorageService.set('dishes',dishes);
            }
          }
        };
      }],
      link: function postLink(scope, element, attrs) {
        
      }
    };
  }]);
