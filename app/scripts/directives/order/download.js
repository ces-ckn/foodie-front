'use strict';

/**
 * @ngdoc directive
 * @name foodieFrontApp.directive:download
 * @description
 * # download
 */
angular.module('foodieFrontApp')
  .directive('downLoad', function () {
    return {
      template: "<span class='download' ng-click='download()'><i class='fa fa-cloud-download'></i>Download the receipt</span>",
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
      	scope.download = function(){
    			var pdf = new jsPDF(); 								              //create pdf object
    			pdf.addHTML(angular.element('.panel'),function() {  //select form in html to build pdf
    				var string = pdf.output('datauristring');
    				pdf.save("Receipt.pdf"); 						              //save it as pdf file
    			});
      	}
      }
    };
  });
