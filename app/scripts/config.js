'use strict';
/*var configUrl = "http://192.168.0.113:3000/api/";*/
var configUrl = "https://foodie-2016-back.herokuapp.com/api/";
angular.module("foodieFrontApp")
	.constant("AppConfig",
		{
			"api" : {
				"session" : {
					"login"  	: configUrl + "login",
					"signup" 	: configUrl + "users",
					"account"  	: configUrl + "account",
					"supplier"	: configUrl + "suppliers"
				},
				"products" : {
					"products"  	: configUrl + "products",
					"types" 		: configUrl + "dish_types",
					"likes" 		: configUrl + "likes",
					"comments" 		: configUrl + "comments",
					"ingredients" 	: configUrl	+ "ingredients",
					"boxes" 		: {
						"allboxes"		: configUrl + "boxes",
						"dailyboxes"  	: configUrl + "daily_box",
						"weeklybox"  	: configUrl + "weekly_box"
					}
				},
				"email":{
					"subscribe" 			: configUrl + "email_subscriptions",
					"confirm_subscription" 	: configUrl + "confirm_subscription",
					"unsubscribe" 			: configUrl + "confirm_unsubscription",
					"forgotpassword" 		: configUrl + "forgot_password",
					"resetpasword" 			: configUrl + "reset_password"
				},
				"information":{
					"personal"				: configUrl + "edit_user",
					"updatepersonal" 		: configUrl + "update_user",
					"useraddress" 			: configUrl + "user_addresses",
					"address" 				: configUrl + "addresses",
					"favorite" 				: configUrl + "user_favorites",
					"foodpreference"		: configUrl + "surveys",
					"user"					: configUrl + "user_info",
					"avatar" 				: configUrl + "users/"
				},
				"order":{
					"hours"				: configUrl + "time_frames",
					"orders" 			: configUrl + "orders.json",
					"checkout" 			: configUrl + "checkouts",
					"shipments"			: configUrl + "shipments",
					"cancelorder" 		: configUrl + "orders",
					"historyorder"		: configUrl	+ "user_history_orders.json",
					"orderstatus"		: configUrl + "user_next_orders.json"
				},
				"payment":{
					"paypal" 			: configUrl + "paypal/",
					"addfund"			: configUrl + "paypall/add_fund?payment_method_id=3",
					"account"			: configUrl + "orders/",
					"bitcoin"			: configUrl + "spree_bitpay/"
				}
			}
		})
	.constant("AppURL",{
		"app": {
			"domain" 		: "http://sheltered-stream-93214.herokuapp.com",
			"image" 		: "https://food-service-x.s3.amazonaws.com/spree/images/3/large/pho.jpg?1454660658",
			"name" 			: "Fresh Food",
			"description" 	: "Cooking make easier for Vietnamese women."
		}
	});