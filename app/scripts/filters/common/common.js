'use strict';

/**
 * @ngdoc filter
 * @name foodieFrontApp.filter:common
 * @function
 * @description
 * # common
 * Filter in the foodieFrontApp.
 */
angular.module('foodieFrontApp')
    /**************************minimize Text*************************/
    /*
    @func: minimizeText
    @desc: Using it to cut a long text and using tail(...,!!!,etc) instead of text
    @param: value(long text), max(length of text what you want), tail (..., !!!,etc)
    @return: minimize Text and tail
    */
  	.filter('minimizeText', function () {
	    return function (value, max, tail) {
            if (!value) return '';              //if text is null
            max = parseInt(max, 10);            
            if (!max) return value;
            if (value.length <= max) return value;  //if length of text <= max
            value = value.substr(0, max);           //get text from 0 to max character
            var lastspace = value.lastIndexOf(' '); //check last character
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
            return value + (tail || ' …');          //return minimize text + tail(defaut is ...)
        };
	})
    /******************************************************************/

    /**************************around and add commas for money*************************/
    /*
    @func: filterMoney
    @desc: Money will be shown , if money > one thound sand, plus commas
    @param: value(money)
    @return: money
    */
    .filter('filterMoney', function () {
        return function (value) {
            value = parseInt(value).toFixed(0);
            while (/(\d+)(\d{3})/.test(value.toString())){      //add commas
                value = value.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
            }
            return value;
        };
    })
    /******************************************************************/
    /**************************delete space character*************************/
    /*
    @func: deleteBlank
    @desc: when a text had space character in first/last text. Example: "     asdas     "
            we really want to delete it.
    @param: value (text)
    @return: text (deleted space)
    */
    .filter('deleteBlank', function () {
        return function (value) {
            if(typeof value != "undefined"){
                return value.trim();            //delete blank
            }
        };
    })
    /******************************************************************/
    /**************************get all dates following month,year*************************/
    /*
    @func: filterDate
    @desc: a user choose month,year. this filter return all available dates.
    @param: month, year
    @return: all available dates
    */
    .filter('filterDate', function () {
        return function (value,month,year) {
            if(typeof month != "undefined" && typeof year != "undefined"){
                value = [];
                var date = new Date(year, month, 0).getDate();
                for (var i = 1; i <= date; i++) {
                    value.push(i);
                };
                return value;
            }
        };
    })
    /******************************************************************/
    /**************************get all dates following month,year*************************/
    /*
    @func: filterBoxMoney
    @desc: a box include 3 dishes. So, price of box = total price of 3 dishes
    @param: products
    @return: money
    */
    .filter('filterBoxMoney', function () {
        return function (products) {
            if(typeof products != "undefined"){
                var money = 0;
                for(var i = 0; i < products.length; i++){
                    money = money + parseFloat(products[i].dish_price);
                }
                return money;
            }
        };
    })
    /******************************************************************/
    /**************************get all dates following month,year*************************/
    /*
    @func: filterBoxMoney
    @desc: a box include 3 dishes. So, price of box = total price of 3 dishes
    @param: products
    @return: money
    */
    .filter('tranferTime', function () {
        return function (date) {
            if(typeof date != "undefined"){
                return date.getTime();
            }
        };
    })
    /******************************************************************/