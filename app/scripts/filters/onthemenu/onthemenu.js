'use strict';

/**
 * @ngdoc filter
 * @name foodieFrontApp.filter:onthemenu
 * @function
 * @description
 * # onthemenu
 * Filter in the foodieFrontApp.
 */
angular.module('foodieFrontApp')

    /**************************filter by type of dish*************************/
    /*
    @func 	: filterByType
    @desc 	: when a user click type of dish. the website will show dish following type of dish
    @param 	: input (products), index(id of type), selectedDate(date on day bar), types (type of dish)
    @return : filtered products array 
    */
	.filter('filterByType', function () {
	    return function (input,index,selectedDate,types) {
	    	if(typeof input !== "undefined" && typeof index !== "undefined" &&  typeof selectedDate !== "undefined" && typeof types !== "undefined"){
		    	var filteredItems = [];
	    		var date1 = new Date(selectedDate);
		    	for(var i = 0; i< input.length; i++){
		    		var item = input[i];
		    		for(var j = 0; j< item.available_on.length; j++){
		    			var date = new Date(item.available_on[j].delivery_date);
		    			if(date1.toDateString() === date.toDateString()){
							if(item.dish_type.id === index ||
						 		(index === 0 && types[item.dish_type.id-1].is_active)
						 	){
				    			filteredItems.push(item);
				    		}
		    			}
		    		}
		    	}
	    	}
	    	return filteredItems;
	    };
	});
	/******************************************************************/
