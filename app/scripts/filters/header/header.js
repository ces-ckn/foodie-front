'use strict';

/**
 * @ngdoc filter
 * @name foodieFrontApp.filter:header
 * @function
 * @description
 * # header
 * Filter in the foodieFrontApp.
 */
angular.module('foodieFrontApp')
    /**************************filter name*************************/
    /*
    @func   : filterName
    @desc   : showing name of a user account. Default: name is "No Name" string.
                if the user has name => name is user's name
                else the user has email => name is user's email
    @param  : input(information of user)
    @return : name
    */
    .filter('filterName', function () {
        return function (input) {
        	if(typeof input != "undefined" && typeof input != null){
        		var output = "NoName";
        		if(input.first_name != null){
        			output = input.first_name;
        		}else if(input.email != null){
        			output = input.email;
        		}
        	}
            return output;
        };
    })
    /******************************************************************/
     /**************************filter avarta*************************/
    /*
    @func   : filterAvarta
    @desc   : showing image of a user account. Default: path is "../images/no_avatar.gif"
                if the user has image => image will be set
    @param  : input(information of user)
    @return : image
    */
    .filter('filterAvarta', function () {
        return function (input) {
        	if(typeof input != "undefined" && input != ""){
        		var output = "../images/no_avatar.gif";
        		if(input.images.length > 0){
        			output = input.images[0].normal_url;
        		}
        	}
            return output;
        };
    });
    /******************************************************************/