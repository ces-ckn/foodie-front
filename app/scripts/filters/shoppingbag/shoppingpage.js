'use strict';

/**
 * @ngdoc filter
 * @name foodieFrontApp.filter:shoppingpage
 * @function
 * @description
 * # shoppingpage
 * Filter in the foodieFrontApp.
 */
angular.module('foodieFrontApp')
	/**************************arrange dish following date*************************/
    /*
    @func 	: filterByDate
    @desc 	: arrange dish by group following date on the bag page.
    @param 	: input (products), selectedDate(selected Date)
    @return : filtered products array 
    */
  	.filter('filterByDate', function () {
	    return function (input,selectedDate) {
	    	if(typeof input !== "undefined" &&  typeof selectedDate !== "undefined"){
		    	var filteredItems = [];
	    		var selectedDate = new Date(selectedDate);
	    		var currentDate=new Date();
		    	for(var i = 0; i < input.length; i++){
		    		var date = new Date(input[i].date);
		    			date.setHours(23);
			    		date.setMinutes(59);
			    		date.setSeconds(59);
		    		if(selectedDate.toDateString() === date.toDateString() 
		    			&& date >= currentDate){
		    			filteredItems.push(input[i]);
		    		}
		    	}
	    	}
	    	return filteredItems;
	    };
	})
	/*****************************************************************************/
	.filter('filterByHour', function () {
	    return function (input,selectedHour) {
	    	var filteredItems = [];
	    	if(typeof input !== "undefined" &&  typeof selectedHour !== "undefined"){
		    	for(var i = 0; i < input.length; i++){
		    		if(input[i].hour.id===selectedHour.id){
		    			filteredItems.push(input[i]);
		    		}
		    	}
	    	}
	    	return filteredItems;
	    };
	})
	.filter('filterOrderByDate', function () {
	    return function (input,selectedDate) {
	    	var filteredItems = [];
	    	if(typeof input !== "undefined" &&  typeof selectedDate !== "undefined"){
		    	for(var i=0;i<input.length;i++){
		    		var date=new Date(input[i].date_delivery);
		    			date.setHours(23);
			    		date.setMinutes(59);
			    		date.setSeconds(59);
		    		if(new Date(selectedDate).toDateString()===date.toDateString()){
		    			filteredItems.push(input[i]);
		    		}
		    	}
		    	console.log(filteredItems);
	    	}
	    	return filteredItems;
	    };
	})
	;
	/******************************************************************/
