// // var gzippo = require('gzippo');
// var express = require('express');
// // var morgan = require('morgan');
// var app = express();

// app.use(morgan('dev'));
// app.use(gzippo.staticGzip("" + __dirname + "/dist"));
// app.listen(process.env.PORT || 5000);
var express = require("express");
var port = process.env.PORT || 3000;

var app = module.exports = express();
app.use(express.static(__dirname + "/dist"));

app.get("*", function (req, res){
	res.sendfile(__dirname + "/dist/index.html");
});

app.all("/*");
app.listen(port);

